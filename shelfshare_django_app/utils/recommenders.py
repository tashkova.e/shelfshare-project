from recommender_engine.recommenders.collaborative import (
    CollaborativeFilteringRecommender,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from recommender_engine.recommenders.hybrid import FWLSRecommender
from recommender_engine.recommenders.popularity import PopularityRecommender

recs_dict = {
    "popularity": PopularityRecommender,
    "collab": CollaborativeFilteringRecommender,
    "content": ContentBasedRecommender,
    "hybrid": FWLSRecommender,
}
