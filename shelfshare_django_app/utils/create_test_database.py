import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
from pathlib import Path

import pandas as pd
from bookexchange.models import ReadBook
from books.models import Author, Book, Genre
from django.conf import settings
from tqdm import tqdm
from users.models import User

BASE_DIR = Path(settings.BASE_DIR)
DATA_PATH = Path(BASE_DIR.parent, "data")
TEST_DATASET = Path(DATA_PATH, "test_datasets")
import logging

from django.apps import apps

logger = logging.getLogger(__name__)


def delete_db(
    db_name: str,
):
    """Delete all the models in the specified database."""
    logger.info("Deleting all models in the database %s", db_name)
    for model in apps.get_models():
        logger.info("Deleting all instances of %s", model)
        model.objects.using(db_name).all().delete()


def create_reduced_database(database_name):
    """Populate the database with the 'original_db_reduced' dataset"""
    logger.info("Populating the database '%s' with the reduced dataset", database_name)
    # delete all the models in the specified database
    delete_db(database_name)
    # create books
    books_path = TEST_DATASET / "original_db_reduced" / "books_sample.csv"
    books = pd.read_csv(books_path)
    books["genres_list"] = books.genres.apply(
        lambda x: list(map(lambda x: x.strip().strip("'"), x.strip("[").strip("]").split(",")))
    )
    books["author_list"] = books.author.apply(
        lambda x: list(map(lambda x: x.strip().strip("'"), x.strip("[").strip("]").split(",")))
    )
    for book_tuple in tqdm(books.itertuples(), "Creating books..."):
        embedding = book_tuple.embeddings.replace("\n", "").strip("[]").strip().split(" ")
        embedding = [float(i) for i in embedding if i]
        publish_date = book_tuple.publishDate if isinstance(book_tuple.publishDate, str) else None
        new_book = Book.objects.using(database_name).create(
            ISBN=book_tuple.isbn,
            title=book_tuple.title,
            description=book_tuple.description,
            imageURL=book_tuple.imageUrl,
            publisher=book_tuple.publisher,
            publishDate=publish_date,
            goodreadsURL=book_tuple.url,
            embedding=embedding,
        )

        # Add genres to the book
        for genre_name in book_tuple.genres_list:
            genre, created = Genre.objects.using(database_name).get_or_create(genre=genre_name)
            new_book.genres.add(genre)
        # Add authors to the book
        for author_name in book_tuple.author_list:
            author, created = Author.objects.using(database_name).get_or_create(name=author_name)
            new_book.author.add(author)
        new_book.save()
    users_sample = TEST_DATASET / "original_db_reduced" / "users_sample.csv"
    # create users
    users = pd.read_csv(users_sample)
    sex_dict = {"male": "M", "female": "F"}
    user_lst = []
    for i, user in tqdm(users.iterrows(), "Creating users..."):
        new_user = User(
            username=user["username"], password="password", email="generic@mail.com", sex=sex_dict[user["sex"]]
        )
        user_lst.append(new_user)
    User.objects.using(database_name).bulk_create(user_lst)
    # create ratings
    ratings_path = TEST_DATASET / "original_db_reduced" / "ratings_sample.csv"
    ratings = pd.read_csv(ratings_path)
    readbooks_lst = []
    for rating_tuple in tqdm(ratings.itertuples(), "Creating ratings..."):
        user = User.objects.using(database_name).get(username=rating_tuple.username)
        book = Book.objects.using(database_name).get(ISBN=rating_tuple.isbn)
        new_readbooks = ReadBook(user=user, book=book, rating=rating_tuple.rating)
        readbooks_lst.append(new_readbooks)
    ReadBook.objects.using(database_name).bulk_create(readbooks_lst)


def create_database_with_fake_books(database_name):
    """Populate the database with the fake_books dataset"""
    logger.info("Populating the database '%s' with the fake_books dataset", database_name)
    # delete all the models in the specified database
    delete_db(database_name)
    # load ratings
    ratings_path = TEST_DATASET / "fake_books" / "ratings.csv"
    ratings = pd.read_csv(ratings_path)
    # create books
    books = ratings["isbn"].unique()
    books_lst = []
    for book_isbn in tqdm(books, "Creating books..."):
        new_book = Book(ISBN=book_isbn, title="Book" + str(book_isbn))
        books_lst.append(new_book)
    Book.objects.using(database_name).bulk_create(books_lst)
    # create users
    users = ratings["username"].unique()
    user_lst = []
    for username in tqdm(users, "Creating users..."):
        new_user = User(username=username, password="password", email="generic@mail.com", sex="F")
        user_lst.append(new_user)
    User.objects.using(database_name).bulk_create(user_lst)
    rating_lst = []
    for rating_tuple in tqdm(ratings.itertuples(), "Creating ratings..."):
        user = User.objects.using(database_name).get(username=rating_tuple.username)
        book = Book.objects.using(database_name).get(ISBN=rating_tuple.isbn)
        new_rating = ReadBook(user=user, book=book, rating=rating_tuple.rating)
        rating_lst.append(new_rating)
    ReadBook.objects.using(database_name).bulk_create(rating_lst)


def create_movie_book_database(database_name):
    """Populate the database with the movie_book dataset"""
    logger.info("Populating the database '%s' with the movie_book dataset", database_name)
    # delete all the models in the specified database
    delete_db(database_name)
    # load ratings
    ratings_path = TEST_DATASET / "movie_book" / "ratings.csv"
    ratings = pd.read_csv(ratings_path)
    # creating the books
    books = ratings["title"].unique()
    isbn = [str(i) for i in range(1, len(books) + 1)]
    books_lst = []
    for book_title, book_isbn in zip(books, isbn):
        new_book = Book(ISBN=book_isbn, title=book_title)
        books_lst.append(new_book)
    Book.objects.using(database_name).bulk_create(books_lst)
    # create users
    users = ratings["username"].unique()
    user_lst = []
    for username in tqdm(users, "Creating users..."):
        new_user = User(username=username, password="password", email="generic@mail.com", sex="M")
        user_lst.append(new_user)
    User.objects.using(database_name).bulk_create(user_lst)
    ratings_lst = []
    for rating_tuple in tqdm(ratings.itertuples(), "Creating ratings..."):
        user = User.objects.using(database_name).get(username=rating_tuple.username)
        book = Book.objects.using(database_name).get(title=rating_tuple.title)
        new_rating = ReadBook(user=user, book=book, rating=rating_tuple.rating)
        ratings_lst.append(new_rating)
    ReadBook.objects.using(database_name).bulk_create(ratings_lst)
