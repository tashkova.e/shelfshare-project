import functools
import time


def timer(log):
    """Log the runtime of the decorated function"""

    def actual_decorator(func):
        @functools.wraps(func)
        def wrapper_timer(*args, **kwargs):
            start_time = time.perf_counter()
            value = func(*args, **kwargs)
            end_time = time.perf_counter()
            run_time = end_time - start_time
            log.info(f"Finished {func.__name__!r} in {run_time:.4f} secs")
            return value

        return wrapper_timer

    return actual_decorator
