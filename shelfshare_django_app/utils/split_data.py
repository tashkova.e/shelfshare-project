import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import logging
from typing import Tuple

import pandas as pd
from sklearn.model_selection import train_test_split

logger = logging.getLogger("utils.split_data")
from numpy.typing import ArrayLike

from shelfshare.settings import RANDOM_SEED


def split_data_by_users(read_books_df: pd.DataFrame, test_user_size: float = 0.2) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Split the data into training and test data. The test ratings are split in half for each user
        and appended to the training data.

    Args:
        read_books_df (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        test_user_size (float, optional): The percentage of users to be used for testing. Defaults to 0.2.
    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: The test data and the training data.
    """
    all_user_ids = read_books_df["user_id"].unique()
    train_user_ids, test_user_ids = train_test_split(all_user_ids, test_size=test_user_size, random_state=RANDOM_SEED)
    logger.info(
        "Splitting data for %d training users and %d test users.", len(list(train_user_ids)), len(list(test_user_ids))
    )
    # log the memory size of the dataframe
    logger.info(
        "Memory size of the dataframe: %f MB", read_books_df.memory_usage(index=True, deep=True).sum() / 1024**2
    )
    # create a data frame with all the ratings of the users with user_id in train_user_ids
    train_data = read_books_df[read_books_df["user_id"].isin(train_user_ids)]
    # create a data frame with all the ratings of the users with user_id in test_user_ids
    test_data_temp = read_books_df[read_books_df["user_id"].isin(test_user_ids)]
    # add a bool column to the test data, which is False if the rating is 0
    test_data_temp.loc[:, ["is_rated"]] = test_data_temp["rating"] != 0
    # group by user_id and spit the ratings in half for the training and test data
    idx_for_train, idx_for_test = [], []
    for _, ratings in test_data_temp.groupby("user_id"):
        if ratings.groupby("is_rated").count().min()["book_id"] == 1:
            train, test = train_test_split(ratings, test_size=0.5, random_state=RANDOM_SEED)
        else:
            train, test = train_test_split(
                ratings, test_size=0.5, stratify=ratings["is_rated"], random_state=RANDOM_SEED
            )
        idx_for_train.extend(train.index)
        idx_for_test.extend(test.index)
    test_data = test_data_temp.loc[idx_for_test].drop(columns="is_rated")
    train_data = pd.concat([train_data, test_data_temp.loc[idx_for_train]]).drop(columns="is_rated")
    return test_data, train_data
