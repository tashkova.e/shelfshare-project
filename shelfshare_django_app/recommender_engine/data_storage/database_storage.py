import logging

import pandas as pd
from recommender_engine.data_storage.utils import (
    filter_by_min_number_of_ratings,
    select_ratings_by_users_with_most_rated_books,
)

logger = logging.getLogger(__name__)
from typing import Dict, List, Set, Tuple

from bookexchange.models import ReadBook
from books.models import Book
from django.db.models import Avg, F, IntegerField, QuerySet
from django.db.models.functions import Cast
from recommend_app.models import CollabSimilarity
from recommender_engine.data_storage.base_classes import BaseStorage
from users.models import User


class DatabaseStorage(BaseStorage):
    """Class for interacting with the data"""

    def load_ratings(self, min_no_ratings: int = 0) -> pd.DataFrame:
        """Load the ratings for the recommendation system. Ratings equal to 0 are not included.
        Args:
            min_no_ratings (int, optional): Minimum number of ratings per user. Defaults to 0.
        Returns:
            pd.DataFrame: Dataframe with columns user_id, book_id, rating.
        """
        logger.debug("Loading ratings from the database %s.", self.vector_db_name)
        columns = ["user_id", "book_id", "rating"]
        ratings_data = ReadBook.objects.using(self.vector_db_name).all().exclude(rating="0").values_list(*columns)
        if len(ratings_data) == 0:
            return pd.DataFrame(columns=columns)
        ratings = pd.DataFrame.from_records(ratings_data, columns=columns)
        ratings["rating"] = ratings["rating"].astype(float)
        ratings = filter_by_min_number_of_ratings(ratings, min_no_ratings)
        return ratings

    def top_rated_books_qs(self, top_n: int = 6) -> QuerySet:
        """Return the top rated books.
        Args:
            to_n (int, optional): Number of books to return. Defaults to 6.
        Returns:
            QuerySet: Queryset with values book_id, rating.
        """
        logger.debug("Loading the top %d rated books for %s.", top_n, self.vector_db_name)
        top_books_qs = (
            ReadBook.objects.using(self.vector_db_name)
            .exclude(rating="0")
            .annotate(rating_int=Cast("rating", output_field=IntegerField()))
            .values("book_id")
            .annotate(rating=Avg("rating_int"))
            .order_by("-rating")
            .values("book_id", "rating")
        )
        if top_n != -1:
            top_books_qs = top_books_qs[:top_n]
        return top_books_qs

    def top_rated_books(self, top_n: int = 6) -> List[Tuple[int, int]]:
        """Return the top rated books.

        Args:
            top_n (int, optional): Number of books to return. Defaults to 6.

        Returns:
            List[Dict[int, int]]: [(book_id, rating), ...]
        """
        top_books_qs = self.top_rated_books_qs(top_n)
        res_lst = []
        for dict in top_books_qs:
            book_id = dict["book_id"]
            rating = dict["rating"]
            tuple_to_add = (book_id, rating)
            res_lst.append(tuple_to_add)
        return res_lst

    def load_read_books(self, min_no_ratings: int = 0, users_with_most_books: int = -1) -> pd.DataFrame:
        """Load the read books for the recommendation system.
        Args:
            min_no_ratings (int, optional): Minimum number of ratings per user. Defaults to 0.
            users_with_most_books (int, optional): Return only the top users with most books. Return all users if -1. Defaults to -1.
        Returns:
            pd.DataFrame: Dataframe with columns user_id, book_id, rating.
        """
        logger.debug(
            "Loading read books from the database %s, min_no_ratings=%d, users_with_most_books=%d.",
            self.vector_db_name,
            min_no_ratings,
            users_with_most_books,
        )
        columns = ["user_id", "book_id", "rating"]
        read_books_data = ReadBook.objects.using(self.vector_db_name).all().values_list(*columns)
        if len(read_books_data) == 0:
            logger.debug("No read books found in the database %s.", self.vector_db_name)
            return pd.DataFrame(columns=columns)
        read_books_df = pd.DataFrame.from_records(read_books_data, columns=columns)
        read_books_df["rating"] = read_books_df["rating"].astype(float)
        read_books_df = filter_by_min_number_of_ratings(read_books_df, min_no_ratings)
        read_books_df = select_ratings_by_users_with_most_rated_books(read_books_df, users_with_most_books)
        return read_books_df

    def ratings_for_book_qs(self, book_id: int) -> QuerySet:
        """Return the ratings for book_id.
        Args:
            book_id (int): ID of the book.
        Returns:
            QuerySet: Queryset with values user_id, rating.
        """
        logger.debug("Getting the ratings for book %d, using the database %s.", book_id, self.vector_db_name)
        ratings_qs = (
            ReadBook.objects.using(self.vector_db_name)
            .filter(book_id=book_id)
            .exclude(rating="0")
            # cast the rating to integer
            .annotate(rating_int=Cast("rating", output_field=IntegerField()))
            .values("user_id")
            .annotate(rating=F("rating_int"))
            .values("user_id", "rating")
            .order_by("rating")
        )
        return ratings_qs

    def ratings_for_book(self, book_id: int) -> Dict[int, int]:
        """Return the ratings for book_id.

        Args:
            book_id (int): ID of the book

        Returns:
            Dict[int,int]: {user_id: rating}
        """
        ratings_qs = self.ratings_for_book_qs(book_id)
        result_dict = {}
        for dict in ratings_qs:
            user_id = dict["user_id"]
            rating = dict["rating"]
            result_dict[user_id] = rating
        return result_dict

    def read_books_by_user_qs(self, user_id: int) -> QuerySet:
        """Return the read books of user_id. 0 ratings are included.
        Args:
            user_id (int): ID of the user.
        Returns:
            QuerySet: Queryset with values book_id, rating.
        """
        logger.debug("Getting the read books for user %d, using the database %s.", user_id, self.vector_db_name)
        ratings = (
            ReadBook.objects.using(self.vector_db_name)
            .filter(user_id=user_id)
            .annotate(rating_int=Cast("rating", output_field=IntegerField()))
            .values("book_id")
            .annotate(rating=F("rating_int"))
            .values("book_id", "rating")
            .order_by("rating")
        )
        return ratings

    def rated_books_by_user_qs(self, user_id: int) -> QuerySet:
        """Return the rated books of user_id. 0 ratings are excluded.
        Args:
            user_id (int): ID of the user
        Returns:
            QuerySet: Queryset with values book, rating.
        """
        logger.debug("Getting the rated books for user %d, using the database %s.", user_id, self.vector_db_name)
        read_books = self.read_books_by_user_qs(user_id)
        rated_books = read_books.exclude(rating=0)
        return rated_books

    def unrated_books_by_user_qs(self, user_id: int) -> QuerySet:
        """Return the unrated books of user_id.
        Args:
            user_id (int): ID of the user
        Returns:
            QuerySet: Queryset with values book_id
        """
        logger.debug("Getting the unrated books for user %d, using the database %s.", user_id, self.vector_db_name)
        read_books = self.read_books_by_user_qs(user_id)
        unrated_books = read_books.filter(rating=0).values("book_id")
        return unrated_books

    def read_books_by_user(self, user_id: int) -> Dict[int, int]:
        """Return the read books of user_id. 0 ratings are included.

        Args:
            user_id (int): ID of the user.

        Returns:
            dict: {book_id:rating}
        """
        logger.debug("Getting the read books for user %d, using the database %s.", user_id, self.vector_db_name)
        ratings = (
            ReadBook.objects.using(self.vector_db_name)
            .filter(user_id=user_id)
            .annotate(rating_int=Cast("rating", output_field=IntegerField()))
            .values("book__id", "rating_int")
            .order_by("rating_int")
        )
        # turn the queryset into a dictionary
        rez_dict = {}
        for rating in ratings:
            rez_dict[rating["book__id"]] = rating["rating_int"]
        return rez_dict

    def rated_books_by_user(self, user_id: int) -> Dict[int, int]:
        """Return the rated books of user_id. 0 ratings are excluded.

        Args:
            user_id (int): ID of the user

        Returns:
            dict: {book_id:rating}
        """
        logger.debug("Getting the rated books for user %d, using the database %s.", user_id, self.vector_db_name)
        ratings = (
            ReadBook.objects.using(self.vector_db_name)
            .filter(user_id=user_id)
            .exclude(rating="0")
            .annotate(rating_int=Cast("rating", output_field=IntegerField()))
            .values("book__id", "rating_int")
            .order_by("rating_int")
        )
        # turn the queryset into a dictionary
        rez_dict = {}
        for rating in ratings:
            rez_dict[rating["book__id"]] = rating["rating_int"]
        return rez_dict

    def unrated_books_by_user(self, user_id: int) -> Set[int]:
        """Return the unrated books of user_id.

        Args:
            user_id (int): ID of the user

        Returns:
            set: {book_id}
        """
        logger.debug("Getting the unrated books for user %d, using the database %s.", user_id, self.vector_db_name)
        ratings = ReadBook.objects.using(self.vector_db_name).filter(user_id=user_id, rating="0").values("book__id")
        # turn the queryset into a set
        unrated_books = set(rating["book__id"] for rating in ratings)
        return unrated_books

    def books_similar_to_book_collab(
        self,
        book_id: int,
        books_in: set | None = None,
        max_candidates: int = 100,
    ) -> QuerySet:
        """Return the books most similar to book_id using the collaborative similarity matrix.

        Args:
            book_id (int): ID of the book.
            books_in (set|None): Set of book ids to filter the results.If None, search all. Defaults to None.
            max_candidates (int, optional): Maximum number of records to get from the db. Defaults to 100.
        Returns:
            QuerySet: Queryset with values source, similarity.
        """
        logger.debug(
            "Getting books similar to book %d using the CollabSimilarity model in the %s database.",
            book_id,
            self.vector_db_name,
        )
        if books_in is None:
            starting_query = CollabSimilarity.objects.using(self.vector_db_name).filter(target=book_id)
        else:
            starting_query = CollabSimilarity.objects.using(self.vector_db_name).filter(
                target=book_id, source__in=books_in
            )
        similar_books = starting_query.order_by("-similarity").values("source", "similarity")
        return similar_books.all()[:max_candidates]

    def candidate_books_predict_score(self, sim_type: str, book_id: int, rated_books: set, max_candidates: int = 100):
        """Return the books most similar to book_id that the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or d2v).
            book_id (int): ID of the book.
            rated_books (set): {book_id}
            max_candidates (int, optional): Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, similarity.
        """
        columns = ["source", "similarity"]
        if sim_type == "collab":
            similar_books = self.books_similar_to_book_collab(
                book_id, books_in=rated_books, max_candidates=max_candidates
            )
        elif sim_type == "content":
            similar_books = self.books_similar_to_book_content(
                book_id, books_in=rated_books, max_candidates=max_candidates
            )
        else:
            raise ValueError(f"Invalid sim_type: {sim_type}")
        if len(similar_books) == 0:
            return pd.DataFrame(columns=columns)
        # turn the queryset into a dataframe
        similar_books_df = pd.DataFrame.from_records(similar_books.values(*columns))
        return similar_books_df

    def candidate_books_recommend_items_collab(
        self,
        rated_books: set,
        unrated_books: set,
        max_candidates: int = 100,
    ) -> QuerySet:
        """Return the most similar books based on the previous interactions of the user, using
        the collaborative similarity matrix.
        Args:
            rated_books (set): {book_id}
            unrated_books (set): {book_id}
            max_candidates (int, optional): Maximum number of records. Defaults to 100.
        Returns:
            QuerySet: Queryset with values source,target,similarity.
        """
        logger.debug(
            "Getting candidate books for the user using the CollabSimilarity model in the %s database.",
            self.vector_db_name,
        )
        invalid_ids = rated_books | unrated_books
        candidate_books = (
            CollabSimilarity.objects.using(self.vector_db_name)
            .filter(source__in=rated_books)
            .exclude(target__in=invalid_ids)
            .order_by("-similarity")
            .values("source", "target", "similarity")
        )
        return candidate_books.all()[:max_candidates]

    def candidate_books_recommend_items(
        self, sim_type: str, rated_books: set, unrated_books: set, max_candidates: int = 100
    ) -> pd.DataFrame:
        """Return the books most similar to the books the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or content).
            rated_books (set): {book_id}
            unrated_books (set): {book_id}
            max_candidates (int, optional):Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, target, similarity.
        """
        columns = ["source", "target", "similarity"]
        if sim_type == "collab":
            candidate_books = self.candidate_books_recommend_items_collab(rated_books, unrated_books, max_candidates)
        elif sim_type == "content":
            candidate_books = self.candidate_books_recommend_items_content(rated_books, unrated_books, max_candidates)
        else:
            raise ValueError(f"Invalid sim_type: {sim_type}")
        if len(candidate_books) == 0:

            return pd.DataFrame(columns=columns)
        # turn the queryset into a dataframe
        candidate_books_df = pd.DataFrame.from_records(candidate_books.values(*columns))
        return candidate_books_df

    def get_all_user_ids(self) -> Set[int]:
        """Return all the user ids.

        Returns:
            Set[int]: {user_id}
        """
        logger.debug("Getting all the user ids from the database %s.", self.vector_db_name)
        return set(User.objects.using(self.vector_db_name).values_list("id", flat=True))

    def get_all_book_ids(self) -> Set[int]:
        """Return all the book ids.
        Returns:
            Set[int]: {book_id}
        """
        return set(Book.objects.using(self.vector_db_name).values_list("id", flat=True))
