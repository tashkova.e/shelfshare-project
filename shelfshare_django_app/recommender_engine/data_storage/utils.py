import logging

import pandas as pd

logger = logging.getLogger(__name__)


def remove_0_ratings(read_books: pd.DataFrame) -> pd.DataFrame:
    """Remove the ratings equal to 0.
    Args:
        read_books (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
    Returns:
        pd.DataFrame: Dataframe with columns user_id, book_id, rating.
    """
    return read_books[read_books["rating"] != 0]


def filter_by_min_number_of_ratings(read_books: pd.DataFrame, min_no_ratings: int) -> pd.DataFrame:
    """Filter the ratings dataframe by the minimum number of ratings per user.
    Args:
        read_books (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        min_no_ratings (int): Minimum number of ratings per user.
    Returns:
        pd.DataFrame: Dataframe with columns user_id, book_id, rating.
    """
    logger.debug("Filtering users with at least %d ratings.", min_no_ratings)
    if min_no_ratings == 0:
        return read_books
    num_ratings_per_user = read_books.groupby("user_id")["rating"].apply(lambda x: int(sum(x != 0)))
    valid_users = num_ratings_per_user[num_ratings_per_user >= min_no_ratings].index
    ratings = read_books[read_books["user_id"].isin(valid_users)]
    # make sure that the ratings and ratings_new are the same
    return ratings


def select_ratings_by_users_with_most_rated_books(
    read_books_df: pd.DataFrame, users_with_most_books: int = -1
) -> pd.DataFrame:
    """Select the ratings for the users with the most rated books.
    Args:
        read_books_df (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        users_with_most_books (int, optional): Return only the top users with most books. Return all users if -1. Defaults to -1.
    Returns:
        pd.DataFrame: Dataframe with columns user_id, book_id, rating.
    """
    if users_with_most_books != -1:
        logger.debug("Using only the %d users with the most ratings.", users_with_most_books)
        read_books_df["is_rated"] = read_books_df["rating"] != 0
        # sum the values of is_rated for each user
        user_count = read_books_df.groupby("user_id")["is_rated"].sum().reset_index()
        top_users = user_count.nlargest(users_with_most_books, "is_rated")["user_id"]
        read_books_df = read_books_df[read_books_df["user_id"].isin(top_users)]
        # remove the is_rated column
        read_books_df = read_books_df.drop(columns=["is_rated"])
    else:
        logger.debug("Using all users.")
    return read_books_df
