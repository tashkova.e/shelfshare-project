import logging
from abc import ABC, abstractmethod

from sklearn.pipeline import Pipeline

logger = logging.getLogger(__name__)
from typing import Dict, List, Set, Tuple

import pandas as pd
from books.models import Book
from django.db.models import F, IntegerField, QuerySet, Value
from pgvector.django import CosineDistance
from tqdm import tqdm


class BaseStorage(ABC):
    """Base class for all the data providers."""

    def __init__(self, vector_db_name: str = "default"):
        logger.debug("BaseStorage object created with vector_db_name: %s", vector_db_name)
        self._vector_db_name = vector_db_name

    @property
    def fwls_model(self) -> Pipeline:
        """Get the FWLS model."""
        return self._fwls_model

    @fwls_model.setter
    def fwls_model(self, fwls_model: Pipeline):
        self._fwls_model = fwls_model

    @property
    def vector_db_name(self) -> str:
        """Get the vector database name. This is where the books and vectors for them should be stored."""
        return self._vector_db_name

    @abstractmethod
    def load_ratings(self, min_no_ratings: int = 0) -> pd.DataFrame:
        """Load the ratings for the recommendation system. Ratings equal to 0 are not included.
        Args:
            min_no_ratings (int, optional): Minimum number of ratings per user. Defaults to 0.
        Returns:
            pd.DataFrame: Dataframe with columns user_id, book_id, rating.
        """

    @abstractmethod
    def load_read_books(self, min_no_ratings: int = 0, users_with_most_books: int = -1) -> pd.DataFrame:
        """Load the read books for the recommendation system.
        Args:
            min_no_ratings (int, optional): Minimum number of ratings per user. Defaults to 0.
            users_with_most_books (int, optional): Return only the top users with most books. Return all users if -1. Defaults to -1.
        Returns:
            pd.DataFrame: Dataframe with columns user_id, book_id, rating.
        """

    @abstractmethod
    def rated_books_by_user(self, user_id: int) -> Dict[int, int]:
        """Return the rated books of user_id. 0 ratings are excluded.

        Args:
            user_id (int): ID of the user

        Returns:
            dict: {book_id:rating}
        """

    @abstractmethod
    def unrated_books_by_user(self, user_id: int) -> Set[int]:
        """Return the unrated books of user_id.

        Args:
            user_id (int): ID of the user

        Returns:
            set: {book_id}
        """

    @abstractmethod
    def candidate_books_recommend_items(
        self, sim_type: str, rated_books: set, unrated_books: set, max_candidates: int = 100
    ) -> pd.DataFrame:
        """Return the books most similar to the books the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or content).
            rated_books (set): {book_id}
            unrated_books (set): {book_id}
            max_candidates (int, optional):Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, target, similarity.
        """

    @abstractmethod
    def candidate_books_predict_score(
        self, sim_type: str, book_id: int, rated_books: set, max_candidates: int = 100
    ) -> pd.DataFrame:
        """Return the books most similar to book_id that the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or d2v).
            book_id (int): ID of the book.
            rated_books (set): {book_id}
            max_candidates (int, optional): Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, similarity.
        """

    @abstractmethod
    def top_rated_books(self, top_n: int = 6) -> List[Tuple[int, int]]:
        """Return the top rated books.

        Args:
            top_n (int, optional): Number of books to return. Defaults to 6.

        Returns:
            List[Dict[int, int]]: [(book_id, rating), ...]
        """

    @abstractmethod
    def ratings_for_book(self, book_id: int) -> Dict[int, int]:
        """Return the ratings for book_id.

        Args:
            book_id (int): ID of the book

        Returns:
            Dict[int,int]: {user_id: rating}
        """

    @abstractmethod
    def get_all_user_ids(self) -> Set[int]:
        """Return all the user ids.

        Returns:
            Set[int]: {user_id}
        """

    @abstractmethod
    def get_all_book_ids(self) -> Set[int]:
        """Return all the book ids.

        Returns:
            Set[int]: {book_id}
        """

    @abstractmethod
    def read_books_by_user(self, user_id: int) -> Dict[int, int]:
        """Return the read books of user_id. 0 ratings are included.

        Args:
            user_id (int): ID of the user.

        Returns:
            dict: {book_id:rating}
        """

    # methods
    def candidate_books_recommend_items_content(
        self,
        rated_books: set,
        unrated_books: set,
        max_candidates: int = 100,
    ) -> QuerySet:
        """Return the most similar books based on the previous interactions of the user, using
        the vector similarity between the embeddings.
        The targets are unread books. The source is the book the user has rated.
        Args:
            rated_books (set): {book_id}
            unrated_books (set): {book_id}
            max_candidates (int, optional): Maximum number of records to get from the db. Defaults to 100.
        Returns:
            QuerySet: Queryset with values source, target, similarity.
        """
        logger.debug(
            "Getting candidate books for the user using the similarity between the embeddings in the Book model, in the %s database.",
            self.vector_db_name,
        )
        rated_books_qs = Book.objects.using(self.vector_db_name).filter(id__in=rated_books)
        if len(rated_books_qs) == 0:
            return rated_books_qs
        read_books = rated_books | unrated_books
        max_candidates_per_book = max_candidates // len(rated_books_qs) + 1
        query_set_lst = []
        for rated_book in tqdm(rated_books_qs, desc="Going through rated books"):
            rated_book_embedding = rated_book.embedding
            rated_book_id = rated_book.id
            similarity = 1 - CosineDistance("embedding", rated_book_embedding)
            books_similar_to_rated_books = (  # the id is the target
                Book.objects.using(self.vector_db_name)
                .exclude(id__in=read_books)
                .annotate(similarity=similarity)
                .filter(similarity__gt=0)
                .order_by("-similarity")
            ).all()[:max_candidates_per_book]
            books_similar_to_rated_books = (
                books_similar_to_rated_books.values("id", "similarity")
                .annotate(target=F("id"))
                .annotate(source=Value(rated_book_id, output_field=IntegerField()))
                .values("source", "target", "similarity")
            )
            query_set_lst.append(books_similar_to_rated_books)
        if len(query_set_lst) == 1:
            return query_set_lst[0]
        # join the querysets
        candidate_books = QuerySet.union(*query_set_lst)
        # sort and limit
        candidate_books = candidate_books.order_by("-similarity")[:max_candidates]
        return candidate_books

    def books_similar_to_book_content(
        self,
        book_id: int,
        books_in: set | None = None,
        max_candidates: int = 100,
    ) -> QuerySet:
        """Return the books most similar to book_id using the vector similarity between the embeddings.
        Args:
            book_id (int): ID of the book.
            books_in (set|None): Set of book ids to filter the results.If None, search all. Defaults to None.
            max_candidates (int, optional): Maximum number of records to get from the db. Defaults to 100.
        Returns:
            QuerySet: Queryset with values source, similarity.
        """
        logger.debug(
            "Getting books similar to book %d using the similarity between the embeddings in the Book model, in the %s database.",
            book_id,
            self.vector_db_name,
        )
        book = Book.objects.using(self.vector_db_name).get(id=book_id)
        book_embedding = book.embedding
        if books_in is None:
            starting_query = Book.objects.using(self.vector_db_name).exclude(id=book_id)
        else:
            starting_query = Book.objects.using(self.vector_db_name).exclude(id=book_id).filter(id__in=books_in)
        second_query = (
            starting_query.annotate(similarity=1 - CosineDistance("embedding", book_embedding))
            .filter(similarity__gt=0)
            .order_by("-similarity")
        ).all()[:max_candidates]
        similar_books = (
            second_query.values("id", "similarity")
            # rename id to source
            .annotate(source=F("id")).values("source", "similarity")
        )
        return similar_books
