import logging

import pandas as pd

logger = logging.getLogger(__name__)
from typing import Dict, List, Set, Tuple

from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.utils import (
    filter_by_min_number_of_ratings,
    remove_0_ratings,
    select_ratings_by_users_with_most_rated_books,
)
from tqdm import tqdm


class FileStorage(BaseStorage):
    """Class for loading data from files."""

    def __init__(self, vector_db_name: str = "default", user_item_df: pd.DataFrame | None = None):
        """Initialize the file storage with the paths to the ratings and vector db.
        The paths should be csv files.
        """
        super().__init__(vector_db_name)
        self._user_item_df = None
        self._rated_books_by_user_cache = {}
        self._ratings_for_book_cache = {}
        self._unrated_books_by_user_cache = {}
        if user_item_df is not None:
            self.user_item_df = user_item_df
        self.collab_sim_matrix = None  # dataframe with columns source, target, similarity

    @property
    def user_item_df(self) -> pd.DataFrame | None:
        """Return the ratings dataframe."""
        return self._user_item_df

    @user_item_df.setter
    def user_item_df(self, user_item_df: pd.DataFrame):
        logger.debug("Setting the ratings dataframe.")
        if not isinstance(user_item_df, pd.DataFrame):
            raise ValueError("user_item_df should be a pandas dataframe.")
        # check if the columns are correct
        if not all(col in user_item_df.columns for col in ["user_id", "book_id", "rating"]):
            raise ValueError("user_item_df should have columns user_id, book_id and rating.")
        self._user_item_df = user_item_df[["user_id", "book_id", "rating"]]
        # convert the rating column to float
        self._user_item_df.loc[:, ["rating"]] = self._user_item_df.loc[:, ["rating"]].astype(float)
        # convert the user_id and book_id to int
        self._user_item_df.loc[:, ["user_id"]] = self._user_item_df.loc[:, ["user_id"]].astype(int)
        self._user_item_df.loc[:, ["book_id"]] = self._user_item_df.loc[:, ["book_id"]].astype(int)
        if len(self._rated_books_by_user_cache) != 0:
            logger.info("Cleaning rated_books_by_user_cache")
        self._rated_books_by_user_cache = {}
        if len(self._ratings_for_book_cache) != 0:
            logger.info("Cleaning ratings_for_book_cache")
        if len(self._unrated_books_by_user_cache) != 0:
            logger.info("Cleaning unrated_books_by_user_cache")
        self._ratings_for_book_cache = {}

    def populate_ratings_by_user_cache(
        self,
    ) -> None:
        """Populate the cache with all the ratings for each user."""
        if self.user_item_df is None:
            raise ValueError("Ratings path is not set.")
        for user_id, df in tqdm(self.user_item_df.groupby("user_id"), desc="Populating ratings by user cache"):
            zero_ratings_mask = df["rating"] == 0
            zero_ratings_df = df[zero_ratings_mask]
            non_zero_ratings_df = df[~zero_ratings_mask]
            zero_ratings_set = set(zero_ratings_df["book_id"].values)
            non_zero_ratings_dict = dict(zip(non_zero_ratings_df["book_id"], non_zero_ratings_df["rating"]))
            self._rated_books_by_user_cache[user_id] = non_zero_ratings_dict
            self._unrated_books_by_user_cache[user_id] = zero_ratings_set

    def populate_ratings_for_book_cache(
        self,
    ) -> None:
        """Populate the cache with all the ratings for each book."""
        ratings = self.load_ratings(min_no_ratings=1)
        for book_id, df in ratings.groupby("book_id"):
            ratings_dict = dict(zip(df["user_id"], df["rating"]))
            self._ratings_for_book_cache[book_id] = ratings_dict

    def load_ratings(self, min_no_ratings: int = 0) -> pd.DataFrame:
        if self.user_item_df is None:
            raise ValueError("user_item_df is not set.")
        ratings = self.user_item_df
        # remove ratings equal to "0"
        ratings = remove_0_ratings(ratings)
        if min_no_ratings <= 1:
            return ratings
        ratings = filter_by_min_number_of_ratings(ratings, min_no_ratings)
        return ratings

    def load_read_books(self, min_no_ratings: int = 0, users_with_most_books: int = -1) -> pd.DataFrame:
        if self.user_item_df is None:
            raise ValueError("user_item_df is not set.")
        read_books_df = self.user_item_df
        read_books_df = filter_by_min_number_of_ratings(read_books_df, min_no_ratings)
        read_books_df = select_ratings_by_users_with_most_rated_books(read_books_df, users_with_most_books)
        return read_books_df

    def rated_books_by_user(self, user_id: int) -> Dict[int, int]:
        user_id = int(user_id)
        if self._rated_books_by_user_cache.get(user_id, None) is None:
            logger.debug("No cached ratings for user %i", user_id)
            ratings = self.load_ratings(min_no_ratings=1)
            rated_books = ratings[ratings["user_id"] == user_id]
            # remove the books with rating 0
            rated_books = rated_books[rated_books["rating"] != 0]
            rez = dict(zip(rated_books["book_id"], rated_books["rating"]))
            self._rated_books_by_user_cache[user_id] = rez
        else:
            logger.debug("We have cached ratings for user %i", user_id)
            rez = self._rated_books_by_user_cache[user_id]
        return rez

    def unrated_books_by_user(self, user_id: int) -> Set[int]:
        user_id = int(user_id)
        if self._unrated_books_by_user_cache.get(user_id, None) is None:
            logger.debug("No cached unrated books for user %i", user_id)
            read_books = self.read_books_by_user(user_id)
            unrated_books = set()
            for book_id, rating in read_books.items():
                if rating == 0:
                    unrated_books.add(book_id)
            self._unrated_books_by_user_cache[user_id] = unrated_books
        else:
            logger.debug("We have cached unrated books for user %i", user_id)
            unrated_books = self._unrated_books_by_user_cache[user_id]
        return unrated_books

    def top_rated_books(self, top_n: int = 6) -> List[Tuple[int, float]]:
        """Return the top rated books.
        Args:
            to_n (int, optional): Number of books to return. Defaults to 6.
        Returns:
            List[Tuple[int, float]]: List of tuples with book_id and rating.
        """
        ratings = self.load_ratings(min_no_ratings=1)
        top_rated_books_df = (
            ratings.groupby("book_id")
            .mean()
            .sort_values("rating", ascending=False)
            .reset_index()
            .loc[:, ["book_id", "rating"]]
        )
        if top_n != -1:
            top_rated_books_df = top_rated_books_df.head(top_n)
        # convert to list of tuples
        top_rated_books = list(top_rated_books_df.itertuples(index=False, name=None))
        return top_rated_books

    def ratings_for_book(self, book_id: int) -> Dict[int, int]:
        book_id = int(book_id)
        if self._ratings_for_book_cache.get(book_id, None) is None:
            logger.debug("No cached ratings for book %i", book_id)
            ratings = self.load_ratings(min_no_ratings=1)
            ratings_for_book = ratings[ratings["book_id"] == book_id]
            rez = dict(zip(ratings_for_book["user_id"], ratings_for_book["rating"]))
            self._ratings_for_book_cache[book_id] = rez
        else:
            logger.debug("We have cached ratings for book %i", book_id)
            rez = self._ratings_for_book_cache[book_id]
        return rez

    def get_all_user_ids(self) -> Set[int]:
        if self.user_item_df is None:
            raise ValueError("Ratings path is not set.")
        return set(self.user_item_df["user_id"].unique())

    def get_all_book_ids(self) -> Set[int]:
        if self.user_item_df is None:
            raise ValueError("Ratings path is not set.")
        return set(self.user_item_df["book_id"].unique())

    def read_books_by_user(self, user_id: int) -> Dict[int, int]:
        if self.user_item_df is None:
            raise ValueError("Ratings path is not set.")
        ratings = self.user_item_df
        user_id = int(user_id)
        read_books = ratings[ratings["user_id"] == user_id]
        return dict(zip(read_books["book_id"], read_books["rating"]))

    def candidate_books_recommend_items(
        self, sim_type: str, rated_books: set, unrated_books: set, max_candidates: int = 100
    ) -> pd.DataFrame:
        """Return the books most similar to the books the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or content).
            rated_books (set): {book_id}
            unrated_books (set): {book_id}
            max_candidates (int, optional):Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, target, similarity.
        """
        columns = ["source", "target", "similarity"]

        if sim_type == "collab":
            if self.collab_sim_matrix is None:
                raise ValueError("Collaborative similarity matrix is not set.")
            sim_matrix = self.collab_sim_matrix
            # get the books that the user has not rated, but are similar to the books the user has rated
            df_filtered = sim_matrix.loc[sim_matrix.source.isin(rated_books)]
            invalid_ids = rated_books | unrated_books
            candidate_items = df_filtered.loc[~df_filtered.target.isin(invalid_ids)]
            # sort the candidate items by the similarity
            candidate_items = candidate_items.sort_values(by="similarity", ascending=False).head(max_candidates)
            return candidate_items
        if sim_type == "content":
            candidate_items_qs = self.candidate_books_recommend_items_content(
                rated_books, unrated_books, max_candidates
            )
            # turn the queryset into a dataframe
            if len(candidate_items_qs) == 0:
                return pd.DataFrame(columns=columns)
            candidate_items_df = pd.DataFrame.from_records(candidate_items_qs.values(*columns))
            return candidate_items_df

        raise ValueError("Unknown similarity type.")

    def candidate_books_predict_score(
        self, sim_type: str, book_id: int, rated_books: set, max_candidates: int = 100
    ) -> pd.DataFrame:
        """Return the books most similar to book_id that the user has rated.

        Args:
            sim_type (str): Type of similarity matrix (collab or d2v).
            book_id (int): ID of the book.
            rated_books (set): {book_id}
            max_candidates (int, optional): Maximum number of records. Defaults to 100.

        Returns:
            pd.DataFrame: Dataframe with columns source, similarity.
        """
        columns = ["source", "similarity"]
        if sim_type == "collab":
            if self.collab_sim_matrix is None:
                raise ValueError("Collaborative similarity matrix is not set.")
            sim_matrix = self.collab_sim_matrix
            df_filtered = sim_matrix.loc[sim_matrix.target == book_id]
            candidate_items = (
                df_filtered.loc[df_filtered.source.isin(rated_books)]
                .sort_values(by="similarity", ascending=False)
                .head(max_candidates)
            )  # this can be empty if there are no similar books
            candidate_items = candidate_items.loc[:, columns]
            return candidate_items
        if sim_type == "content":
            similar_books_qs = self.books_similar_to_book_content(book_id, rated_books, max_candidates)
            # turn the queryset into a dataframe
            if len(similar_books_qs):
                return pd.DataFrame(columns=columns)
            similar_books_df = pd.DataFrame.from_records(similar_books_qs.values(*columns))
            return similar_books_df
        raise ValueError("Unknown similarity type.")
