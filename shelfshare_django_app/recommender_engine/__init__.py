import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
