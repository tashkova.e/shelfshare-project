"""This script contains functions for calculating different metrics for evaluating the recommenders"""

import logging
from typing import List, Tuple

import dask.dataframe as dd
from recommender_engine.recommenders.base_classes import BaseRecommender
from tqdm import tqdm

logger = logging.getLogger(__name__)
import pandas as pd
from tqdm import tqdm
from utils.time import timer


@timer(logger)
def mean_absolute_error(recommender: BaseRecommender, test_ratings: pd.DataFrame):
    """Calculate the mean absolute error for some recommender, on some test data.
    For each user, the mean absolute error is calculated and then averaged over all users.

    Args:
        recommender (BaseRecommender): recommender to be evaluated
        test_ratings (pd.DataFrame): DataFrame with the test ratings. It should have a user_id as an index and book_id and rating as columns.
    Returns:
        dask dataframe: mean absolute error
    """
    # convert to dask dataframe
    test_user_ids = test_ratings.index.unique()
    logger.info("Calculating MAE for %d users", len(test_user_ids))
    if len(test_user_ids) == 0:
        logger.warning("No test users found")
        return 0.0
    test_ratings_grouped = test_ratings.groupby(test_ratings.index)
    mae_per_user = test_ratings_grouped.apply(lambda group: mae_for_user(group, recommender))
    rez = mae_per_user.mean()
    return rez


@timer(logger)
def mean_absolute_error_dask(recommender: BaseRecommender, test_ratings: pd.DataFrame):
    """Calculate the mean absolute error for some recommender, on some test data.
    For each user, the mean absolute error is calculated and then averaged over all users.

    Args:
        recommender (BaseRecommender): recommender to be evaluated
        test_ratings (pd.DataFrame): DataFrame with the test ratings. It should have a user_id as an index and book_id and rating as columns.
    Returns:
        dask dataframe: mean absolute error, not computed yet
    """
    # convert to dask dataframe
    test_user_ids = test_ratings.index.unique()
    test_ratings = dd.from_pandas(test_ratings, npartitions=4)
    logger.info("Calculating MAE for %d users", len(test_user_ids))
    if len(test_user_ids) == 0:
        logger.warning("No test users found")
        return 0.0
    mae_per_user = test_ratings.groupby(test_ratings.index).apply(
        lambda group: mae_for_user(group, recommender), meta=("mae", "float")
    )
    rez = mae_per_user.mean()
    return rez


def mae_for_user(group: pd.DataFrame, recommender: BaseRecommender) -> float:
    """Calculate the mean absolute error for a user.

    Args:
        group (pd.DataFrame): Ratings for a user, by a groupby operation.
        recommender (BaseRecommender): recommender to be evaluated.

    Returns:
        float: mean absolute error for the user.
    """
    user_id = int(group.name)
    group["predicted_rating"] = group["book_id"].apply(lambda book_id: recommender.predict_score(user_id, book_id))
    group["error"] = abs(group["rating"] - group["predicted_rating"])
    rez = group["error"].mean()
    logger.debug("User %d MAE: %f", user_id, rez)
    return rez


def mean_average_precision_at_k(recommender: BaseRecommender, test_ratings: pd.DataFrame, k: int) -> float:
    """Calculate the mean average precision at k for the recommender.
    For each user, the average precision at k is calculated and then averaged over all users.

    Args:
        recommender (BaseRecommender): recommender to be evaluated
        test_ratings (pd.DataFrame): DataFrame with the test ratings. It should have a user_id as an index and book_id and rating as columns.
        k (int): Number of recommendations to get from the recommender.

    Returns:
        float: mean average precision at k.
    """
    # get the test ratings
    test_user_ids = test_ratings.index.unique()
    logger.info("Calculating MAP@%d for %d users", k, len(test_user_ids))
    if len(test_user_ids) == 0:
        logger.warning("No test users found")
        return 0.0
    users_apk = (
        test_ratings.groupby(test_ratings.index)
        .apply(lambda group: apk_for_user(group, k, recommender))
        .reset_index(name="apk_per_user")
    )
    mapk = users_apk["apk_per_user"].mean()
    logger.info("Mean average precision at %d: %f", k, mapk)
    return mapk


def apk_for_user(group: pd.DataFrame, k: int, recommender: BaseRecommender) -> float:
    """Calculate the average precision at k for a user.

    Args:
        group (pd.DataFrame): Ratings for a user, by a groupby operation.
        k (int): Value of k for the average precision at k.
        recommender (BaseRecommender): recommender to be evaluated.

    Returns:
        float: average precision at k for the user.
    """

    user_id = int(group.name)
    recs = recommender.recommend_items(user_id, k)
    relevant = set(group["book_id"].values)
    apk = average_precision_k(recs, relevant)
    logger.debug("User %d MAP@%d: %f", user_id, k, apk)
    return average_precision_k(recs, relevant)


def average_precision_k(recs: List[Tuple], relevant: set) -> float:
    """Calculate the average precision at k for the recommendations.
        Return 0 if no items were recommended.

    Args:
        recs (List[Tuple]): recommendations, sorted by relevance, output of the recommender
        relevant (set): books that the user has liked or read.

    Returns:
        float: average precision at k
    """
    if len(recs) == 0 or len(relevant) == 0:
        return 0.0
    score = 0.0
    num_hits = 0

    for i, prediction in enumerate(recs):
        TP = prediction[0] in relevant
        if TP:
            num_hits += 1.0
        score += num_hits / (i + 1.0)
    return score / min(len(recs), len(relevant))


@timer(logger)
def mean_average_precision_at_multiple_k(
    recommender: BaseRecommender, test_ratings: pd.DataFrame, k_list: List[int]
) -> List[float]:
    """Calculate the mean average precision at k for the recommender for multiple k values.
    For each user, the average precision at k is calculated and then averaged over all users.
    We calculate the MAP@k for each k in the k_list, starting from the largest k, so we can reuse the recommendations.

    Args:
        recommender (BaseRecommender): recommender to be evaluated
        test_ratings (pd.DataFrame): DataFrame with the test ratings. It should have a user_id as an index and book_id and rating as columns.
        k_list (List[int]): List of values for k.
    Returns:
        List[float]: list of mean average precision at k.
    """
    # get the test ratings
    test_user_ids = test_ratings.index.unique()
    if len(k_list) == 0:
        logger.warning("No k values provided")
        return []
    if len(test_user_ids) == 0:
        logger.warning("No test users found")
        return [0.0] * len(k_list)
    logger.info("Calculating MAP for %d users, for the following k values: %s", len(test_user_ids), k_list)
    # find the maximum k
    max_k = max(k_list)
    rated_books_by_user = test_ratings.groupby(test_ratings.index)["book_id"].apply(set)
    # convert the series to a df with the user_id as index
    user_info_df = pd.DataFrame({"rated_books_set": rated_books_by_user})
    # get the recommendations for the maximum k
    user_info_df["recs_for_max_k"] = user_info_df.apply(
        lambda row: recommender.recommend_items(int(row.name), max_k), axis=1
    )
    user_info_df["len_of_recs"] = user_info_df["recs_for_max_k"].apply(len)
    for k in tqdm(k_list, "Going over all k values"):
        user_info_df[f"apk_for_{k}"] = user_info_df.apply(
            lambda row: average_precision_k(
                row["recs_for_max_k"][: min(k, row["len_of_recs"])], row["rated_books_set"]
            ),
            axis=1,
        )
    # get the mean of the apk values for each k
    map_values = user_info_df[[f"apk_for_{k}" for k in k_list]].mean()
    # convert the series to a list
    map_values_list = []
    for k in k_list:
        map_values_list.append(map_values[f"apk_for_{k}"])
    return map_values_list
