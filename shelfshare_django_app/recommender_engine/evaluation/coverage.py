import logging

from recommender_engine.recommenders.base_classes import BaseRecommender

logger = logging.getLogger("recommender_engine_parallel.coverage")
from collections import defaultdict
from typing import Tuple

from tqdm import tqdm


def calculate_coverage(recommender: BaseRecommender, num: int = 6) -> Tuple[float, float]:
    """How many users have been recommended items and how many items have been recommended.

    Args:
        recommender (BaseRecommender): Recommender to calculate coverage for
        num (int, optional): Number of items to recommend. Defaults to 6.

    Returns:
        Tuple[float,float]: user_coverage,book_coverage
    """
    # get the storage from the recommender
    recommender_name = recommender.__class__.__name__
    logger.info("Calculating coverage for %s", recommender_name)
    storage = recommender.storage
    all_users = storage.get_all_user_ids()
    all_books = storage.get_all_book_ids()
    users_with_recs = []
    items_in_rec = defaultdict(int)
    for user_id in tqdm(all_users, "Going over all users"):
        rec_set = recommender.recommend_items(user_id, num)
        if len(rec_set) > 0:
            users_with_recs.append(user_id)
            for rec in rec_set:
                items_in_rec[rec[0]] += 1
    num_books = len(all_books)
    num_books_in_rec = len(items_in_rec)
    num_users = len(all_users)
    num_users_with_recs = len(users_with_recs)
    user_coverage = float(num_users_with_recs / num_users)
    book_coverage = float(num_books_in_rec / num_books)
    logger.info("User coverage: %.2f, Book coverage: %.2f", user_coverage, book_coverage)
    return user_coverage, book_coverage
