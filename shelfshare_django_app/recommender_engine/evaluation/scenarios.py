import logging
from typing import Dict, List, Type

import numpy as np
from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.evaluation.metrics import (
    mean_absolute_error,
    mean_average_precision_at_multiple_k,
)
from recommender_engine.recommenders.base_classes import (
    BaseRecommender,
    ModelBuildingParams,
    WithModel,
)
from utils.split_data import split_data_by_users

logger = logging.getLogger(__name__)
from dataclasses import asdict

import pandas as pd


def train_test_split_one_param(
    type_of_rec: Type[BaseRecommender],
    input_storage: BaseStorage,
    k_list: List[int],
    builder_params: ModelBuildingParams | None | Dict = None,
    recommender_params: Dict | None = None,
    test_size: float = 0.2,
    min_no_ratings: int = 1,
    users_with_most_books: int = -1,
) -> Dict:
    """Evaluate a recommender by calculating the mean absolute error and the mean average precision at k for multiple k values.
    The evaluation is done by splitting the data into train and test sets, and then training the recommender on the train set and testing it on the test set.

    Args:
        type_of_rec (Type[BaseRecommender]): The type of recommender to be evaluated.
        input_storage (BaseStorage): The storage with the data to be used for evaluation.
        k_list (List[int]): The list of k values for which the mean average precision at k will be calculated.
        builder_params (ModelBuildingParams | None, optional): The parameters for building the recommender, if needed. Defaults to None.
        recommender_params (Dict | None, optional): The parameters for making the recommendations, if needed. Defaults to None.
        test_size (float, optional): Test size of users. Defaults to 0.2.
        min_no_ratings (int, optional): Minimum number of ratings per user, for him to be included in the evaluation. Defaults to 1.
        users_with_most_books (int, optional): Should you only use the top n users with most rated books. Defaults to -1.

    Returns:
        Dict: A dictionary with the mean absolute error and the mean average precision at k for each k value.
    """
    user_item_df = input_storage.load_read_books(
        min_no_ratings, users_with_most_books
    )  # these are not hyperparameters.
    vector_db_name = input_storage.vector_db_name
    all_user_ids = user_item_df["user_id"].unique()
    all_book_ids = user_item_df["book_id"].unique()
    # get only the descriptions which are in the user_item_df
    logger.info(
        "Dataset for the evaluation contains %d interactions, for %d users and %d books.",
        len(user_item_df),
        len(all_user_ids),
        len(all_book_ids),
    )
    logger.info("Evaluation for multiple k values: %s", k_list)
    logger.info(
        "Evaluating %s by testing on %d%% users",
        type_of_rec.__name__,
        test_size * 100,
    )
    test_df, train_df = split_data_by_users(user_item_df, test_size)
    # create train and test storage
    train_storage = FileStorage(vector_db_name, train_df)
    test_storage = FileStorage(vector_db_name, test_df)
    rec_to_test = type_of_rec.get_trained_recommender_with_file_storage(
        train_storage, builder_params=builder_params, recommender_params=recommender_params
    )
    # get the test ratings
    test_ratings = test_storage.load_ratings(min_no_ratings=1)  # users with no ratings are not considered
    # set an index on book_id
    test_ratings = test_ratings.set_index("user_id")
    map_k_values = mean_average_precision_at_multiple_k(rec_to_test, test_ratings, k_list)
    mea = mean_absolute_error(
        rec_to_test, test_ratings
    )  # the _rated_books_by_user_cache is full of the test user's books
    return {"mean_absolute_error": mea, "mean_average_precision_at_k": map_k_values}
