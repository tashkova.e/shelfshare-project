import logging

from books.models import Book
from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.recommenders.building_utils import (
    save_similarities_to_db,
)
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    CollaborativeFilteringRecommender,
    build_collab_item_similarity_matrix,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    FWLSRecommender,
    build_fwls_model,
)
from recommender_engine.recommenders.popularity import PopularityRecommender
from users.models import User
from utils.create_test_database import (
    create_movie_book_database,
    create_reduced_database,
)

logger = logging.getLogger(__name__)


class TestWithReducedDataset(TestCase):
    """Test the functions in this script with the test dataset 'reduced'."""

    @classmethod
    def setUpTestData(cls):
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_reduced_database(cls.test_db_name)

    def test_content_predict_score(self):
        """Test the predict_score method."""
        recommender = ContentBasedRecommender(self.storage)
        user_id = User.objects.get(username="mellowgold15").id
        book_id = Book.objects.get(title="Wall and Piece").id
        real_rating = 5.0
        prediction = recommender.predict_score(user_id, book_id)
        self.assertAlmostEqual(real_rating, prediction, places=3)

    def test_content_recommend_items(self):
        """Test the recommend_items method."""
        recommender = ContentBasedRecommender(self.storage)
        user_id = User.objects.get(username="mellowgold15").id
        num = 6
        recommendations = recommender.recommend_items(user_id, num)
        self.assertLessEqual(len(recommendations), num)
        self.assertIsInstance(recommendations, list)
        self.assertIsInstance(recommendations[0], tuple)

    def test_fwls_predict_score(self):
        """Test the predict_score method."""
        ratings_0 = self.storage.load_read_books(min_no_ratings=0)
        collab_params = CollabItemSimilarityParams(0, 0.2, True)
        fwls_params = FWLSModelBuilderParams(
            {"builder_params": collab_params}, users_sample_size=-1, valid_size=0.2, vector_db_name=self.test_db_name
        )
        # build the FWLS model
        fwls_model, collab_rec, content_rec = build_fwls_model(ratings_0, fwls_params)
        # add the FWLS model to the storage
        self.storage.fwls_model = fwls_model
        # initialize the FWLS recommender
        recommender = FWLSRecommender(self.storage, neighborhood_size_collab=6, neighborhood_size_content=6)
        user_id = User.objects.get(username="mellowgold15").id
        book_id = Book.objects.get(title="Wall and Piece").id
        real_rating = 5.0
        prediction = recommender.predict_score(user_id, book_id)
        self.assertLessEqual(abs(real_rating - prediction), 2.0)

    def test_fwls_recommend_items(self):
        """Test the recommend_items method."""
        ratings_0 = self.storage.load_read_books(min_no_ratings=0)
        collab_params = CollabItemSimilarityParams(0, 0.2, True)
        fwls_params = FWLSModelBuilderParams(
            {"builder_params": collab_params}, users_sample_size=-1, valid_size=0.2, vector_db_name=self.test_db_name
        )
        # build the FWLS model
        fwls_model, collab_rec, content_rec = build_fwls_model(ratings_0, fwls_params)
        # add the FWLS model to the storage
        self.storage.fwls_model = fwls_model
        # initialize the FWLS recommender
        recommender = FWLSRecommender(self.storage, neighborhood_size_collab=6, neighborhood_size_content=6)
        user_id = User.objects.get(username="mellowgold15").id
        num = 6
        recommendations = recommender.recommend_items(user_id, num)
        self.assertLessEqual(len(recommendations), num)
        self.assertIsInstance(recommendations, list)
        self.assertIsInstance(recommendations[0], tuple)


class TestWithMovieBookDataset(TestCase):
    """Test the functions in this script with the test dataset 'movie_book'."""

    @classmethod
    def setUpTestData(cls):
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_movie_book_database(cls.test_db_name)
        cls.ratings = cls.storage.load_ratings(min_no_ratings=0)
        # build the collab similarity matrix
        collab_params = CollabItemSimilarityParams(0, 0.2, True)
        sim_matrix = build_collab_item_similarity_matrix(cls.ratings, model_params=collab_params)
        save_similarities_to_db(sim_matrix, cls.test_db_name)

    def test_popularity_predict_score(self):
        """Test the predict_score method."""
        recommender = PopularityRecommender(self.storage)
        user_id = User.objects.get(username="user10").id
        book_id = Book.objects.get(title="dr_strangelove").id
        real_rating = 4.0
        prediction = recommender.predict_score(user_id, book_id)
        self.assertLessEqual(abs(real_rating - prediction), 2.0)

    def test_popularity_recommend_items(self):
        """Test the recommend_items method."""
        recommender = PopularityRecommender(self.storage)
        user_id = User.objects.get(username="user10").id
        num = 6
        recommendations = recommender.recommend_items(user_id, num)
        self.assertLessEqual(len(recommendations), num)
        self.assertIsInstance(recommendations, list)
        self.assertIsInstance(recommendations[0], tuple)

    def test_collab_predict_score(self):
        """Test the predict_score method."""
        recommender = CollaborativeFilteringRecommender(self.storage, neighborhood_size=6)
        user_id = User.objects.get(username="user10").id
        book_id = Book.objects.get(title="dr_strangelove").id
        real_rating = 4.0
        prediction = recommender.predict_score(user_id, book_id)
        self.assertAlmostEqual(real_rating, prediction, places=3)

    def test_collab_recommend_items(self):
        """Test the recommend_items method."""
        recommender = CollaborativeFilteringRecommender(self.storage, neighborhood_size=6)
        user_id = User.objects.get(username="user10").id
        num = 6
        recommendations = recommender.recommend_items(user_id, num)
        self.assertLessEqual(len(recommendations), num)
        self.assertIsInstance(recommendations, list)
        self.assertIsInstance(recommendations[0], tuple)
