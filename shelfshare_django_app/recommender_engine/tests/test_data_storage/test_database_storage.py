import logging

from books.models import Book
from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from users.models import User
from utils.create_test_database import create_reduced_database

logger = logging.getLogger(__name__)

from django.db.models import QuerySet

EXPECTED_RATINGS_COLUMNS = ["user_id", "book_id", "rating"]


class TestWithOriginalDBReduced(TestCase):
    """Test the DatabaseStorage class with the test dataset 'original_db_reduced'."""

    @classmethod
    def setUpTestData(cls):
        cls.test_db_name = "default"
        create_reduced_database(cls.test_db_name)
        cls.storage = DatabaseStorage(cls.test_db_name)

    def test_books_similar_to_book_content(self):
        """Test the method books_similar_to_book_content."""
        book_id = Book.objects.first().id
        similar_books = self.storage.books_similar_to_book_content(book_id)
        self.assertGreater(len(similar_books), 0)
        # check the type of the returned object
        self.assertIsInstance(similar_books, QuerySet)
        self.assertIsInstance(similar_books[0], dict)
        # check the keys
        expected_keys = ["source", "similarity"]
        for entry in similar_books:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_books_similar_to_book_collab_untrained(self):
        """Test the method books_similar_to_book_collab, without calculating the similarities"""
        book_id = Book.objects.first().id
        similar_books = self.storage.books_similar_to_book_collab(book_id)
        self.assertEqual(len(similar_books), 0)
        # check the type of the returned object
        self.assertIsInstance(similar_books, QuerySet)

    def test_top_rated_books_qs_all(self):
        """Test the method top_rated_books_qs, with all books."""
        top_rated_books = self.storage.top_rated_books_qs(-1)
        # make sure that the number of books is the same as the number of books in the database
        self.assertEqual(top_rated_books.count(), 61)
        # check the type of the returned object
        self.assertIsInstance(top_rated_books, QuerySet)
        self.assertIsInstance(top_rated_books[0], dict)
        # check the keys
        expected_keys = ["book_id", "rating"]
        for entry in top_rated_books:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_top_rated_books_qs_10(self):
        """Test the method top_rated_books_qs, with 10 books."""
        top_rated_books = self.storage.top_rated_books_qs(10)
        # make sure that the number of books is the same as the number of books in the database
        self.assertEqual(top_rated_books.count(), 10)
        # check the type of the returned object
        self.assertIsInstance(top_rated_books, QuerySet)
        self.assertIsInstance(top_rated_books[0], dict)
        # check the keys
        expected_keys = ["book_id", "rating"]
        for entry in top_rated_books:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_top_rated_books_qs_0(self):
        """Test the method top_rated_books_qs, with 0 books."""
        top_rated_books = self.storage.top_rated_books_qs(0)
        # make sure that the number of books is the same as the number of books in the database
        self.assertEqual(top_rated_books.count(), 0)
        # check the type of the returned object
        self.assertIsInstance(top_rated_books, QuerySet)

    def test_ratings_for_book_qs(self):
        """Test the method ratings_for_book_qs."""
        book_id = Book.objects.get(ISBN="1844137872").id
        ratings_qs = self.storage.ratings_for_book_qs(book_id)
        # check the length of the returned object
        self.assertEqual(len(ratings_qs), 1)
        # check the type of the returned object
        self.assertIsInstance(ratings_qs, QuerySet)
        self.assertIsInstance(ratings_qs[0], dict)
        expected_keys = ["user_id", "rating"]
        for entry in ratings_qs:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_read_books_by_user_qs(self):
        """Test the method read_books_by_user_qs."""
        user_id = User.objects.get(username="mellowgold15").id
        read_books_qs = self.storage.read_books_by_user_qs(user_id)
        # check the length of the returned object
        self.assertEqual(len(read_books_qs), 13)
        # check the type of the returned object
        self.assertIsInstance(read_books_qs, QuerySet)
        # check the keys
        expected_keys = ["book_id", "rating"]
        for entry in read_books_qs:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_rated_books_by_user_qs(self):
        """Test the method rated_books_by_user_qs."""
        user_id = User.objects.get(username="mellowgold15").id
        rated_books_qs = self.storage.rated_books_by_user_qs(user_id)
        # check the length of the returned object
        self.assertEqual(len(rated_books_qs), 7)
        # check the type of the returned object
        self.assertIsInstance(rated_books_qs, QuerySet)
        # check the keys
        expected_keys = ["book_id", "rating"]
        for entry in rated_books_qs:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_unrated_books_by_user_qs(self):
        """Test the method unrated_books_by_user_qs."""
        user_id = User.objects.get(username="mellowgold15").id
        unrated_books_qs = self.storage.unrated_books_by_user_qs(user_id)
        # check the length of the returned object
        self.assertEqual(len(unrated_books_qs), 6)
        # check the type of the returned object
        self.assertIsInstance(unrated_books_qs, QuerySet)
        # check the keys
        expected_keys = ["book_id"]
        for entry in unrated_books_qs:
            self.assertEqual(set(entry.keys()), set(expected_keys))

    def test_load_ratings(self):
        """Test loading the ratings from the database."""
        all_ratings = self.storage.load_ratings()
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 69)
        # check if the columns are the same
        self.assertListEqual(all_ratings.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_ratings_greater_than_n(self):
        """Test loading the ratings from the database with a minimum number of ratings."""
        all_ratings = self.storage.load_ratings(min_no_ratings=11)
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 0)
