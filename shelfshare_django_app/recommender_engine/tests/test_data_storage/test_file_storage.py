import pandas as pd
from books.models import Book
from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    build_collab_item_similarity_matrix,
)
from recommender_engine.tests.test_data_storage import (
    EXPECTED_RATINGS_COLUMNS,
    EXPECTED_SIM_COLUMNS,
)
from users.models import User
from utils.create_test_database import (
    create_database_with_fake_books,
    create_movie_book_database,
    create_reduced_database,
)

from shelfshare.settings import TEST_DATASET_DIR


class TestsWithFakeBooks(TestCase):
    """Test the FileStorage class with the test dataset 'fake_books'."""

    @classmethod
    def setUpTestData(cls):
        ratings_path = TEST_DATASET_DIR / "fake_books" / "ratings.csv"
        ratings = pd.read_csv(ratings_path)
        vector_db_name = "default"
        create_database_with_fake_books(vector_db_name)
        cls.storage = FileStorage(vector_db_name=vector_db_name, user_item_df=ratings)

    def test_load_ratings(self):
        """Test loading the ratings from the database."""
        all_ratings = self.storage.load_ratings()
        self.assertIsNotNone(all_ratings)
        self.assertTrue(len(all_ratings) > 0)
        # check if the columns are the same
        self.assertListEqual(all_ratings.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_ratings_greater_than_n(self):
        """Test loading the ratings from the database with a minimum number of ratings."""
        all_ratings = self.storage.load_ratings(min_no_ratings=9)
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 0)

    def test_load_read_books(self):
        """Test loading the read books from the database."""
        read_books = self.storage.load_read_books()
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 9)
        # check if the columns are the same
        self.assertListEqual(read_books.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_read_books_with_most_books(self):
        """Test loading the read books from the database with the users with the most books."""
        read_books = self.storage.load_read_books(users_with_most_books=2)
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 6)


class TestsMovieBookDB(TestCase):
    """Test the FileStorage class with the test dataset 'movie_book'."""

    @classmethod
    def setUpTestData(cls):
        ratings_path = TEST_DATASET_DIR / "movie_book" / "ratings.csv"
        ratings_csv = pd.read_csv(ratings_path)
        cls.vector_db_name = "default"
        create_movie_book_database(cls.vector_db_name)
        # initialize a DatabaseStorage object
        cls.db_storage = DatabaseStorage(cls.vector_db_name)
        cls.storage = FileStorage(vector_db_name=cls.vector_db_name, user_item_df=ratings_csv)

    def test_load_ratings(self):
        """Test loading the ratings from the database."""
        all_ratings = self.storage.load_ratings()
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 69)
        # check if the columns are the same
        self.assertListEqual(all_ratings.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_ratings_greater_than_n(self):
        """Test loading the ratings from the database with a minimum number of ratings."""
        all_ratings = self.storage.load_ratings(min_no_ratings=8)
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 35)

    def test_load_read_books(self):
        """Test loading the read books from the database."""
        read_books = self.storage.load_read_books()
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 69)
        # check if the columns are the same
        self.assertListEqual(read_books.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_read_books_with_most_books(self):
        """Test loading the read books from the database with the users with the most books."""
        read_books = self.storage.load_read_books(users_with_most_books=2)
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 18)

    def test_read_books_by_user_10(self):
        """Test the output of the read_books_by_user method for user with id 10"""
        read_books = self.storage.read_books_by_user(10)
        self.assertIsInstance(read_books, dict)
        self.assertEqual(len(read_books), 4)
        self.assertEqual(read_books[2], 4)
        self.assertEqual(read_books[3], 3)
        self.assertEqual(read_books[1], 5)
        self.assertEqual(read_books[4], 4)

    def test_rated_books_by_user_10(self):
        """Test the output of the rated_books_by_user method for user with id 10"""
        rated_books = self.storage.rated_books_by_user(10)
        self.assertIsInstance(rated_books, dict)
        self.assertEqual(len(rated_books), 4)
        self.assertEqual(rated_books[2], 4)
        self.assertEqual(rated_books[3], 3)
        self.assertEqual(rated_books[1], 5)
        self.assertEqual(rated_books[4], 4)

    def test_unrated_books_by_user_10(self):
        """Test the output of the unrated_books_by_user method for user with id 10"""
        unrated_books = self.storage.unrated_books_by_user(10)
        self.assertIsInstance(unrated_books, set)
        self.assertEqual(len(unrated_books), 0)

    def test_ratings_for_book_cache(self):
        """Test the private attribute _ratings_for_book_cache"""
        ratings = self.storage.load_ratings(min_no_ratings=1)
        test_storage = FileStorage()
        # check if the cache is empty
        self.assertEqual(test_storage._ratings_for_book_cache, {})
        # assign the ratings to the cache
        test_storage.user_item_df = ratings
        # check if the cache is empty
        self.assertEqual(test_storage._ratings_for_book_cache, {})
        # get the ratings for book 1
        ratings_for_book_1_first_time = test_storage.ratings_for_book(1)
        # check if the ratings for book 1 are in the cache
        self.assertEqual(id(ratings_for_book_1_first_time), id(test_storage._ratings_for_book_cache[1]))
        # get the ratings for book 1 again
        ratings_for_book_1_second_time = test_storage.ratings_for_book(1)
        # check if we got the same ratings
        self.assertEqual(id(ratings_for_book_1_first_time), id(ratings_for_book_1_second_time))
        # get the ratings for book 2
        ratings_for_book_2 = test_storage.ratings_for_book(2)
        # check if the ratings for book 2 are in the cache
        self.assertEqual(id(ratings_for_book_2), id(test_storage._ratings_for_book_cache[2]))
        # reassign the ratings_df
        test_storage.user_item_df = ratings
        # check if the cache is empty
        self.assertEqual(test_storage._ratings_for_book_cache, {})

    def test_rated_books_by_user_cache(self):
        """Test the private attribute _rated_books_by_user_cache"""
        ratings = self.storage.load_ratings(min_no_ratings=1)
        test_storage = FileStorage()
        test_storage.user_item_df = ratings
        # check if the cache is empty
        self.assertEqual(test_storage._rated_books_by_user_cache, {})
        # get the rated books for user 1
        rated_books_for_user_1_first_time = test_storage.rated_books_by_user(1)
        # check if the rated books for user 1 are in the cache
        self.assertEqual(id(rated_books_for_user_1_first_time), id(test_storage._rated_books_by_user_cache[1]))
        # get the rated books for user 1 again
        rated_books_for_user_1_second_time = test_storage.rated_books_by_user(1)
        # check if we got the same rated books
        self.assertEqual(id(rated_books_for_user_1_first_time), id(rated_books_for_user_1_second_time))
        # get the rated books for user 2
        rated_books_for_user_2 = test_storage.rated_books_by_user(2)
        # check if the rated books for user 2 are in the cache
        self.assertEqual(id(rated_books_for_user_2), id(test_storage._rated_books_by_user_cache[2]))
        # reassign the ratings_df
        test_storage.user_item_df = ratings
        # check if the cache is empty
        self.assertEqual(test_storage._rated_books_by_user_cache, {})

    def test_top_rated_books_all(self):
        """Test the top_rated_books method, loading all books"""
        top_rated_books = self.storage.top_rated_books(-1)
        self.assertIsInstance(top_rated_books, list)
        self.assertEqual(len(top_rated_books), 10)

    def test_top_6_rated_books(self):
        """Test the top_rated_books method, loading 6 books"""
        top_rated_books = self.storage.top_rated_books(6)
        self.assertIsInstance(top_rated_books, list)
        self.assertEqual(len(top_rated_books), 6)

    def test_candidate_books_recommend_items_user10_collab(self):
        """Test the output of the candidate_books_recommend_items method for user with name user10"""
        # the ratings in user_item_df have to point to the same books as in the database"
        read_books = self.db_storage.load_read_books()
        # initialize a FileStorage object
        file_storage = FileStorage(vector_db_name=self.vector_db_name, user_item_df=read_books)

        # build the similarity matrix
        collab_params = CollabItemSimilarityParams(0, 0)
        sim_matrix = build_collab_item_similarity_matrix(read_books, collab_params)
        file_storage.collab_sim_matrix = sim_matrix

        # get the parameters for the user
        id_user = User.objects.get(username="user10").id
        rated_books = file_storage.rated_books_by_user(id_user)
        unrated_books = file_storage.unrated_books_by_user(id_user)
        candidate_books = file_storage.candidate_books_recommend_items("collab", set(rated_books.keys()), unrated_books)
        self.assertIsInstance(candidate_books, pd.DataFrame)
        self.assertListEqual(candidate_books.columns.tolist(), EXPECTED_SIM_COLUMNS)
        self.assertEqual(len(candidate_books), 8)

    def test_candidate_books_predict_score_collab(self):
        """Test the output of the candidate_books_predict_score method for collaborative filtering"""
        # the ratings in user_item_df have to point to the same books as in the database"
        read_books = self.db_storage.load_read_books()
        # initialize a FileStorage object
        file_storage = FileStorage(vector_db_name=self.vector_db_name, user_item_df=read_books)

        # build the similarity matrix
        collab_params = CollabItemSimilarityParams(0, 0)
        sim_matrix = build_collab_item_similarity_matrix(read_books, collab_params)
        file_storage.collab_sim_matrix = sim_matrix

        # get the parameters for the method
        id_user = User.objects.get(username="user10").id
        id_book = Book.objects.get(title="wonder woman").id
        rated_books = file_storage.rated_books_by_user(id_user)

        candidate_books = file_storage.candidate_books_predict_score("collab", id_book, set(rated_books.keys()))
        self.assertIsInstance(candidate_books, pd.DataFrame)
        self.assertListEqual(candidate_books.columns.tolist(), ["source", "similarity"])
        self.assertEqual(len(candidate_books), 2)


class TestsWithOriginalDBReduced(TestCase):
    """Test the FileStorage class with the test dataset 'original_db_reduced'."""

    @classmethod
    def setUpTestData(cls):
        ratings_path = TEST_DATASET_DIR / "original_db_reduced" / "ratings_sample.csv"
        ratings = pd.read_csv(ratings_path)
        vector_db_name = "default"
        create_reduced_database(vector_db_name)
        cls.storage = FileStorage(vector_db_name=vector_db_name, user_item_df=ratings)

    def test_load_ratings(self):
        """Test loading the ratings from the file."""
        all_ratings = self.storage.load_ratings()
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 69)
        self.assertListEqual(all_ratings.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_ratings_greater_than_n(self):
        """Test loading the ratings from the file with a minimum number of ratings."""
        all_ratings = self.storage.load_ratings(min_no_ratings=11)
        self.assertIsNotNone(all_ratings)
        self.assertEqual(len(all_ratings), 0)

    def test_load_read_books(self):
        """Test loading the read books from the file."""
        read_books = self.storage.load_read_books()
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 127)
        self.assertListEqual(read_books.columns.tolist(), EXPECTED_RATINGS_COLUMNS)

    def test_load_read_books_with_most_books(self):
        """Test loading the read books from the file with the users with the most books."""
        read_books = self.storage.load_read_books(users_with_most_books=2)
        self.assertIsNotNone(read_books)
        self.assertEqual(len(read_books), 29)


class TestInvalidOperations(TestCase):
    """Test the FileStorage class with invalid operations, supposed to raise errors."""

    def test_load_ratings_no_path(self):
        """Test loading the ratings from the file without setting the path."""
        storage = FileStorage()
        with self.assertRaises(ValueError):
            storage.load_ratings()

    def test_load_read_books_no_path(self):
        """Test loading the read books from the file without setting the path."""
        storage = FileStorage()
        with self.assertRaises(ValueError):
            storage.load_read_books()

    def test_initialize_file_storage_with_dict_as_ratings_df(self):
        """Test initializing the FileStorage class with a dictionary as the ratings DataFrame."""
        ratings = {"user_id": [1, 2, 3], "book_id": [1, 2, 3], "rating": [4, 5, 6]}
        with self.assertRaises(ValueError, msg="user_item_df should be a pandas dataframe"):
            FileStorage(user_item_df=ratings)

    def test_initialize_file_storage_with_invalid_ratings_df(self):
        """Test initializing the FileStorage class with an invalid ratings DataFrame."""
        ratings = pd.DataFrame({"user": [1, 2, 3], "book_id": [1, 2, 3], "rating": [4, 5, 6]})
        with self.assertRaises(ValueError, msg="user_item_df should have columns 'user_id', 'book_id' and 'rating'"):
            FileStorage(user_item_df=ratings)
