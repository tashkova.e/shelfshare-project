import logging

from django.test import TestCase
from recommend_app.models import CollabSimilarity
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.recommenders.building_utils import (
    normalize,
    save_similarities_to_db,
)
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    CollaborativeFilteringRecommender,
    build_collab_item_similarity_matrix,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    build_fwls_model,
)
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from utils.create_test_database import (
    create_database_with_fake_books,
    create_reduced_database,
)

logger = logging.getLogger(__name__)
import unittest

import pandas as pd


class TestsWithFakeBooksDB(TestCase):
    """Test the functions in this script with the test dataset 'fake_books'."""

    @classmethod
    def setUpTestData(cls):
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_database_with_fake_books(cls.test_db_name)
        cls.ratings = cls.storage.load_ratings(min_no_ratings=1)

    def test_build_collab_item_similarity_matrix(self):
        """Check the returned similarities for the simple case."""
        params = CollabItemSimilarityParams(0, 0.2, True)
        self.assertEqual(CollabSimilarity.objects.count(), 0)
        sim_matrix_df = build_collab_item_similarity_matrix(self.ratings, model_params=params)
        self.assertEqual(CollabSimilarity.objects.count(), 0)
        self.assertEqual(len(sim_matrix_df), 2)
        # Check that the two similarities are equal to each other
        self.assertEqual(sim_matrix_df.iloc[0, 1], sim_matrix_df.iloc[1, 0])

    def test_save_similarities_to_db(self):
        """Test the saving of the similarities to the db"""
        params = CollabItemSimilarityParams(0, 0.2, True)
        ratings = self.storage.load_ratings(min_no_ratings=1)
        # make sure there are no similarities in the db
        self.assertEqual(CollabSimilarity.objects.count(), 0)
        sim_matrix_df = build_collab_item_similarity_matrix(ratings, model_params=params)
        self.assertEqual(CollabSimilarity.objects.count(), 0)
        save_similarities_to_db(sim_matrix_df, self.test_db_name)
        self.assertEqual(CollabSimilarity.objects.count(), 2)
        # make sure the similarities are the same as in the df
        # get the first similarity from the db
        first_sim_df = sim_matrix_df.iloc[0]
        first_sim_db = CollabSimilarity.objects.get(source=first_sim_df.source, target=first_sim_df.target)
        self.assertEqual(first_sim_df.similarity, first_sim_db.similarity)


class TestNormalize(unittest.TestCase):
    """Test the normalize function."""

    def test_normalize(self):
        """Test the normalization of an array."""
        array = pd.Series([2, 3, 1])
        self.assertAlmostEqual(array.mean(), 2.0)
        normalized = normalize(array)
        self.assertAlmostEqual(normalized.mean(), 0.0)
        self.assertAlmostEqual(normalized[0], (array[0] - 2))
        self.assertAlmostEqual(normalized[1], (array[1] - 2))
        self.assertAlmostEqual(normalized[2], (array[2] - 2))
        self.assertAlmostEqual(normalized[0], 0.0)
        self.assertAlmostEqual(normalized[1], 1)
        self.assertAlmostEqual(normalized[2], -1)


class TestsWithReducedDB(TestCase):
    """Test the functions in this script with the test dataset 'reduced'."""

    @classmethod
    def setUpTestData(cls):
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_reduced_database(cls.test_db_name)
        # build the similarity matrix
        cls.ratings_0 = cls.storage.load_read_books(min_no_ratings=0)

    def test_build_fwls_model_all_users(self):
        """Test the building of the FWLS model, using all data"""
        collab_params = CollabItemSimilarityParams(0, 0, True)
        fwls_params = FWLSModelBuilderParams(
            {"builder_params": collab_params}, users_sample_size=-1, valid_size=0.2, vector_db_name=self.test_db_name
        )
        model, collab_rec, content_rec = build_fwls_model(self.ratings_0, fwls_params)
        self.assertIsInstance(model, Pipeline)
        self.assertIsInstance(model.named_steps["scaler"], StandardScaler)
        self.assertIsInstance(model.named_steps["model"], LinearRegression)
        self.assertIsInstance(collab_rec, CollaborativeFilteringRecommender)
        self.assertIsInstance(content_rec, ContentBasedRecommender)
        # make sure both recommenders have the same storage
        self.assertEqual(collab_rec.storage, content_rec.storage)
