import logging

from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
)

logger = logging.getLogger(__name__)

from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.evaluation.coverage import calculate_coverage
from recommender_engine.recommenders.collaborative import (
    CollaborativeFilteringRecommender,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    FWLSRecommender,
)
from recommender_engine.recommenders.popularity import PopularityRecommender
from utils.create_test_database import create_reduced_database


class TestWithOriginalDBReduced(TestCase):
    """Test the calculation of coverage for the recommender system, using the original_db_reduced dataset read from a file."""

    @classmethod
    def setUpTestData(cls):
        # load the original_db_reduced dataset
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_reduced_database(cls.test_db_name)
        # setup popularity recommender
        cls.pop_rec = PopularityRecommender(cls.storage)
        # build the collab similarity matrix
        collab_params_build = CollabItemSimilarityParams(0, 0.2, True)
        recommender_params = {"neighborhood_size": 6}
        # build the first layer recommenders
        cls.collab_rec = CollaborativeFilteringRecommender.get_trained_recommender_with_file_storage(
            cls.storage, builder_params=collab_params_build, recommender_params=recommender_params
        )
        cls.content_rec = ContentBasedRecommender.get_trained_recommender_with_file_storage(
            cls.storage, recommender_params=recommender_params
        )
        # build the FWLS model
        fwls_params = FWLSModelBuilderParams(
            collab_rec={"builder_params": collab_params_build, "recommender_params": recommender_params},
            content_rec=recommender_params,
            users_sample_size=-1,
            valid_size=0.2,
            vector_db_name=cls.test_db_name,
        )
        # initialize the FWLS recommender
        cls.fwls_rec = FWLSRecommender.get_trained_recommender_with_file_storage(
            cls.storage, builder_params=fwls_params
        )

    def test_coverage_popularity(self):
        """Test the calculation of coverage for the popularity recommender."""
        user_coverage, book_coverage = calculate_coverage(self.pop_rec, num=6)
        self.assertGreaterEqual(user_coverage, 0.0)
        self.assertGreaterEqual(book_coverage, 0.0)
        self.assertLessEqual(user_coverage, 1.0)
        self.assertLessEqual(book_coverage, 1.0)

    def test_coverage_fwls(self):
        """Test the calculation of coverage for the FWLS recommender."""
        user_coverage, book_coverage = calculate_coverage(self.fwls_rec, num=6)
        self.assertGreaterEqual(user_coverage, 0.0)
        self.assertGreaterEqual(book_coverage, 0.0)
        self.assertLessEqual(user_coverage, 1.0)
        self.assertLessEqual(book_coverage, 1.0)

    def test_coverage_collab(self):
        """Test the calculation of coverage for the collaborative filtering recommender."""
        user_coverage, book_coverage = calculate_coverage(self.collab_rec, num=6)
        self.assertGreaterEqual(user_coverage, 0.0)
        self.assertGreaterEqual(book_coverage, 0.0)
        self.assertLessEqual(user_coverage, 1.0)
        self.assertLessEqual(book_coverage, 1.0)

    def test_coverage_content(self):
        """Test the calculation of coverage for the content-based recommender."""
        user_coverage, book_coverage = calculate_coverage(self.content_rec, num=6)
        self.assertGreaterEqual(user_coverage, 0.0)
        self.assertGreaterEqual(book_coverage, 0.0)
        self.assertLessEqual(user_coverage, 1.0)
        self.assertLessEqual(book_coverage, 1.0)
