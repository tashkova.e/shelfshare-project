import logging

logger = logging.getLogger(__name__)
import pandas as pd
from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.evaluation.scenarios import train_test_split_one_param
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    CollaborativeFilteringRecommender,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    FWLSRecommender,
)
from recommender_engine.recommenders.popularity import PopularityRecommender
from utils.create_test_database import create_reduced_database


class TestWithOriginalDBReduced(TestCase):
    """Test the calculation of different scenarios for the recommender system, using the original_db_reduced dataset read from a file."""

    @classmethod
    def setUpTestData(cls):
        # load the original_db_reduced dataset
        cls.test_db_name = "default"
        cls.storage = DatabaseStorage(cls.test_db_name)
        create_reduced_database(cls.test_db_name)
        cls.k_list = [1, 3, 5, 10]
        cls.test_size = 0.2
        cls.min_no_ratings = 0
        cls.users_with_most_books = -1

    def train_test_split_one_param_popularity(self):
        """Test the calculation of train_test_split_one_param for the popularity recommender."""
        res_dict = train_test_split_one_param(
            PopularityRecommender,
            self.storage,
            self.k_list,
            test_size=self.test_size,
            min_no_ratings=self.min_no_ratings,
            users_with_most_books=self.users_with_most_books,
        )
        expected_keys = ["mean_absolute_error", "mean_average_precision_at_k"]
        # check if the keys are correct
        self.assertEqual(set(res_dict.keys()), set(expected_keys))
        # check if the values are correct
        self.assertIsInstance(res_dict["mean_absolute_error"], float)
        self.assertIsInstance(res_dict["mean_average_precision_at_k"], list)
        self.assertEqual(len(res_dict["mean_average_precision_at_k"]), len(self.k_list))

    def test_train_test_split_one_param_collaborative(self):
        """Test the calculation of train_test_split_one_param for the collaborative filtering recommender."""
        builder_params = CollabItemSimilarityParams(0, 0, True)
        recommender_params = {"neighborhood_size": 5, "max_candidates": 500}
        res_dict = train_test_split_one_param(
            CollaborativeFilteringRecommender,
            self.storage,
            self.k_list,
            builder_params=builder_params,
            recommender_params=recommender_params,
            test_size=self.test_size,
            min_no_ratings=self.min_no_ratings,
            users_with_most_books=self.users_with_most_books,
        )
        expected_keys = ["mean_absolute_error", "mean_average_precision_at_k"]
        # check if the keys are correct
        self.assertEqual(set(res_dict.keys()), set(expected_keys))
        # check if the values are correct
        self.assertIsInstance(res_dict["mean_absolute_error"], float)
        self.assertIsInstance(res_dict["mean_average_precision_at_k"], list)
        self.assertEqual(len(res_dict["mean_average_precision_at_k"]), len(self.k_list))

    def test_train_test_split_one_param_content(self):
        """Test the calculation of train_test_split_one_param for the content-based recommender."""
        recommender_params = {"neighborhood_size": 5, "max_candidates": 500}
        res_dict = train_test_split_one_param(
            ContentBasedRecommender,
            self.storage,
            self.k_list,
            recommender_params=recommender_params,
            test_size=self.test_size,
            min_no_ratings=self.min_no_ratings,
            users_with_most_books=self.users_with_most_books,
        )
        expected_keys = ["mean_absolute_error", "mean_average_precision_at_k"]
        # check if the keys are correct
        self.assertEqual(set(res_dict.keys()), set(expected_keys))
        # check if the values are correct
        self.assertIsInstance(res_dict["mean_absolute_error"], float)
        self.assertIsInstance(res_dict["mean_average_precision_at_k"], list)
        self.assertEqual(len(res_dict["mean_average_precision_at_k"]), len(self.k_list))

    def test_train_test_split_one_param_hybrid(self):
        """Test the calculation of train_test_split_one_param for the hybrid recommender."""
        collab_params_builder = CollabItemSimilarityParams(0, 0, True)
        collab_params_recommender = {"neighborhood_size": 5, "max_candidates": 500}
        content_params_recommender = {"neighborhood_size": 3, "max_candidates": 300}
        builder_params = FWLSModelBuilderParams(
            collab_rec={"builder_params": collab_params_builder, "recommender_params": collab_params_recommender},
            content_rec=content_params_recommender,
            users_sample_size=-1,
            valid_size=0.2,
            vector_db_name=self.test_db_name,
        )
        recommender_params = {"neighborhood_size_collab": 5, "neighborhood_size_content": 4, "max_candidates": 300}
        res_dict = train_test_split_one_param(
            FWLSRecommender,
            self.storage,
            self.k_list,
            builder_params=builder_params,
            recommender_params=recommender_params,
            test_size=self.test_size,
            min_no_ratings=self.min_no_ratings,
            users_with_most_books=self.users_with_most_books,
        )
        expected_keys = ["mean_absolute_error", "mean_average_precision_at_k"]
        # check if the keys are correct
        self.assertEqual(set(res_dict.keys()), set(expected_keys))
        # check if the values are correct
        self.assertIsInstance(res_dict["mean_absolute_error"], float)
        self.assertIsInstance(res_dict["mean_average_precision_at_k"], list)
        self.assertEqual(len(res_dict["mean_average_precision_at_k"]), len(self.k_list))
