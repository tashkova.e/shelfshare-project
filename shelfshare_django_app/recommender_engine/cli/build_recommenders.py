import logging

logger = logging.getLogger("cli.build_recommender")
import typer
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    build_collab_item_similarity_matrix,
)
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    build_fwls_model,
)

app = typer.Typer()
import pickle

import pandas as pd
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.recommenders.building_utils import (
    save_similarities_to_db,
)
from sklearn.pipeline import Pipeline
from typing_extensions import Annotated
from utils.time import timer

from shelfshare.settings import SAVE_MODEL_DIR


@app.command()
@timer(logger)
def collab_item_similarity_matrix(
    db_name: Annotated[str, "name of the database to use"],
    min_no_ratings: Annotated[int, "minimum number of ratings for a user to be included into the calculations"] = 0,
    users_with_most_ratings: Annotated[int, "Should you consider only users with the most ratings? -1 if no"] = -1,
    min_overlap: Annotated[int, "minimum number of common ratings between two items to be considered"] = 0,
    min_similarity: Annotated[float, "minimum similarity between two items to be considered"] = 0.2,
    normalize: Annotated[bool, "whether to normalize the ratings"] = True,
) -> pd.DataFrame:
    """Build the item-item similarity matrix using collaborative filtering."""
    logger.info("Building the item-item similarity matrix using collaborative filtering, using database %s.", db_name)
    storage = DatabaseStorage(db_name)
    # log the arguments
    logger.info("***Arguments for dataset:***")
    logger.info("min_no_ratings: %s", min_no_ratings)
    logger.info("users_with_most_ratings: %s", users_with_most_ratings)
    logger.info("***Arguments for model:***")
    logger.info("min_overlap: %s", min_overlap)
    logger.info("min_similarity: %s", min_similarity)
    logger.info("normalize: %s", normalize)
    user_item_matrix = storage.load_read_books(
        min_no_ratings=min_no_ratings, users_with_most_books=users_with_most_ratings
    )
    params = CollabItemSimilarityParams(min_overlap, min_similarity, normalize)
    sim_matrix_df = build_collab_item_similarity_matrix(user_item_matrix, model_params=params)
    save_similarities_to_db(sim_matrix_df, db_name)
    return sim_matrix_df


@app.command()
@timer(logger)
def fwls_model_with_first_layer(
    db_name: Annotated[str, "name of the database to use"],
    min_no_ratings: Annotated[int, "minimum number of ratings for a user to be included into the calculations"] = 0,
    users_with_most_ratings: Annotated[int, "Should you consider only users with the most ratings? -1 if no"] = -1,
    # collab builder params
    min_overlap: Annotated[
        int, "minimum number of common ratings between two items to be considered. This is for the collab recommender"
    ] = 0,
    min_similarity: Annotated[
        float, "minimum similarity between two items to be considered. This is for the collab recommender"
    ] = 0.2,
    normalize: Annotated[bool, "whether to normalize the ratings. This is for the collab recommender"] = True,
    # collab recommender params
    neighborhood_size_collab: Annotated[int, "number of neighbors to consider for the collab recommender"] = 10,
    max_candidates_collab: Annotated[int, "number of candidates to consider for the collab recommender"] = 100,
    # content recommender params
    neighborhood_size_content: Annotated[int, "number of neighbors to consider for the content recommender"] = 10,
    max_candidates_content: Annotated[int, "number of candidates to consider for the content recommender"] = 100,
    # for the fwls model
    users_sample_size: Annotated[int, "number of users to sample for the model"] = -1,
    valid_size: Annotated[float, "size of the validation set"] = 0.2,
) -> Pipeline:
    """Build the FWLS model with the first layer."""
    logger.info("Building the FWLS model, as well as the first layer models, using database %s.", db_name)
    storage = DatabaseStorage(db_name)
    # log the arguments
    logger.info("***Arguments for dataset:***")
    logger.info("min_no_ratings: %s", min_no_ratings)
    logger.info("users_with_most_ratings: %s", users_with_most_ratings)
    logger.info("***Arguments building the collab recommender:***")
    logger.info("min_overlap: %s", min_overlap)
    logger.info("min_similarity: %s", min_similarity)
    logger.info("normalize: %s", normalize)
    logger.info("***Arguments for the collab recommender:***")
    logger.info("neighborhood_size_collab: %s", neighborhood_size_collab)
    logger.info("max_candidates_collab: %s", max_candidates_collab)
    logger.info("***Arguments for the content recommender:***")
    logger.info("neighborhood_size_content: %s", neighborhood_size_content)
    logger.info("max_candidates_content: %s", max_candidates_content)
    logger.info("***Arguments for the FWLS model:***")
    logger.info("users_sample_size: %s", users_sample_size)
    logger.info("valid_size: %s", valid_size)
    user_item_matrix = storage.load_read_books(
        min_no_ratings=min_no_ratings, users_with_most_books=users_with_most_ratings
    )
    # setup the parameters
    collab_build_params = CollabItemSimilarityParams(min_overlap, min_similarity, normalize)
    collab_recommender_params = {
        "builder_params": collab_build_params,
        "recommender_params": {"neighborhood_size": neighborhood_size_collab, "max_candidates": max_candidates_collab},
    }
    content_recommender_params = {
        "neighborhood_size": neighborhood_size_content,
        "max_candidates": max_candidates_content,
    }
    fwls_model_params = FWLSModelBuilderParams(
        collab_rec=collab_recommender_params,
        content_rec=content_recommender_params,
        users_sample_size=users_sample_size,
        valid_size=valid_size,
        vector_db_name=db_name,
    )
    model, collab_rec, _ = build_fwls_model(user_item_matrix, fwls_model_params)
    # save the model in a pickle file
    model_path = SAVE_MODEL_DIR / "fwls_model.pkl"
    with open(model_path, "wb") as f:
        pickle.dump(model, f)
    # save the similarity matrix in the database
    collab_sim_matrix = collab_rec.storage.collab_sim_matrix
    save_similarities_to_db(collab_sim_matrix, db_name)


if __name__ == "__main__":
    app()
