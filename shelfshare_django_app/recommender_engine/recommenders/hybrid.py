import logging
from dataclasses import dataclass

import pandas as pd
from recommender_engine.data_storage.utils import (
    filter_by_min_number_of_ratings,
    remove_0_ratings,
)
from recommender_engine.recommenders.building_utils import logger
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from tqdm import tqdm

logger = logging.getLogger(__name__)
from dataclasses import field
from typing import Dict, Self, Tuple

import numpy as np
from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.recommenders.base_classes import (
    ModelBuildingParams,
    WithModel,
)
from recommender_engine.recommenders.building_utils import (
    save_similarities_to_db,
)
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    CollaborativeFilteringRecommender,
)
from recommender_engine.recommenders.content import ContentBasedRecommender


@dataclass
class FWLSModelBuilderParams(ModelBuildingParams):
    """Parameters for the fwls model building"""

    collab_rec: Dict[str, CollabItemSimilarityParams | Dict[str, int]] = field(
        default_factory=lambda: {
            "builder_params": CollabItemSimilarityParams(),
            "recommender_params": {"neighborhood_size": 6, "max_candidates": 100},
        }
    )
    content_rec: Dict[str, int] = field(
        default_factory=lambda: {"neighborhood_size": 6, "max_candidates": 100}
    )  # there are no builder params for the content rec
    users_sample_size: int = -1  # -1 means all users
    valid_size: float = 0.1
    vector_db_name: str = "default"


class FWLSRecommender(WithModel):
    """Recommend items using a collaborative filtering and content-based filtering hybrid model."""

    def __init__(
        self,
        storage: BaseStorage,
        neighborhood_size_collab: int = 6,
        neighborhood_size_content: int = 6,
        max_candidates: int = 100,
    ):
        super().__init__(storage)
        self.neighborhood_size_collab = neighborhood_size_collab
        self.neighborhood_size_content = neighborhood_size_content
        self.max_candidates = max_candidates
        # initialize the recommenders
        self._collab_recommender = CollaborativeFilteringRecommender(storage, neighborhood_size_collab, max_candidates)
        self._content_recommender = ContentBasedRecommender(storage, neighborhood_size_content, max_candidates)

    @property
    def collab_recommender(self) -> CollaborativeFilteringRecommender:
        """Get the collaborative filtering recommender."""
        return self._collab_recommender

    @collab_recommender.setter
    def collab_recommender(self, value: CollaborativeFilteringRecommender):
        self._collab_recommender = value
        self.neighborhood_size_collab = value.neighborhood_size

    @property
    def content_recommender(self) -> ContentBasedRecommender:
        """Get the content-based filtering recommender."""
        return self._content_recommender

    @content_recommender.setter
    def content_recommender(self, value: ContentBasedRecommender):
        self._content_recommender = value
        self.neighborhood_size_content = value.neighborhood_size

    def _prediction(
        self, pred_collab: float, pred_content: float, num_books_rated_by_user: int, num_ratings_for_book: int
    ) -> float:
        c1 = pred_collab
        d1 = pred_content
        c2 = int(num_books_rated_by_user > 3) * pred_collab
        d2 = int(num_books_rated_by_user > 3) * pred_content
        c3 = np.log(num_ratings_for_book + 1) * pred_collab
        d3 = np.log(num_ratings_for_book + 1) * pred_content
        rez = self.storage.fwls_model.predict([[c1, d1, c2, d2, c3, d3]])
        return rez[0]

    def predict_score(self, user_id: int, book_id: int):
        logger.debug("Predicting score for user %d and book %d", user_id, book_id)
        num_books_rated_by_user = len(self.storage.rated_books_by_user(user_id))
        num_ratings_for_book = len(self.storage.ratings_for_book(book_id))
        collab_pred = self.collab_recommender.predict_score(user_id, book_id)
        content_pred = self.content_recommender.predict_score(user_id, book_id)
        return self._prediction(collab_pred, content_pred, num_books_rated_by_user, num_ratings_for_book)

    def recommend_items(self, user_id: int, num: int = 6):
        logger.debug("Recommending items for user %d", user_id)
        num_books_rated_by_user = len(self.storage.rated_books_by_user(user_id))
        # get the recommendations from the first layer recommenders
        collab_recs = self.collab_recommender.recommend_items(user_id, num * 5)
        content_recs = self.content_recommender.recommend_items(user_id, num * 5)
        # collab_recs and content_recs are lists of tuples in format (book_id, prediction)
        # combine the recommendations
        combined_recs = {}
        # format of the dictionary: {'book_id':{'collab':float, 'content':float}}
        for rec in collab_recs:
            book_id = rec[0]
            combined_recs[book_id] = {"collab": rec[1]}
        for rec in content_recs:
            book_id = rec[0]
            if book_id in combined_recs:
                combined_recs[book_id]["content"] = rec[1]
            else:
                combined_recs[book_id] = {"content": rec[1]}
        recommendations = []
        for book_id, predictions in combined_recs.items():
            # predictions is a dictionary with 'collab' and 'content' as keys and the predictions as values
            # get the predictions from the first layer recommenders if they are missing
            if "collab" not in predictions:
                predictions["collab"] = self.collab_recommender.predict_score(user_id, book_id)
            if "content" not in predictions:
                predictions["content"] = self.content_recommender.predict_score(user_id, book_id)
            num_ratings_for_book = len(self.storage.ratings_for_book(book_id))
            fwls_pred = self._prediction(
                predictions["collab"], predictions["content"], num_books_rated_by_user, num_ratings_for_book
            )
            recommendations.append((book_id, fwls_pred))
        sorted_recommendations = sorted(recommendations, key=lambda x: x[1], reverse=True)
        sliced = sorted_recommendations[:num]
        return sliced

    @classmethod
    def get_trained_recommender_with_file_storage(cls, input_storage: BaseStorage, *args, **kwargs) -> Self:
        """Get a trained recommender with a new FileStorage instance as its storage.
        INPUT STORAGE IS NOT MODIFIED. The recommender and first layer recommenders share the same storage.
        No matter the neighborhood size for the first layer recommenders, the result recommender will have the same first layer recommenders used in the building of the model.
        """
        read_books = input_storage.load_read_books(0, -1)
        vector_db_name = input_storage.vector_db_name
        storage = FileStorage(vector_db_name, read_books)
        # get the builder params from the kwargs
        builder_params = kwargs.get("builder_params", FWLSModelBuilderParams())
        recommender_params = kwargs.get("recommender_params", {})
        # build the model
        fwls_model, collab_rec, content_rec = build_fwls_model(read_books, builder_params)
        storage.fwls_model = fwls_model
        rec = cls(storage, **recommender_params)
        rec.collab_recommender = collab_rec
        rec.content_recommender = content_rec
        return rec

    def train_model_and_add_to_storage(self, model_params: FWLSModelBuilderParams) -> Self:
        """Train the model and add it to the storage. MODIFIES THE STORAGE."""
        read_books = self.storage.load_read_books(0, -1)
        fwls_model, collab_rec, content_rec = build_fwls_model(read_books, model_params)
        file_storage = collab_rec.storage
        collab_sim_matrix = file_storage.collab_sim_matrix
        # add the fwls model to the storage
        self.storage.fwls_model = fwls_model
        if isinstance(self.storage, FileStorage):
            self.storage.collab_sim_matrix = collab_sim_matrix
        elif isinstance(self.storage, DatabaseStorage):
            save_similarities_to_db(collab_sim_matrix, self.storage.database_name)
        else:
            raise ValueError("The storage should be either FileStorage or DatabaseStorage")
        # add the first layer recommenders to self
        self.collab_recommender = CollaborativeFilteringRecommender(
            self.storage, neighborhood_size=collab_rec.neighborhood_size, max_candidates=collab_rec.max_candidates
        )
        self.content_recommender = ContentBasedRecommender(
            self.storage, neighborhood_size=content_rec.neighborhood_size, max_candidates=content_rec.max_candidates
        )
        return self


def create_features(
    data_for_features: pd.DataFrame,
    collab_recommender: CollaborativeFilteringRecommender,
    content_recommender: ContentBasedRecommender,
):
    """Create features for the FWLS model.
       Features are the predictions of the two recommenders, multiplied by different functions of the ratings.
       The functions are:
         - f1(user,book)=1
         - f2(user,book)=1 if the user has rated more than 3 books, 0 otherwise
         - f3(user,book)=log(number of ratings for the book+1)

    Args:
        data_for_features (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        collab_recommender (CollaborativeFilteringRecommender): Recommender for collaborative filtering.
        content_recommender (ContentBasedRecommender): Recommender for content-based filtering.

    Raises:
        ValueError: If there are missing collaborative filtering predictions.
        ValueError: If there are missing content predictions.
    """
    logger.info("Creating features")

    def calculate_predictions(group):
        # Get the predictions for each book
        collab_recs = group.apply(lambda x: collab_recommender.predict_score(x["user_id"], x["book_id"]), axis=1)
        content_recs = group.apply(lambda x: content_recommender.predict_score(x["user_id"], x["book_id"]), axis=1)
        data_for_features.loc[group.index, "collab"] = collab_recs
        data_for_features.loc[group.index, "content"] = content_recs

    # Use groupby to apply the function
    tqdm.pandas(desc="Generating predictions for each user...")
    data_for_features.groupby("user_id").progress_apply(calculate_predictions)
    if data_for_features.collab.isnull().sum() > 0:
        raise ValueError("There are missing collaborative filtering predictions")
    if data_for_features.content.isnull().sum() > 0:
        raise ValueError("There are missing content predictions")
    # multiply the predictions with the feature functions
    data_for_features["c1"] = data_for_features["collab"]
    data_for_features["d1"] = data_for_features["content"]
    more_than_3_for_user = data_for_features.groupby("user_id")["rating"].transform(lambda x: int(sum(x != 0) > 3))
    data_for_features["c2"] = data_for_features["collab"] * more_than_3_for_user
    data_for_features["d2"] = data_for_features["content"] * more_than_3_for_user
    # number of times each book has been rated
    book_freq = data_for_features.groupby("book_id")["book_id"].transform("count")
    log_freq = np.log(book_freq + 1)  # add 1 to avoid log(0)
    data_for_features["c3"] = data_for_features["collab"] * log_freq
    data_for_features["d3"] = data_for_features["content"] * log_freq


def train_model(train_data: pd.DataFrame, valid_data: pd.DataFrame) -> Pipeline:
    """Train a linear regression model on the training data and evaluate it on the validation data.
       It assumes the data has been preprocessed and the features are in the columns c1, d1, c2, d2, c3, d3.

    Args:
        train_data (pd.DataFrame): Training data.
        valid_data (pd.DataFrame): Validation data.

    Returns:
        Pipeline: Trained model.
    """
    logger.info("Training a linear regression model")
    model = LinearRegression(fit_intercept=True, n_jobs=-1)
    scaler = StandardScaler()
    feature_columns = ["c1", "d1", "c2", "d2", "c3", "d3"]
    pipeline = Pipeline([("scaler", scaler), ("model", model)])
    pipeline.fit(train_data[feature_columns].values, train_data["rating"].values)
    # print the metrics
    predictions_train = pipeline.predict(train_data[feature_columns].values)
    predictions_valid = pipeline.predict(valid_data[feature_columns].values)
    mse_train = mean_squared_error(train_data["rating"].values, predictions_train)
    mse_valid = mean_squared_error(valid_data["rating"].values, predictions_valid)
    logger.info("Mean squared error on train data: %s", mse_train)
    logger.info("Mean squared error on validation data: %s", mse_valid)
    return pipeline


def build_fwls_model(
    user_item_df: pd.DataFrame,
    model_params: FWLSModelBuilderParams,
) -> Tuple[Pipeline, CollaborativeFilteringRecommender, ContentBasedRecommender]:
    """Build a FWLS model using the input ratings and the model parameters.
    First layer recommenders with FileStorage are created here.
    They are trained on the whole user_item_df dataframe.
    For training the linear regression model, only the ratings are used.

    Args:
        user_item_df (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        model_params (FWLSModelBuilderParams): Parameters for building the model.

    Raises:
        ValueError: If there are not enough users to train the model.

    Returns:
        Tuple[Pipeline, CollaborativeFilteringRecommender, ContentBasedRecommender]: The trained model, collaborative filtering recommender, content-based recommender.
    """
    # check if there are any 0 ratings
    ratings = remove_0_ratings(user_item_df)
    data_for_model = filter_by_min_number_of_ratings(ratings, 1)
    number_of_ratings = len(data_for_model)
    # should we train the linear regression model only on a sample of users?
    if model_params.users_sample_size != -1:
        users = data_for_model["user_id"].unique()
        users_sample = np.random.choice(users, model_params.users_sample_size)
        data_for_model = data_for_model[data_for_model["user_id"].isin(users_sample)]
        logger.debug("Sampled %d users", model_params.users_sample_size)
    number_of_users = len(data_for_model["user_id"].unique())
    number_of_ratings = len(data_for_model)
    number_of_books = len(data_for_model["book_id"].unique())
    logger.info(
        "Building fwls model using %d ratings, %d users, %d books", number_of_ratings, number_of_users, number_of_books
    )
    # get the recommenders from the parameters
    file_storage = FileStorage(vector_db_name=model_params.vector_db_name, user_item_df=user_item_df)
    collab_rec = CollaborativeFilteringRecommender.get_trained_recommender_with_file_storage(  # the storage of the collab_rec is NOT the same as the storage in the input
        file_storage, **model_params.collab_rec
    )
    # make the content and collab_rec use the same storage
    collab_storage = collab_rec.storage
    content_rec = ContentBasedRecommender(collab_storage, **model_params.content_rec)
    pipeline = build_fwls_model_inner(data_for_model, collab_rec, content_rec, model_params.valid_size)
    return pipeline, collab_rec, content_rec


def build_fwls_model_inner(
    data_for_model: pd.DataFrame,
    collab_rec: CollaborativeFilteringRecommender,
    content_rec: ContentBasedRecommender,
    valid_size: float,
) -> Pipeline:
    """Build the FWLS model using the input data and the recommenders.
    We assume the content and collaborative recommenders are set up and trained.
    We only check if there are 0 ratings in the data and remove them.
    Only use this function if you know what you are doing.

    Args:
        data_for_model (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        collab_rec (CollaborativeFilteringRecommender): Collaborative filtering recommender.
        content_rec (ContentBasedRecommender): Content-based filtering recommender.
        valid_size (float): Size of the validation set.

    Returns:
        Pipeline: Trained model.
    """
    # remove 0 ratings if they exist
    data_for_model = remove_0_ratings(data_for_model)
    create_features(data_for_model, collab_rec, content_rec)  # the data_for_model df is modified in place
    # split data_for_model into train and test
    train_ratings, test_ratings = train_test_split(data_for_model, test_size=valid_size)
    # train the model
    pipeline = train_model(train_ratings, test_ratings)
    return pipeline
