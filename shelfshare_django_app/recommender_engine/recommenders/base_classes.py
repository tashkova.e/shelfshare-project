import logging
from dataclasses import dataclass

logger = logging.getLogger(__name__)
from abc import ABC, abstractmethod
from typing import List, Self, Tuple

from recommender_engine.data_storage.base_classes import BaseStorage


class BaseRecommender(ABC):
    """Base class for all recommenders"""

    def __init__(self, storage: BaseStorage):
        self.storage = storage

    @abstractmethod
    def recommend_items(self, user_id: int, num: int = 6) -> List[Tuple[int, float]]:
        """Recommend num items for user user_id

        Args:
            user_id (int): Id of user
            num (int, optional): Number of items to recommend. Defaults to 6.

        Returns:
            List[Tuple[int, float]]: List of tuples in format (book_id, prediction)
        """

    @abstractmethod
    def predict_score(self, user_id: int, book_id: int) -> float:
        """Predict the score user_id would have given book_id

        Args:
            user_id (int): Id of user
            book_id (int): Id of book

        Returns:
            float: Predicted score. 0.0 if no prediction can be made.
        """

    @classmethod
    @abstractmethod
    def get_trained_recommender_with_file_storage(cls, input_storage: BaseStorage, *args, **kwargs) -> Self:
        """Get a trained recommender with a new FileStorage instance as its storage.
        INPUT STORAGE IS NOT MODIFIED."""


class NeighborhoodBasedRecommender(BaseRecommender):
    """Recommend the most similar items in a neighborhood."""

    def __init__(self, storage: BaseStorage, neighborhood_size: int = 7, max_candidates: int = 100):
        super().__init__(storage)
        self.neighborhood_size = neighborhood_size
        self.max_candidates = max_candidates

    def _recommend_items(self, sim_type: str, user_id: int, num: int = 6):
        """Recommend num items for user user_id."""
        active_user_rated_books = self.storage.rated_books_by_user(user_id)
        if len(active_user_rated_books) == 0:
            logger.debug("User has not rated any books.")
            return []
        rated_books_set = set(active_user_rated_books.keys())
        active_user_unrated_books = self.storage.unrated_books_by_user(user_id)
        candidate_books = self.storage.candidate_books_recommend_items(
            sim_type, rated_books_set, active_user_unrated_books, self.max_candidates
        )
        # candidate_books is a dataframe with columns source, target, similarity
        # get the similarity weighted average rating of the candidate books
        recommendations = []
        candidate_target_books = candidate_books["target"].unique()
        for target_book in candidate_target_books:
            rated_books_similar_to_target = candidate_books[candidate_books["target"] == target_book].head(
                self.neighborhood_size
            )  # get the top neighborhood_size similar items
            sim_dot_rating = rated_books_similar_to_target.apply(
                lambda x: x.similarity * active_user_rated_books[x.source], axis=1
            )
            prediction_sum = sim_dot_rating.sum()
            sim_sum = rated_books_similar_to_target.similarity.sum()
            predicted_rating = prediction_sum / sim_sum
            recommendations.append((target_book, predicted_rating))
        # sort the recommendations by predicted rating
        sorted_recommendations = sorted(recommendations, key=lambda x: x[1], reverse=True)
        sliced = sorted_recommendations[:num]
        return sliced

    def _predict_score(self, sim_type: str, user_id: int, book_id: int):
        """Predict the score user_id would have given book_id."""
        active_user_rated_books = self.storage.rated_books_by_user(user_id)
        if len(active_user_rated_books) == 0:
            logger.debug("User has not rated any books.")
            return 0.0
        candidate_books = self.storage.candidate_books_predict_score(
            sim_type, book_id, active_user_rated_books, self.neighborhood_size
        )
        if len(candidate_books) == 0:
            logger.debug("No similar books found.")
            return 0.0
        # candidate_books is a dataframe with columns source, target, similarity, it can be empty
        # multiply the predictions with the similarity and sum them up
        sim_dot_rating = candidate_books.apply(lambda x: x.similarity * active_user_rated_books[x.source], axis=1)
        prediction = sim_dot_rating.sum()
        sim_sum = candidate_books.similarity.sum()
        return prediction / sim_sum


@dataclass
class ModelBuildingParams:
    """Parameters for the model building"""


class WithModel(BaseRecommender):
    """Base class for recommenders that use some kind of offline trained model."""

    @abstractmethod
    def train_model_and_add_to_storage(self, model_params: ModelBuildingParams) -> Self:
        """Train the model and add it to the storage. MODIFIES THE STORAGE."""
