import logging
from dataclasses import dataclass
from datetime import datetime

import pandas as pd
from recommender_engine.data_storage.utils import remove_0_ratings
from recommender_engine.recommenders.building_utils import (
    normalize,
    save_similarities_to_db,
)
from scipy.sparse import coo_matrix
from sklearn.metrics.pairwise import cosine_similarity

logger = logging.getLogger(__name__)
from typing import Self

from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.recommenders.base_classes import (
    ModelBuildingParams,
    NeighborhoodBasedRecommender,
    WithModel,
)


@dataclass
class CollabItemSimilarityParams(ModelBuildingParams):
    """Parameters for the collaborative filtering item similarity matrix building."""

    min_overlap: int = 5
    min_sim: float = 0.2
    normalize: bool = True


class CollaborativeFilteringRecommender(NeighborhoodBasedRecommender, WithModel):
    """Recommend items based on collaborative filtering."""

    def recommend_items(self, user_id: int, num: int = 6):
        logger.debug("Recommending items for user %d", user_id)
        return self._recommend_items("collab", user_id, num)

    def predict_score(self, user_id: int, book_id: int):
        logger.debug("Predicting score for user %d and book %d", user_id, book_id)
        return self._predict_score("collab", user_id, book_id)

    @classmethod
    def get_trained_recommender_with_file_storage(cls, input_storage: BaseStorage, *args, **kwargs) -> Self:
        """Get a trained recommender with a new FileStorage instance as its storage.
        INPUT STORAGE IS NOT MODIFIED."""
        read_books = input_storage.load_read_books(0, -1)
        vector_db_name = input_storage.vector_db_name
        new_storage = FileStorage(vector_db_name, read_books)
        # get the builder params and recommender params
        builder_params = kwargs.get("builder_params", CollabItemSimilarityParams())
        recommender_params = kwargs.get("recommender_params", {})
        # build the model
        similarity_matrix = build_collab_item_similarity_matrix(read_books, builder_params)
        # save the similarities to the file storage
        new_storage.collab_sim_matrix = similarity_matrix
        # create the recommender
        return cls(new_storage, **recommender_params)

    def train_model_and_add_to_storage(self, model_params: CollabItemSimilarityParams) -> Self:
        """Train the model and add it to the storage. MODIFIES THE STORAGE."""
        data_for_model = self.storage.load_ratings(min_no_ratings=1)
        similarity_matrix = build_collab_item_similarity_matrix(data_for_model, model_params)
        # save the similarities to the file storage
        if isinstance(self.storage, FileStorage):
            self.storage.collab_sim_matrix = similarity_matrix
        elif isinstance(self.storage, DatabaseStorage):
            save_similarities_to_db(similarity_matrix, self.storage.database_name)
        else:
            raise ValueError("The storage should be either FileStorage or DatabaseStorage")
        return self


def build_collab_item_similarity_matrix(
    ratings: pd.DataFrame,
    model_params: CollabItemSimilarityParams = CollabItemSimilarityParams(),
) -> pd.DataFrame:
    """Build the item similarity matrix from the ratings dataframe.

    Args:
        ratings (pd.DataFrame): Dataframe with columns user_id, book_id, rating.
        build_params (CollabItemSimilarityParams, optional): Parameters for building the matrix. Defaults to CollabItemSimilarityParams().

    Returns:
        pd.DataFrame: Dataframe containing the similarity between selected pairs of items.
    """
    start_time = datetime.now()
    # check if there are any 0 ratings
    if ratings["rating"].min() == 0:
        logger.warning("There are ratings 0 in the dataset, I will remove them")
        ratings = remove_0_ratings(ratings)
    number_of_ratings = len(ratings)
    if number_of_ratings == 0:
        logger.warning("No ratings to build the similarity matrix")
        return pd.DataFrame(columns=["source", "target", "similarity"])
    logger.info("Building item similarity matrix using %d ratings", number_of_ratings)
    logger.debug("Creating ratings matrix")
    ratings = ratings.copy()
    ratings["rating"] = ratings["rating"].astype(float)
    if model_params.normalize:
        logger.debug("Normalizing ratings")
        ratings["rating"] = ratings.groupby("user_id")["rating"].transform(normalize)
    # convert user and book ids to categorical variables
    ratings["user_id"] = ratings["user_id"].astype("category")
    ratings["book_id"] = ratings["book_id"].astype("category")
    # create a sparse matrix. The rows are the books, the columns are the users
    coo = coo_matrix((ratings["rating"], (ratings["book_id"].cat.codes.copy(), ratings["user_id"].cat.codes.copy())))
    logger.info(
        "Sparse ratings matrix created with shape %s, %s",
        coo.shape[0],
        coo.shape[1],
    )
    logger.debug("Ratings matrix:")
    logger.debug(coo)
    sparsity_level = 1 - (ratings.shape[0] / (coo.shape[0] * coo.shape[1]))
    logger.info("Sparsity level is %f", sparsity_level)
    logger.debug("Calculating overlaps in ratings between the books")
    # the overlap matrix is a square matrix with the size of the number of books.
    # it represents the number of users that have rated both books.
    ones_data = [1] * len(coo.data)
    ones_matrix = coo_matrix(
        (ones_data, (coo.row, coo.col)), shape=coo.shape
    )  # matrix with 1s where the original matrix has values, that is, where the user has rated the book
    overlap_matrix = ones_matrix.dot(ones_matrix.transpose())
    logger.debug("Overlap matrix: ")
    logger.debug(overlap_matrix)

    logger.debug("Calculating similarities")
    cor = cosine_similarity(coo, dense_output=False)
    logger.debug("Similarity matrix:")
    logger.debug(cor)
    # filter the correlation matrix with the minimum overlap and minimum similarity
    number_of_correlations = (cor > model_params.min_sim).count_nonzero()
    logger.debug(
        "Correlation matrix leaves %d out of %d elements with min similarity=%f",
        number_of_correlations,
        cor.count_nonzero(),
        model_params.min_sim,
    )
    cor = cor.multiply(
        cor > model_params.min_sim
    )  # keep the elements that are greater than the minimum similarity,set the rest to 0

    # filter the correlation matrix with the minimum overlap
    number_of_overlaps = (cor.multiply(overlap_matrix > model_params.min_overlap)).count_nonzero()

    logger.debug(
        "Overlap matrix leaves %d out of %d elements with min overlap=%f",
        number_of_overlaps,
        cor.count_nonzero(),
        model_params.min_overlap,
    )
    cor = cor.multiply(
        overlap_matrix > model_params.min_overlap
    )  # keep the elements that have more than the minimum overlap, set the rest to 0

    # TODO: should this be with absolute value?
    index_to_book_id = dict(enumerate(ratings["book_id"].cat.categories))
    logger.info("Similarity matrix calculated in %s", (datetime.now() - start_time))
    row, col = cor.nonzero()
    # convert the matrix to a dataframe
    df = pd.DataFrame(
        {
            "source": [index_to_book_id[i] for i in row],
            "target": [index_to_book_id[i] for i in col],
            "similarity": cor.data,
        }
    ).astype({"source": "int32", "target": "int32", "similarity": "float64"})
    # drop rows with the same source and target
    df = df[df["source"] != df["target"]]
    return df
