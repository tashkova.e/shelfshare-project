import logging
from datetime import datetime

import pandas as pd
from recommend_app.models import CollabSimilarity
from tqdm import tqdm

logger = logging.getLogger(__name__)
from books.models import Book


def normalize(x: pd.Series) -> pd.Series:
    """Mean normalization of a pandas series.
    Args:
        x (pd.Series): input series

    Returns:
        pd.Series: output series
    """
    x_mean = x.mean()
    return x - x_mean


def save_similarities_to_db(sim_df: pd.DataFrame, database_name: str) -> None:
    """Save the similarities to the database.

    Args:
        sim_df (pd.DataFrame): Dataframe with columns source, target, similarity.
        database_name (str): Name of the database to save the similarities.
    """
    logger.info("Saving collab similarities to database %s", database_name)
    logger.info("Deleting old similarities")
    CollabSimilarity.objects.using(database_name).all().delete()
    start_time = datetime.now()
    # remove rows where source and target are the same
    sim_df = sim_df[sim_df["source"] != sim_df["target"]]
    sims = []
    # iterate with itertuples
    for row in tqdm(sim_df.itertuples(), desc="Creating similarities model instances"):
        source_book = Book.objects.using(database_name).get(id=row.source)
        target_book = Book.objects.using(database_name).get(id=row.target)
        new_similarity = CollabSimilarity(source=source_book, target=target_book, similarity=row.similarity)
        sims.append(new_similarity)
    # save the similarities in bulk
    CollabSimilarity.objects.using(database_name).bulk_create(sims)
    no_saved = len(sims)
    logger.info("%i similarities saved in %i seconds", no_saved, (datetime.now() - start_time).seconds)
