import logging

logger = logging.getLogger(__name__)
from typing import List, Self, Tuple

from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.recommenders.base_classes import BaseRecommender


class PopularityRecommender(BaseRecommender):
    """Recommends the most poplar books"""

    def recommend_items(self, user_id: int, num: int = 6) -> List[Tuple[int, float]]:
        logger.debug("Recommending items for user %d", user_id)
        top_rated_books = self.storage.top_rated_books(-1)
        # query set with values Book, rating
        active_user_read_books = self.storage.read_books_by_user(user_id)
        # remove books that the user has already read
        top_rated_books_filtered = []
        for book_id, rating in top_rated_books:
            if book_id not in active_user_read_books:
                tuple_to_add = (book_id, rating)
                top_rated_books_filtered.append(tuple_to_add)

        if len(top_rated_books_filtered) < num:
            recommendations = top_rated_books
        else:
            recommendations = top_rated_books_filtered[:num]
        return recommendations

    def predict_score(self, user_id: int, book_id: int) -> float:
        logger.debug("Predicting score for user %d and book %d", user_id, book_id)
        ratings_for_book = self.storage.ratings_for_book(book_id)
        # remove the rating of the active user if it exists
        ratings_for_book_filtered = {}
        for user_id_r, rating in ratings_for_book.items():
            if user_id != user_id_r:
                ratings_for_book_filtered[user_id_r] = rating
        if len(ratings_for_book_filtered) == 0:
            return 0.0
        # get the average rating of the book
        length = len(ratings_for_book_filtered)
        values = list(ratings_for_book_filtered.values())
        sum_values = sum(values)
        avg_rating = sum_values / length
        return avg_rating

    @classmethod
    def get_trained_recommender_with_file_storage(cls, input_storage: BaseStorage, *args, **kwargs) -> Self:
        read_books = input_storage.load_read_books(0, -1)
        vector_db_name = input_storage.vector_db_name
        storage = FileStorage(vector_db_name, read_books)
        return cls(storage)
