import logging

from recommender_engine.data_storage.base_classes import BaseStorage
from recommender_engine.data_storage.file_storage import FileStorage

logger = logging.getLogger(__name__)
from typing import Self

from recommender_engine.recommenders.base_classes import (
    NeighborhoodBasedRecommender,
)


class ContentBasedRecommender(NeighborhoodBasedRecommender):
    """Recommend items based on content based filtering."""

    def recommend_items(self, user_id: int, num: int = 6):
        logger.debug("Recommending items for user %d", user_id)
        return self._recommend_items("content", user_id, num)

    def predict_score(self, user_id: int, book_id: int):
        logger.debug("Predicting score for user %d and book %d", user_id, book_id)
        return self._predict_score("content", user_id, book_id)

    @classmethod
    def get_trained_recommender_with_file_storage(cls, input_storage: BaseStorage, *args, **kwargs) -> Self:
        read_books = input_storage.load_read_books(0, -1)
        vector_db_name = input_storage.vector_db_name
        storage = FileStorage(vector_db_name, read_books)
        # get the recommender params
        recommender_params = kwargs.get("recommender_params", {})
        # create the recommender
        return cls(storage, **recommender_params)
