import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import logging

logger = logging.getLogger("print_database_state")
import typer
from bookexchange.models import BookOwnership, BookRequest, ReadBook, ToRead
from books.models import Author, Book, Genre
from recommend_app.models import CollabSimilarity
from users.models import Friend, FriendshipRequest, User

app = typer.Typer()


@app.command()
def main(db_name: str):
    """Prints the state of the database

    Args:
        db_name (str): Name of the database
    """
    logger.info("Info for database: %s", db_name)
    logger.info("Number of books: %s", Book.objects.using(db_name).count())
    logger.info("Number of authors: %s", Author.objects.using(db_name).count())
    logger.info("Number of genres: %s", Genre.objects.using(db_name).count())
    logger.info("Number of users: %s", User.objects.using(db_name).count())
    logger.info("Number of friends: %s", Friend.objects.using(db_name).count())
    logger.info("Number of friendship requests: %s", FriendshipRequest.objects.using(db_name).count())
    logger.info("Number of read books: %s", ReadBook.objects.using(db_name).count())
    logger.info("Number of books to read: %s", ToRead.objects.using(db_name).count())
    logger.info("Number of book ownerships: %s", BookOwnership.objects.using(db_name).count())
    logger.info("Number of book requests: %s", BookRequest.objects.using(db_name).count())
    logger.info("Number of CollabSimilarity: %s", CollabSimilarity.objects.using(db_name).count())


if __name__ == "__main__":
    app()
