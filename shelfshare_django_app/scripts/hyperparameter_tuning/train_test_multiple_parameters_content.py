import typer

app = typer.Typer()
import logging
import time

from utils.time import timer

logger = logging.getLogger("hyperparameter_tuning.train_test_multiple_parameters_content")

import numpy as np
import pandas as pd
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.data_storage.file_storage import FileStorage
from recommender_engine.evaluation.metrics import (
    mean_absolute_error,
    mean_average_precision_at_multiple_k,
)
from recommender_engine.recommenders.content import ContentBasedRecommender
from tqdm import tqdm
from typing_extensions import Annotated
from utils.split_data import split_data_by_users

from shelfshare.settings import OUTSIDE_DATA_DIR


@app.command()
@timer(logger)
def main(
    db_name: Annotated[str, "name of the database to use"],
    min_no_ratings: Annotated[int, "minimum number of ratings for a user to be included into the calculations"] = 0,
    users_with_most_books: Annotated[int, "Should you consider only users with the most ratings? -1 if no"] = -1,
    test_size: Annotated[float, "Percentage of users to use for testing"] = 0.2,
):
    """Train and test multiple parameters for the content based filtering recommender."""
    logger.info(
        "Training and testing multiple parameters for the content based filtering recommender, using database %s.",
        db_name,
    )
    storage = DatabaseStorage(db_name)
    k_list = [1, 5, 10, 20]
    recommender_params_lst = [
        # {"neighborhood_size": 5, "max_candidates": 500},
        {"neighborhood_size": 7, "max_candidates": 500},
        {"neighborhood_size": 10, "max_candidates": 500},
        # {"neighborhood_size": 15, "max_candidates": 500},
    ]
    vector_db_name = storage.vector_db_name
    user_item_df = storage.load_read_books(min_no_ratings, users_with_most_books)  # these are not hyperparameters.
    all_user_ids = user_item_df["user_id"].unique()
    all_book_ids = user_item_df["book_id"].unique()
    logger.info(
        "Dataset for the evaluation contains %d interactions, for %d users and %d books.",
        len(user_item_df),
        len(all_user_ids),
        len(all_book_ids),
    )
    logger.info(
        "Evaluating by testing on %d%% users",
        test_size * 100,
    )
    test_df, train_df = split_data_by_users(user_item_df, test_size)
    # create train and test storage
    train_storage = FileStorage(vector_db_name, train_df)
    test_storage = FileStorage(vector_db_name, test_df)
    # get the test ratings
    test_ratings = test_storage.load_ratings()  # users with no ratings are not considered
    # set an index on book_id
    test_ratings = test_ratings.set_index("user_id")
    len_results = len(recommender_params_lst)
    map_k_matrix = np.zeros((len_results, len(k_list)))
    mea_list = []
    i = 0
    train_storage.populate_ratings_by_user_cache()
    # go over each combination of builder and recommender parameters
    for recommender_params in tqdm(recommender_params_lst, desc="Going through the parameters"):
        rec_to_test = ContentBasedRecommender(train_storage, **recommender_params)
        map_k_values = mean_average_precision_at_multiple_k(rec_to_test, test_ratings, k_list)
        mea = mean_absolute_error(rec_to_test, test_ratings)
        # add the results to the matrix
        map_k_matrix[i, :] = map_k_values
        mea_list.append(mea)
        i += 1
    # make a dataframe with the parameters and the results
    df = pd.DataFrame(
        {
            "recommender_params": recommender_params_lst,
            "mean_absolute_error": mea_list,
        }
    )
    for i, k in enumerate(k_list):
        df[f"map@{k}"] = map_k_matrix[:, i]
    # make a column "average_map@k" which is the average of the map@k columns
    columns_with_map = [f"map@{k}" for k in k_list]
    df_with_map = df[columns_with_map]
    df["average_map@k"] = df_with_map.mean(axis=1)

    RESULTS_DIR = OUTSIDE_DATA_DIR / "hyperparameter_tuning" / "train_test_multiple_parameters" / "content"
    RESULTS_DIR.mkdir(parents=True, exist_ok=True)
    # get the current time
    current_time = time.strftime("%Y%m%d-%H%M%S")
    file_name = f"{current_time}_{min_no_ratings}_{users_with_most_books}_{test_size}.csv"
    # save the results
    df.to_csv(RESULTS_DIR / file_name, index=False)


if __name__ == "__main__":
    app()
