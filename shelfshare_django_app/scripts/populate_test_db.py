import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import logging

logger = logging.getLogger("populate_test_db")
import typer
from utils.create_test_database import (
    create_database_with_fake_books,
    create_movie_book_database,
    create_reduced_database,
    delete_db,
)

app = typer.Typer()


@app.command()
def create_reduced_db(db_name: str):
    """Populate the specified db with a reduced set of data for testing purposes."""
    logger.info("Starting test database population script for %s...", db_name)
    delete_db(db_name)
    create_reduced_database(db_name)
    logger.info("Finished test database population script for %s.", db_name)


@app.command()
def create_fake_books_db(db_name: str):
    """Populate the specified db with the test dataset "fake_books"."""
    logger.info("Starting test database population script for %s...", db_name)
    delete_db(db_name)
    create_database_with_fake_books(db_name)
    logger.info("Finished test database population script for %s.", db_name)


@app.command()
def create_movie_books_db(db_name: str):
    """Populate the specified db with the test dataset "movie_books"."""
    logger.info("Starting test database population script for %s...", db_name)
    delete_db(db_name)
    create_movie_book_database(db_name)
    logger.info("Finished test database population script for %s.", db_name)


if __name__ == "__main__":
    app()
