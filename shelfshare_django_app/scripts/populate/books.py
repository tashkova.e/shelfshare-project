import os
from pathlib import Path

import pandas as pd
from tqdm import tqdm

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")

import django

django.setup()
import logging

logger = logging.getLogger("populate_books")

from shelfshare.settings import OUTSIDE_DATA_DIR

transformed_folder = OUTSIDE_DATA_DIR / "transformed"
BOOKS_PATH = Path(transformed_folder, "books_final.csv")
GENRES_PATH = Path(transformed_folder, "genres.csv")
from typing import Tuple

from books.models import Author, Book, Genre


def create_book_without_authors_and_genres(book: Tuple):
    publish_date = book.publishDate if isinstance(book.publishDate, str) else None
    embedding = book.embedding.replace("\n", "").strip("[]").strip().split(" ")
    embedding = [float(i) for i in embedding if i]
    new_book = Book(
        ISBN=book.isbn,
        title=book.title,
        description=book.description,
        imageURL=book.imageUrl,
        publisher=book.publisher,
        publishDate=publish_date,
        goodreadsURL=book.url,
        embedding=embedding,
    )

    return new_book


def delete_db(db_name="default"):
    logger.info("Deleting models from the books app, for db: %s", db_name)
    Book.objects.using(db_name).all().delete()
    Genre.objects.using(db_name).all().delete()
    Author.objects.using(db_name).all().delete()


def populate(db_name="default"):
    logger.info("Populating the database %s with models from the books app", db_name)
    delete_db(db_name)
    books = pd.read_csv(BOOKS_PATH)
    genres = pd.read_csv(GENRES_PATH)
    books["genres_list"] = books.genres.apply(
        lambda x: list(map(lambda x: x.strip().strip("'"), x.strip("[").strip("]").split(",")))
    )
    books["author_list"] = books.author.apply(
        lambda x: list(map(lambda x: x.strip().strip("'"), x.strip("[").strip("]").split(",")))
    )
    # get all authors in the df
    authors = set()
    for author_list in books.author_list:
        authors.update(author_list)
    all_authors = list(authors)
    # create Genre objects
    genre_lst = []
    for genre_row in tqdm(genres.itertuples(), "creating Genre objects..."):
        new_genre = Genre(genre=genre_row.genre)
        genre_lst.append(new_genre)
    Genre.objects.using(db_name).bulk_create(genre_lst)
    # create Author objects
    author_lst = []
    for author_name in tqdm(all_authors, "creating Author objects..."):
        new_author = Author(name=author_name)
        author_lst.append(new_author)
    Author.objects.using(db_name).bulk_create(author_lst)
    # create Book objects, without the many-to-many fields
    book_lst = []
    for book_row in tqdm(books.itertuples(), "creating Book objects..."):
        book_lst.append(create_book_without_authors_and_genres(book_row))
    Book.objects.using(db_name).bulk_create(book_lst)
    # add the genres
    for book_row in tqdm(books.itertuples(), "adding genres to books..."):
        book = Book.objects.using(db_name).get(ISBN=book_row.isbn)
        genres_for_book = []
        for genre_name in book_row.genres_list:
            genre_id = Genre.objects.using(db_name).get(genre=genre_name).id
            book_genre = Book.genres.through(book_id=book.id, genre_id=genre_id)
            genres_for_book.append(book_genre)
        book.genres.through.objects.using(db_name).bulk_create(genres_for_book)
    # add the authors
    for book_row in tqdm(books.itertuples(), "adding authors to books..."):
        book = Book.objects.using(db_name).get(ISBN=book_row.isbn)
        authors_for_book = []
        for author_name in book_row.author_list:
            author_id = Author.objects.using(db_name).get(name=author_name).id
            book_author = Book.author.through(book_id=book.id, author_id=author_id)
            authors_for_book.append(book_author)
        book.author.through.objects.using(db_name).bulk_create(authors_for_book)
