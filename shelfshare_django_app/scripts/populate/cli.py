import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")

import django

django.setup()
import logging

import typer
from scripts.populate.bookexchange import populate as populate_bookexchange
from scripts.populate.books import populate as populate_books
from scripts.populate.users import populate as populate_users
from utils.time import timer

logger = logging.getLogger("populate")
app = typer.Typer()


@app.command()
@timer(logger)
def books(db_name: str):
    """Populate the models from the books app."""
    populate_books(db_name)


@app.command()
@timer(logger)
def users(db_name: str):
    """Populate the models from the users app."""
    populate_users(db_name)


@app.command()
@timer(logger)
def bookexchange(db_name: str):
    """Populate the models from the bookexchange app."""
    populate_bookexchange(db_name)


@app.command()
@timer(logger)
def everything(db_name: str):
    """Populate the models from all apps."""
    populate_books(db_name)
    populate_users(db_name)
    populate_bookexchange(db_name)


if __name__ == "__main__":
    app()
