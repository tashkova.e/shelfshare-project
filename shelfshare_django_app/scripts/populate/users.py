import os
from pathlib import Path

import pandas as pd
from tqdm import tqdm

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")

import django

django.setup()
import logging

logger = logging.getLogger("populate_users")

from shelfshare.settings import OUTSIDE_DATA_DIR

transformed_folder = OUTSIDE_DATA_DIR / "transformed"
USERS_PATH = Path(transformed_folder, "users.csv")
from users.models import Friend, FriendshipRequest, User


def delete_db(db_name="default"):
    logger.info("Deleting models from users app, for db: %s", db_name)
    User.objects.using(db_name).exclude(username="admin").delete()
    Friend.objects.using(db_name).all().delete()
    FriendshipRequest.objects.using(db_name).all().delete()


def populate(db_name="default"):
    logger.info("Populating models from users app, for db: %s", db_name)
    delete_db(db_name)
    users = pd.read_csv(USERS_PATH)
    user_lst = []
    user_manager = User.objects.using(db_name)
    sex_dict = {"male": "M", "female": "F"}
    for user_row in tqdm(users.itertuples(), "creating User objects..."):
        user_lst.append(
            User(username=user_row.username, password="password", email="generic@mail.com", sex=sex_dict[user_row.sex])
        )
    user_manager.bulk_create(user_lst)
