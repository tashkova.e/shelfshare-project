import os
from pathlib import Path

import django
import pandas as pd
from tqdm import tqdm

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")

django.setup()
import logging

logger = logging.getLogger("populate_user_book")

from shelfshare.settings import OUTSIDE_DATA_DIR

transformed_folder = OUTSIDE_DATA_DIR / "transformed"
TO_READ_PATH = Path(transformed_folder, "to_read_filtered.csv")
RATINGS_PATH = Path(transformed_folder, "ratings_filtered_some_set_to_0.csv")

from bookexchange.models import ReadBook, ToRead
from books.models import Book
from users.models import User


def populate_toread(db_name="default"):
    to_read = pd.read_csv(TO_READ_PATH)
    to_read_lst = []
    for row in tqdm(to_read.itertuples(), "creating ToRead objects..."):
        user = User.objects.using(db_name).get(username=row.username)
        book = Book.objects.using(db_name).get(ISBN=row.isbn)
        new_toread = ToRead(user=user, book=book)
        to_read_lst.append(new_toread)
    ToRead.objects.using(db_name).bulk_create(to_read_lst)


def populate_readbooks(db_name="default"):
    read_books = pd.read_csv(RATINGS_PATH)
    readbooks_lst = []
    for row in tqdm(read_books.itertuples(), "creatig ReadBook objects..."):
        user = User.objects.using(db_name).get(username=row.username)
        book = Book.objects.using(db_name).get(ISBN=row.isbn)
        new_readbook = ReadBook(user=user, book=book, rating=row.rating)
        readbooks_lst.append(new_readbook)
    ReadBook.objects.using(db_name).bulk_create(readbooks_lst)


def delete_db(db_name="default"):
    logger.info("Deleting models from bookexchange app, for db: %s", db_name)
    ToRead.objects.using(db_name).all().delete()
    ReadBook.objects.using(db_name).all().delete()


def populate(db_name="default"):
    logger.info("Populating models from bookexchange app, for db: %s", db_name)
    delete_db(db_name)
    populate_toread(db_name)
    populate_readbooks(db_name)
