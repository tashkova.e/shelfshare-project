import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import logging

import typer
from django.apps import apps

logger = logging.getLogger(__name__)
app = typer.Typer()


@app.command()
def delete_db(
    db_name: str,
):
    """Delete all the models in the specified database."""
    logger.info("Deleting all models in the database %s", db_name)
    for model in apps.get_models():
        logger.info("Deleting all instances of %s", model)
        model.objects.using(db_name).all().delete()


if __name__ == "__main__":
    app()
