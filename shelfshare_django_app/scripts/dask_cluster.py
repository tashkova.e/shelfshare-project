import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
from dask.distributed import LocalCluster


def main() -> LocalCluster:
    return LocalCluster(scheduler_port=8786, n_workers=1, threads_per_worker=8)


if __name__ == "__main__":
    print("Starting Local Cluster")
    cluster = main()
    print("Local Cluster Started. Press Ctrl+C to shutdown")
    print("Cluster information:")
    print("Number of workers: ", len(cluster.workers))
    print("Link to the dashboard: ", cluster.dashboard_link)
    try:
        while True:
            pass  # keep the process running
    except KeyboardInterrupt:
        print("Shutting Down Local Cluster")
        cluster.close()
        print("Local Cluster Shutdown")
        sys.exit(0)
