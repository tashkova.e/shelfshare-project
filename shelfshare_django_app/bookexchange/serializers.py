from rest_framework import serializers
from users.models import Friend

from .models import BookOwnership, BookRequest, ReadBook, ToRead


class ToReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToRead
        fields = [
            "id",
            "user",
            "book",
            "added_at",
        ]
        read_only_fields = ("user", "added_at")

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        try:
            return ToRead.objects.create(**validated_data)
        except Exception as e:
            raise serializers.ValidationError(str(e))


class ReadBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReadBook
        fields = [
            "id",
            "user",
            "book",
            "rating",
            "added_at",
        ]
        read_only_fields = ("user", "added_at")

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        try:
            return ReadBook.objects.create(**validated_data)
        except Exception as e:
            raise serializers.ValidationError(str(e))


class BookOwnershipSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookOwnership
        fields = [
            "id",
            "owner",
            "book",
            "quantity",
        ]
        read_only_fields = ("owner",)

    def create(self, validated_data):
        validated_data["owner"] = self.context["request"].user
        try:
            return BookOwnership.objects.create(**validated_data)
        except Exception as e:
            raise serializers.ValidationError(str(e))


class BookRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookRequest
        fields = [
            "id",
            "from_user",
            "book_ownership",
            "status",
            "message",
            "created_at",
            "responded_at",
            "days_to_return",
            "lent_at",
            "returned_at",
        ]
        read_only_fields = (
            "from_user",
            "status",
            "created_at",
            "responded_at",
            "lent_at",
            "returned_at",
        )

    def validate(self, attrs):
        if self.context["view"].action == "create":
            # check if the requester and the owner of the book are friends
            # if not, raise an exception
            requester = self.context["request"].user
            # get the book ownership object from the request data
            book_ownership = attrs["book_ownership"]
            owner = book_ownership.owner
            if not Friend.objects.are_friends(requester, owner):
                raise serializers.ValidationError("You are not friends with the owner of the book")
            # check if the requester and the owner of the book are the same person
            # if so, raise an exception
            if requester == owner:
                raise serializers.ValidationError("You cannot request a book from yourself")
            # check if the requester has already requested the book and the request is still pending
            # if so, raise an exception
            if BookRequest.objects.filter(from_user=requester, book_ownership=book_ownership, status="P").exists():
                raise serializers.ValidationError("You have already requested this book")

        request = self.context["request"]
        if request.method == "POST" and self.context["view"].action == "accept":
            days_to_return = attrs.get("days_to_return", None)

            # Validating 'days_to_return' to be a positive integer
            if days_to_return is None or days_to_return <= 0:
                raise serializers.ValidationError("Days to return must be a positive integer.")
        else:
            # For actions other than 'accept', ensure 'days_to_return' isn't being modified
            if "days_to_return" in attrs:
                raise serializers.ValidationError("Days to return can only be set during the 'accept' action.")

        # Add any other object-level validations here

        return super().validate(attrs)

    def create(self, validated_data):
        validated_data["from_user"] = self.context["request"].user
        validated_data["status"] = "P"
        try:
            return BookRequest.objects.create(**validated_data)
        except Exception as e:
            raise serializers.ValidationError(str(e))
