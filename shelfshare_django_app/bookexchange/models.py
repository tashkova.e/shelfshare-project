from typing import Any

from books.models import Book
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from users.models import User


# Create your models here.
class ToRead(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_to_read")
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book_to_read")
    added_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["id"]
        unique_together = ("user", "book")


class ReadBook(models.Model):
    Rating_Choices = [
        ("0", "0"),  # representing no rating
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),
    ]
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_read")
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book_read")
    rating = models.CharField(max_length=1, choices=Rating_Choices, default="0")
    added_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["id"]
        unique_together = ("user", "book")


class BookOwnershipManager(models.Manager):
    def create(self, **kwargs: Any) -> Any:
        if kwargs["quantity"] < 1:
            raise ValidationError("Quantity must be greater than 0")
        return super().create(**kwargs)


class BookOwnership(models.Model):
    objects = BookOwnershipManager()
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="owner")
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book")
    quantity = models.PositiveIntegerField(default=1)

    class Meta:
        ordering = ["id"]
        unique_together = ("owner", "book")

    def get_quantity_of_book(self):
        return self.quantity

    def increment_quantity(self):
        self.quantity += 1
        self.save()
        return True

    def decrement_quantity(self):
        if self.quantity > 0:
            self.quantity -= 1
            self.save()
            return True
        else:
            return False


class BookRequest(models.Model):
    STATUS_CHOICES = [
        ("P", "Pending"),  # waiting for response
        ("R", "Rejected"),  # the request was rejected
        ("A", "Accepted"),  # the request was accepted
        ("L", "Lent"),  # the lender gave the book to the borrower
        ("B", "Returned"),  # the borrower returned the book to the lender
        ("C", "Cancelled"),  # the request was cancelled by the borrower
    ]
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="from_user_request")
    book_ownership = models.ForeignKey(BookOwnership, on_delete=models.CASCADE, related_name="book_ownership_request")
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default="P")
    message = models.TextField("Message", blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    responded_at = models.DateTimeField(blank=True, null=True)
    days_to_return = models.IntegerField(blank=True, null=True)
    lent_at = models.DateTimeField(blank=True, null=True)
    returned_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ["id"]

    def cancel_request(self):
        if self.status in ["P", "A"]:
            self.status = "C"
            self.save()
            return True
        else:
            return False

    def cancel_lending(self):
        if self.status in ["A"]:
            self.status = "C"
            self.save()
            return True
        else:
            return False

    def reject(self):
        if self.status in ["P"]:
            self.status = "R"
            self.responded_at = timezone.now()
            self.save()
            return True
        else:
            return False

    def accept(self, days_to_return=None):
        if self.status in ["P"]:
            self.status = "A"
            self.responded_at = timezone.now()
            self.days_to_return = days_to_return
            self.save()
            return True
        else:
            return False

    def give_book(self):
        if self.status in ["A"] and self.book_ownership.get_quantity_of_book() > 0:
            self.book_ownership.decrement_quantity()
            self.status = "L"
            self.lent_at = timezone.now()
            self.save()
            return True
        else:
            return False

    def return_book(self):
        if self.status in ["L"]:
            self.book_ownership.increment_quantity()
            self.status = "B"
            self.returned_at = timezone.now()
            self.save()
            return True
        else:
            return False
