from books.models import Author, Book
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory
from users.models import Friend

from ..models import BookOwnership, BookRequest, ReadBook, ToRead, User
from ..views import (
    BookOwnershipViewSet,
    BookRequestViewSet,
    ReadBookViewSet,
    ToReadViewSet,
)


class ToReadViewSetTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.uri = "/api/api-bookexchange/toread/"
        self.user = User.objects.create_user("bob", "bob@mail.com", "bobpassword")
        self.admin = User.objects.create_superuser("admin", "admin@mail.com", "adminpassword")
        self.book1 = Book.objects.create(title="book1", ISBN="1234567890123")
        self.author = Author.objects.create(name="author1")
        self.book1.author.add(self.author)
        self.book1.save()

    def test_list(self):
        ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = ToReadViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_list_not_authenticated(self):
        ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        response = ToReadViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        request = self.factory.post(self.uri, {"book": self.book1.id}, format="json")
        request.user = self.user
        response = ToReadViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ToRead.objects.count(), 1)
        self.assertEqual(ToRead.objects.get().user, self.user)
        self.assertEqual(ToRead.objects.get().book, self.book1)

    def test_create_not_authenticated(self):
        request = self.factory.post(self.uri, {"book": self.book1.id}, format="json")
        response = ToReadViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_invalid_book(self):
        request = self.factory.post(self.uri, {"book": 100}, format="json")
        request.user = self.user
        response = ToReadViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ToRead.objects.count(), 0)

    def test_retrieve(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = ToReadViewSet.as_view({"get": "retrieve"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], to_read.id)
        self.assertEqual(response.data["user"], self.user.id)
        self.assertEqual(response.data["book"], self.book1.id)

    def test_retrieve_not_authenticated(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        response = ToReadViewSet.as_view({"get": "retrieve"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"book": self.book1.id}, format="json")
        request.user = self.user
        response = ToReadViewSet.as_view({"put": "update"})(request, pk=to_read.id)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "The response status code is not 200, response:" + str(response.data),
        )
        self.assertEqual(ToRead.objects.get().book, self.book1)

    def test_update_not_authenticated(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"book": self.book1.id}, format="json")
        response = ToReadViewSet.as_view({"put": "update"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_invalid_book(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"book": 100}, format="json")
        request.user = self.user
        response = ToReadViewSet.as_view({"put": "update"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ToRead.objects.get().book, self.book1)

    def test_delete(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        request.user = self.user
        response = ToReadViewSet.as_view({"delete": "destroy"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ToRead.objects.count(), 0)

    def test_delete_not_authenticated(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        response = ToReadViewSet.as_view({"delete": "destroy"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_admin(self):
        to_read = ToRead.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        request.user = self.admin
        response = ToReadViewSet.as_view({"delete": "destroy"})(request, pk=to_read.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ToRead.objects.count(), 0)


class ReadBookViewSetTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.uri = "/api/api-bookexchange/readbooks/"
        self.user = User.objects.create_user("bob", "bob@mail.com", "bobpassword")
        self.admin = User.objects.create_superuser("admin", "admin@mail.com", "adminpassword")
        self.book1 = Book.objects.create(title="book1", ISBN="1234567890123")
        self.author = Author.objects.create(name="author1")
        self.book1.author.add(self.author)
        self.book1.save()

    def test_list(self):
        ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = ReadBookViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_list_not_authenticated(self):
        ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        response = ReadBookViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        request = self.factory.post(self.uri, {"book": self.book1.id}, format="json")
        request.user = self.user
        response = ReadBookViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ReadBook.objects.count(), 1)
        self.assertEqual(ReadBook.objects.get().user, self.user)
        self.assertEqual(ReadBook.objects.get().book, self.book1)
        self.assertEqual(ReadBook.objects.get().rating, "0")

    def test_create_not_authenticated(self):
        request = self.factory.post(self.uri, {"book": self.book1.id}, format="json")
        response = ReadBookViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_invalid_book(self):
        request = self.factory.post(self.uri, {"book": 100}, format="json")
        request.user = self.user
        response = ReadBookViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ReadBook.objects.count(), 0)

    def test_create_invalid_rating(self):
        request = self.factory.post(self.uri, {"book": self.book1.id, "rating": 6}, format="json")
        request.user = self.user
        response = ReadBookViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ReadBook.objects.count(), 0)

    def test_retrieve(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = ReadBookViewSet.as_view({"get": "retrieve"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], read_book.id)
        self.assertEqual(response.data["user"], self.user.id)
        self.assertEqual(response.data["book"], self.book1.id)
        self.assertEqual(response.data["rating"], "0")

    def test_retrieve_not_authenticated(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.get(self.uri)
        response = ReadBookViewSet.as_view({"get": "retrieve"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"rating": "5", "book": self.book1.id}, format="json")
        request.user = self.user
        response = ReadBookViewSet.as_view({"put": "update"})(request, pk=read_book.id)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "The response status code is not 200, response:" + str(response.data),
        )
        self.assertEqual(ReadBook.objects.get().rating, "5")

    def test_update_not_authenticated(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"rating": "5", "book": self.book1.id}, format="json")
        response = ReadBookViewSet.as_view({"put": "update"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_invalid_rating(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.put(self.uri, {"rating": "6", "book": self.book1.id}, format="json")
        request.user = self.user
        response = ReadBookViewSet.as_view({"put": "update"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ReadBook.objects.get().rating, "0")

    def test_delete(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        request.user = self.user
        response = ReadBookViewSet.as_view({"delete": "destroy"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ReadBook.objects.count(), 0)

    def test_delete_not_authenticated(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        response = ReadBookViewSet.as_view({"delete": "destroy"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(ReadBook.objects.count(), 1)

    def test_delete_admin(self):
        read_book = ReadBook.objects.create(user=self.user, book=self.book1)
        request = self.factory.delete(self.uri)
        request.user = self.admin
        response = ReadBookViewSet.as_view({"delete": "destroy"})(request, pk=read_book.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ReadBook.objects.count(), 0)


class BookOwnershipViewSetTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.uri = "/api/api-bookexchange/bookownerships/"
        self.user = User.objects.create_user("bob", "bob@mail.com", "bobpassword")
        self.admin = User.objects.create_superuser("admin", "admin@mail.com", "adminpassword")
        self.book1 = Book.objects.create(title="book1", ISBN="1234567890123")
        self.author = Author.objects.create(name="author1")
        self.book1.author.add(self.author)
        self.book1.save()

    def test_list(self):
        BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_list_not_authenticated(self):
        BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.get(self.uri)
        response = BookOwnershipViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        request = self.factory.post(self.uri, {"book": self.book1.id, "quantity": 1}, format="json")
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookOwnership.objects.count(), 1)
        self.assertEqual(BookOwnership.objects.get().owner, self.user)
        self.assertEqual(BookOwnership.objects.get().book, self.book1)
        self.assertEqual(BookOwnership.objects.get().quantity, 1)

    def test_create_not_authenticated(self):
        request = self.factory.post(self.uri, {"book": self.book1.id, "quantity": 1}, format="json")
        response = BookOwnershipViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_invalid_quantity(self):
        request = self.factory.post(self.uri, {"book": self.book1.id, "quantity": 0}, format="json")
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookOwnership.objects.count(), 0)

    def test_create_invalid_book(self):
        request = self.factory.post(self.uri, {"book": 100, "quantity": 1}, format="json")
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookOwnership.objects.count(), 0)

    def test_retrieve(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.get(self.uri)
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"get": "retrieve"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_ownership.id)
        self.assertEqual(response.data["owner"], self.user.id)
        self.assertEqual(response.data["book"], self.book1.id)
        self.assertEqual(response.data["quantity"], 1)

    def test_retrieve_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.get(self.uri)
        response = BookOwnershipViewSet.as_view({"get": "retrieve"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.put(self.uri, {"quantity": 2, "book": self.book1.id}, format="json")
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"put": "update"})(request, pk=book_ownership.id)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "The response status code is not 200, response:" + str(response.data),
        )
        self.assertEqual(BookOwnership.objects.get().quantity, 2)

    def test_update_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.put(self.uri, {"quantity": 2, "book": self.book1.id}, format="json")
        response = BookOwnershipViewSet.as_view({"put": "update"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_invalid_quantity(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.put(self.uri, {"quantity": -1, "book": self.book1.id}, format="json")
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"put": "update"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookOwnership.objects.get().quantity, 1)

    def test_delete(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.delete(self.uri)
        request.user = self.user
        response = BookOwnershipViewSet.as_view({"delete": "destroy"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(BookOwnership.objects.count(), 0)

    def test_delete_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.delete(self.uri)
        response = BookOwnershipViewSet.as_view({"delete": "destroy"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_admin(self):
        book_ownership = BookOwnership.objects.create(owner=self.user, book=self.book1, quantity=1)
        request = self.factory.delete(self.uri)
        request.user = self.admin
        response = BookOwnershipViewSet.as_view({"delete": "destroy"})(request, pk=book_ownership.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(BookOwnership.objects.count(), 0)


class BookRequestViewSetTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.uri = "/api/api-bookexchange/bookrequests/"
        self.user_bob = User.objects.create_user("bob", "bob@mail.com", "bobpassword")
        self.user_steve = User.objects.create_user("steve", "steve@mail", "stevepassword")
        self.admin = User.objects.create_superuser("admin", "admin@mail.com", "adminpassword")
        self.book1 = Book.objects.create(title="book1", ISBN="1234567890123")
        self.author = Author.objects.create(name="author1")
        self.book1.author.add(self.author)
        self.book1.save()

    def test_list(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_list_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        response = BookRequestViewSet.as_view({"get": "list"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        request = self.factory.post(self.uri, {"book_ownership": book_ownership.id}, format="json")
        # make steve and bob friends
        Friend.objects.create(friend1=self.user_steve, friend2=self.user_bob)
        Friend.objects.create(friend1=self.user_bob, friend2=self.user_steve)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookRequest.objects.count(), 1)
        self.assertEqual(BookRequest.objects.get().from_user, self.user_steve)
        self.assertEqual(BookRequest.objects.get().book_ownership, book_ownership)
        self.assertEqual(BookRequest.objects.get().status, "P")

    def test_create_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        request = self.factory.post(self.uri, {"book_ownership": book_ownership.id}, format="json")
        response = BookRequestViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_invalid_book_ownership(self):
        request = self.factory.post(self.uri, {"book_ownership": 100}, format="json")
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 0)

    def test_create_not_friends(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        request = self.factory.post(self.uri, {"book_ownership": book_ownership.id}, format="json")
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 0)

    def test_create_to_self(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        request = self.factory.post(self.uri, {"book_ownership": book_ownership.id}, format="json")
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "create"})(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 0)

    def test_retrieve(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "retrieve"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_steve.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")

    def test_retrieve_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        response = BookRequestViewSet.as_view({"get": "retrieve"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.put(self.uri, {"status": "A"}, format="json")
        request.user = self.user_bob
        response = BookRequestViewSet.as_view({"put": "update"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.delete(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"delete": "destroy"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(BookRequest.objects.count(), 0)

    def test_delete_admin(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.delete(self.uri)
        request.user = self.admin
        response = BookRequestViewSet.as_view({"delete": "destroy"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(BookRequest.objects.count(), 0)

    def test_delete_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.delete(self.uri)
        response = BookRequestViewSet.as_view({"delete": "destroy"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_request_get(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "cancel_request"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_steve.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")

    def test_cancel_request_post(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "cancel_request"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "C")

    def test_cancel_request_post_with_data(self):
        # data in the post request gets ignored
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.post(self.uri, {"status": "A"}, format="json")
        request.user = self.user_steve
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "cancel_request"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "C")

    def test_cancel_request_not_authenticated(self):
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.post(self.uri)
        response = BookRequestViewSet.as_view({"post": "cancel_request"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_request_not_owner(self):
        # only the sender of the request can cancel it
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "cancel_request"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_accept_get(self):
        # get request returns the book request instance
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.get(self.uri)
        request.user = self.user_bob
        response = BookRequestViewSet.as_view({"get": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_steve.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")
        self.assertIsNone(response.data["days_to_return"])
        self.assertIsNone(response.data["responded_at"])

    def test_accept_post(self):
        # post request accepts the book request
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.post(
            self.uri, {"book_ownership": book_ownership.id, "days_to_return": 10}, format="json"
        )
        request.user = self.user_bob
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK, "response:" + str(response.data))
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "A")
        self.assertEqual(new_book_request.days_to_return, 10)
        self.assertIsNotNone(new_book_request.responded_at)

    def test_accept_post_invalid_days_to_return(self):
        # post request accepts the book request
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.post(self.uri, {"book_ownership": book_ownership.id, "days_to_return": 0}, format="json")
        request.user = self.user_bob
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "P")

    def test_accept_post_not_owner(self):
        # only the receiver of the request can accept it
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        request = self.factory.post(
            self.uri, {"book_ownership": book_ownership.id, "days_to_return": 10}, format="json"
        )
        request.user = self.user_bob
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "P")

    def test_accept_post_admin(self):
        # admin can accept the request
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        request = self.factory.post(
            self.uri, {"book_ownership": book_ownership.id, "days_to_return": 10}, format="json"
        )
        request.user = self.admin
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "A")

    def test_accept_post_already_accepted(self):
        # if the request is already accepted, the request is not changed
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
            status="A",
        )
        request = self.factory.post(
            self.uri, {"book_ownership": book_ownership.id, "days_to_return": 10}, format="json"
        )
        request.user = self.user_bob
        self.assertEqual(book_request.status, "A")
        response = BookRequestViewSet.as_view({"post": "accept"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "A")

    def test_reject_get(self):
        # get request returns the book request instance
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.get(self.uri)
        request.user = self.user_bob
        response = BookRequestViewSet.as_view({"get": "reject"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_steve.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")
        self.assertIsNone(response.data["responded_at"])

    def test_reject_post(self):
        # reject the book request
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "reject"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "R")
        self.assertIsNotNone(new_book_request.responded_at)

    def test_reject_admin(self):
        # admin can reject the request
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
        )
        request = self.factory.post(self.uri)
        request.user = self.admin
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "reject"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "R")
        self.assertIsNotNone(new_book_request.responded_at)

    def test_reject_not_owner(self):
        # only the receiver of the request can reject it
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "reject"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "P")

    def test_reject_already_rejected(self):
        # if the request is already rejected, the request is not changed
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_steve,
            book_ownership=book_ownership,
            status="R",
        )
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "R")
        response = BookRequestViewSet.as_view({"post": "reject"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "R")

    def test_cancel_lending_get(self):
        # get request returns the book request instance
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_bob.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")
        self.assertIsNone(response.data["responded_at"])

    def test_cancel_lending_post(self):
        # cancel the book lending
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "A")
        response = BookRequestViewSet.as_view({"post": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "C")
        self.assertIsNotNone(new_book_request.responded_at)

    def test_cancel_lending_not_accepted(self):
        # if the request is not accepted, you cant't cancel the lending
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "P")

    def test_cancel_lending_not_owner(self):
        # only the sender of the request can cancel it
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_cancel_lending_already_cancelled(self):
        # if the request is already cancelled, you can't cancel it again
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership, status="C")
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "C")
        response = BookRequestViewSet.as_view({"post": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "C")

    def test_cancel_lending_admin(self):
        # admin can cancel the request
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.admin
        response = BookRequestViewSet.as_view({"post": "cancel_lending"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "C")

    def test_give_book_get(self):
        # get request returns the book request instance
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(
            from_user=self.user_bob,
            book_ownership=book_ownership,
        )
        book_request.accept()
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_bob.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "A")
        self.assertIsNone(response.data["lent_at"])

    def test_give_book_post(self):
        # give the book to the requester
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "A")
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "L")
        self.assertIsNotNone(new_book_request.lent_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 0)

    def test_give_book_zero_books(self):
        # you can't give a book if you have zero books
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        book_ownership.decrement_quantity()
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "A")
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "A")
        self.assertIsNone(new_book_request.lent_at)

    def test_give_book_not_owner(self):
        # only the owner can give the book
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_give_book_admin(self):
        # admin can give the book
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.admin
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "L")
        self.assertIsNotNone(new_book_request.lent_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 0)

    def test_give_book_not_accepted(self):
        # you can't give the book if the request is not accepted
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "P")
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "P")
        self.assertIsNone(new_book_request.lent_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_give_book_already_lent(self):
        # you can't give the book if it is already lent
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        book_request.give_book()
        request = self.factory.post(self.uri)
        request.user = self.user_steve
        self.assertEqual(book_request.status, "L")
        response = BookRequestViewSet.as_view({"post": "give_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "L")

    def test_return_book_get(self):
        # get request returns the book request instance
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        request = self.factory.get(self.uri)
        request.user = self.user_steve
        response = BookRequestViewSet.as_view({"get": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], book_request.id)
        self.assertEqual(response.data["from_user"], self.user_bob.id)
        self.assertEqual(response.data["book_ownership"], book_ownership.id)
        self.assertEqual(response.data["status"], "P")
        self.assertIsNone(response.data["returned_at"])
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_return_book_post(self):
        # return the book
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        book_request.give_book()
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "L")
        response = BookRequestViewSet.as_view({"post": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "B")
        self.assertIsNotNone(new_book_request.returned_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_return_book_not_borrowed(self):
        # you can't return the book if it is not borrowed
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership)
        book_request.accept()
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "A")
        response = BookRequestViewSet.as_view({"post": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "A")
        self.assertIsNone(new_book_request.returned_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_return_book_not_borrower(self):
        # only the borrower can return the book
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        book_request.accept()
        book_request.give_book()
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        response = BookRequestViewSet.as_view({"post": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 0)

    def test_return_book_admin(self):
        # admin can return the book
        book_ownership = BookOwnership.objects.create(owner=self.user_bob, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_steve, book_ownership=book_ownership)
        book_request.accept()
        book_request.give_book()
        request = self.factory.post(self.uri)
        request.user = self.admin
        response = BookRequestViewSet.as_view({"post": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "B")
        self.assertIsNotNone(new_book_request.returned_at)
        new_book_ownership = BookOwnership.objects.get(id=book_ownership.id)
        self.assertEqual(new_book_ownership.quantity, 1)

    def test_return_book_already_returned(self):
        # you can't return the book if it is already returned
        book_ownership = BookOwnership.objects.create(owner=self.user_steve, book=self.book1, quantity=1)
        book_request = BookRequest.objects.create(from_user=self.user_bob, book_ownership=book_ownership, status="B")
        request = self.factory.post(self.uri)
        request.user = self.user_bob
        self.assertEqual(book_request.status, "B")
        response = BookRequestViewSet.as_view({"post": "return_book"})(request, pk=book_request.id)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        new_book_request = BookRequest.objects.get(id=book_request.id)
        self.assertEqual(new_book_request.status, "B")
