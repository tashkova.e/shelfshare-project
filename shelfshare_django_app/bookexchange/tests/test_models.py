from books.models import Author, Book
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone
from users.models import User

from ..models import BookOwnership, BookRequest, ReadBook, ToRead


class ToReadTest(TestCase):
    """Test module for ToRead model"""

    def setUp(self):
        self.user = User.objects.create(username="testuser1", password="testpassword1", email="generic@mail.com")
        self.author = Author.objects.create(name="testauthor1")
        self.book = Book.objects.create(title="testtitle1", ISBN="1234567890")
        self.book.author.add(self.author)

    def test_to_read_create(self):
        toread = ToRead.objects.create(user=self.user, book=self.book)
        self.assertEqual(toread.user, self.user)
        self.assertEqual(toread.book, self.book)

    def test_to_read_create_duplicate(self):
        ToRead.objects.create(user=self.user, book=self.book)
        with self.assertRaises(IntegrityError):
            ToRead.objects.create(user=self.user, book=self.book)


class ReadBookTest(TestCase):
    """Test module for ReadBook model"""

    def setUp(self):
        self.user = User.objects.create(username="testuser1", password="testpassword1", email="generic@mail.com")
        self.author = Author.objects.create(name="testauthor1")
        self.book = Book.objects.create(title="testtitle1", ISBN="1234567890")
        self.book.author.add(self.author)

    def test_read_book_create(self):
        readbook = ReadBook.objects.create(user=self.user, book=self.book, rating=5)
        self.assertEqual(readbook.user, self.user)
        self.assertEqual(readbook.book, self.book)
        self.assertEqual(readbook.rating, 5)

    def test_read_book_create_duplicate(self):
        ReadBook.objects.create(user=self.user, book=self.book)
        with self.assertRaises(IntegrityError):
            ReadBook.objects.create(user=self.user, book=self.book)


class BookOwnershipTest(TestCase):
    """Test module for BookOwnership model"""

    def setUp(self):
        self.user = User.objects.create(username="testuser1", password="testpassword1", email="generic@mail.com")
        self.author = Author.objects.create(name="testauthor1")
        self.book = Book.objects.create(title="testtitle1", ISBN="1234567890")
        self.book.author.add(self.author)

    def test_bookownership_create(self):
        bookownership = BookOwnership.objects.create(owner=self.user, book=self.book, quantity=1)
        self.assertEqual(bookownership.owner, self.user)
        self.assertEqual(bookownership.book, self.book)
        self.assertEqual(bookownership.quantity, 1)

    def test_get_quantity_of_book(self):
        bookownership = BookOwnership.objects.create(owner=self.user, book=self.book, quantity=1)
        self.assertEqual(bookownership.get_quantity_of_book(), 1)

    def test_increment_quantity(self):
        bookownership = BookOwnership.objects.create(owner=self.user, book=self.book, quantity=1)
        bookownership.increment_quantity()
        self.assertEqual(bookownership.quantity, 2)

    def test_decrement_quantity(self):
        bookownership = BookOwnership.objects.create(owner=self.user, book=self.book, quantity=2)
        bookownership.decrement_quantity()
        self.assertEqual(bookownership.quantity, 1)

    def test_decrement_quantity_negative(self):
        bookownership = BookOwnership.objects.create(owner=self.user, book=self.book, quantity=1)
        self.assertTrue(bookownership.decrement_quantity())
        self.assertFalse(bookownership.decrement_quantity())


class BookRequestTest(TestCase):
    """Test module for BookRequest model"""

    def setUp(self):
        self.user1 = User.objects.create(username="testuser1", password="testpassword1", email="generic1@mail.com")
        self.user2 = User.objects.create(username="testuser2", password="testpassword2", email="generic2@mail.com")
        self.book = Book.objects.create(title="testtitle1", ISBN="1234567890")
        self.book.author.add(Author.objects.create(name="testauthor1"))
        self.bookownership = BookOwnership.objects.create(owner=self.user1, book=self.book, quantity=1)

    def test_bookrequest_create(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        self.assertEqual(bookrequest.from_user, self.user1)
        self.assertEqual(bookrequest.book_ownership, self.bookownership)
        self.assertEqual(bookrequest.status, "P")
        self.assertEqual(bookrequest.message, "testmessage")

    def test_cancel_bookrequest(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        book_request_id = bookrequest.id
        self.assertTrue(bookrequest.cancel_request())
        self.assertEqual(BookRequest.objects.get(id=book_request_id).status, "C")

    def test_bookrequest_cancel_book_at_borrower(self):
        # the borrower has the book, he can't cancel the request
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="L", message="testmessage"
        )
        self.assertFalse(bookrequest.cancel_request())

    def test_cancel_accepted_bookrequest(self):
        # the owner accepted the request, but changed his mind
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="A", message="testmessage"
        )
        self.assertTrue(bookrequest.cancel_lending())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "C")

    def test_cancel_rejected_bookrequest(self):
        # canceling by the owner can only be done if the status was 'A'
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="R", message="testmessage"
        )
        self.assertFalse(bookrequest.cancel_lending())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "R")

    def test_reject_bookrequest(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        book_request_id = bookrequest.id
        bookrequest.reject()
        self.assertEqual(BookRequest.objects.get(id=book_request_id).status, "R")
        self.assertIsNotNone(BookRequest.objects.get(id=book_request_id).responded_at)

    def test_reject_book_request_invalid(self):
        # rejecting can only be done if the status was pending
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="A", message="testmessage"
        )
        self.assertFalse(bookrequest.reject())

    def test_accept_bookrequest(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        book_request_id = bookrequest.id
        bookrequest.accept()
        self.assertEqual(BookRequest.objects.get(id=book_request_id).status, "A")
        self.assertIsNotNone(BookRequest.objects.get(id=book_request_id).responded_at)

    def test_accept_book_request_invalid(self):
        # accepting can only be done if the status was pending
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="A", message="testmessage"
        )
        self.assertFalse(bookrequest.accept())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "A")

    def test_give_book(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="A", message="testmessage"
        )
        book_request_id = bookrequest.id
        quantity_before = self.bookownership.quantity
        bookrequest.give_book()
        self.assertEqual(BookRequest.objects.get(id=book_request_id).status, "L")
        self.assertIsNotNone(BookRequest.objects.get(id=book_request_id).lent_at)
        self.assertEqual(self.bookownership.quantity, quantity_before - 1)

    def test_give_book_invalid_status(self):
        # giving can only be done if the status was accepted
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        quantity_before = self.bookownership.quantity
        self.assertFalse(bookrequest.give_book())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "P")
        self.assertEqual(self.bookownership.quantity, quantity_before)

    def test_give_book_invalid_quantity(self):
        # giving can only be done if the quantity was positive
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="A", message="testmessage"
        )
        self.bookownership.quantity = 0
        self.bookownership.save()
        quantity_before = self.bookownership.quantity

        self.assertFalse(bookrequest.give_book())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "A")
        self.assertEqual(self.bookownership.quantity, quantity_before)

    def test_return_book(self):
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="L", message="testmessage"
        )
        book_request_id = bookrequest.id
        quantity_before = self.bookownership.quantity
        bookrequest.return_book()
        self.assertEqual(BookRequest.objects.get(id=book_request_id).status, "B")
        self.assertIsNotNone(BookRequest.objects.get(id=book_request_id).returned_at)
        self.assertEqual(self.bookownership.quantity, quantity_before + 1)

    def test_return_book_invalid_status(self):
        # returning can only be done if the status was lent
        bookrequest = BookRequest.objects.create(
            from_user=self.user1, book_ownership=self.bookownership, status="P", message="testmessage"
        )
        quantity_before = self.bookownership.quantity
        self.assertFalse(bookrequest.return_book())
        self.assertEqual(BookRequest.objects.get(id=bookrequest.id).status, "P")
        self.assertEqual(self.bookownership.quantity, quantity_before)
