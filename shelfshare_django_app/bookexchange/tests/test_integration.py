from books.models import Book
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from users.models import Friend, User

from ..models import BookOwnership, BookRequest, ReadBook, ToRead


class BookExchangeIntegrationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client1 = APIClient()
        cls.client2 = APIClient()
        cls.client3 = APIClient()
        cls.user_bob = User.objects.create_user(username="bob", password="password", email="bob@mail.com")
        cls.user_steve = User.objects.create_user(username="steve", password="password", email="steve@mail.com")
        cls.user_john = User.objects.create_user(username="john", password="password", email="john@mail.com")
        cls.book1 = Book.objects.create(
            ISBN="1234567890123",
            title="Sample Book 1",
            description="A sample description.",
            imageURL="https://example.com/image.jpg",
            publisher="Sample Publisher",
            publishDate="2023-01-01",
            goodreadsURL="https://www.goodreads.com/book/123456",
        )
        cls.book2 = Book.objects.create(
            ISBN="1234567890124",
            title="Sample Book 2",
            description="A sample description.",
            imageURL="https://example.com/image.jpg",
            publisher="Sample Publisher",
            publishDate="2023-01-01",
            goodreadsURL="https://www.goodreads.com/book/123456",
        )
        Friend.objects.create(friend1=cls.user_bob, friend2=cls.user_steve)
        Friend.objects.create(friend1=cls.user_steve, friend2=cls.user_bob)
        cls.assertTrue(cls, Friend.objects.are_friends(cls.user_bob, cls.user_steve))

    def test_add_book_to_read(self):
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/toread/", {"book": self.book1.id})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ToRead.objects.get(id=response.data["id"]).user, self.user_bob)
        self.assertEqual(ToRead.objects.get(id=response.data["id"]).book, self.book1)

    def test_add_book_to_read_twice(self):
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/toread/", {"book": self.book1.id})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client1.post("/bookexchange-api/toread/", {"book": self.book1.id})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_book_read_book(self):
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/readbooks/", {"book": self.book1.id, "rating": 5})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ReadBook.objects.get(id=response.data["id"]).user, self.user_bob)
        self.assertEqual(ReadBook.objects.get(id=response.data["id"]).book, self.book1)
        self.assertEqual(ReadBook.objects.get(id=response.data["id"]).rating, "5")

    def test_add_book_read_book_twice(self):
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/readbooks/", {"book": self.book1.id, "rating": 5})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client1.post("/bookexchange-api/readbooks/", {"book": self.book1.id, "rating": 5})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_normal_workflow(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]

        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).owner, self.user_bob)
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).book, self.book1)
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).quantity, 1)
        # steve requests the book from bob
        self.client2.login(username="steve", password="password")
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_request = response.data["id"]
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).from_user, self.user_steve)
        self.assertEqual(
            BookRequest.objects.get(id=id_of_book_request).book_ownership,
            BookOwnership.objects.get(id=id_of_book_ownership),
        )
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).status, "P")
        # bob accepts the request
        response = self.client1.post(
            "/bookexchange-api/bookrequests/" + str(id_of_book_request) + "/accept/",
            {"book_ownership": id_of_book_ownership, "days_to_return": 10},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).status, "A")
        # bob gives the book to steve
        response = self.client1.post("/bookexchange-api/bookrequests/" + str(id_of_book_request) + "/give_book/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).status, "L")
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).quantity, 0)
        # steve returns the book to bob
        response = self.client2.post("/bookexchange-api/bookrequests/" + str(id_of_book_request) + "/return_book/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).status, "B")
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).quantity, 1)

    def test_request_book_from_a_non_friend(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]
        # john requests the book from bob
        self.client2.login(username="john", password="password")
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 0)
        self.assertEqual(str(response.data["non_field_errors"][0]), "You are not friends with the owner of the book")

    def test_request_book_from_yourself(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]
        # bob requests the book from himself
        response = self.client1.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 0)
        self.assertEqual(str(response.data["non_field_errors"][0]), "You are not friends with the owner of the book")

    def test_request_book_twice(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]
        # steve requests the book from bob
        self.client2.login(username="steve", password="password")
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # steve requests the book from bob again
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.count(), 1)
        self.assertEqual(str(response.data["non_field_errors"][0]), "You have already requested this book")

    def test_request_book_cancel_request_again(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]
        # steve requests the book from bob
        self.client2.login(username="steve", password="password")
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_request = response.data["id"]
        # bob accepts the request
        response = self.client1.post(
            "/bookexchange-api/bookrequests/" + str(id_of_book_request) + "/accept/",
            {"book_ownership": id_of_book_ownership, "days_to_return": 10},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # steve cancels the request
        response = self.client2.post("/bookexchange-api/bookrequests/" + str(id_of_book_request) + "/cancel_request/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request).status, "C")
        self.assertEqual(BookRequest.objects.count(), 1)
        # steve sends another request
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_request = response.data["id"]
        self.assertEqual(BookRequest.objects.count(), 2)

    def test_two_users_request_the_same_book(self):
        # creating of book ownership
        self.client1.login(username="bob", password="password")
        response = self.client1.post("/bookexchange-api/bookownerships/", {"book": self.book1.id, "quantity": 1})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_ownership = response.data["id"]
        # steve requests the book from bob
        self.client2.login(username="steve", password="password")
        response = self.client2.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_book_request_steve = response.data["id"]
        # add john as a friend of bob
        Friend.objects.create(friend1=self.user_bob, friend2=self.user_john)
        Friend.objects.create(friend1=self.user_john, friend2=self.user_bob)
        # john requests the book from bob
        self.client3.login(username="john", password="password")
        response = self.client3.post("/bookexchange-api/bookrequests/", {"book_ownership": id_of_book_ownership})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookRequest.objects.count(), 2)
        id_of_book_request_john = response.data["id"]
        # bob accepts the request of steve
        response = self.client1.post(
            "/bookexchange-api/bookrequests/" + str(id_of_book_request_steve) + "/accept/",
            {"book_ownership": id_of_book_ownership, "days_to_return": 10},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request_steve).status, "A")
        # bob accepts the request of john
        response = self.client1.post(
            "/bookexchange-api/bookrequests/" + str(id_of_book_request_john) + "/accept/",
            {"book_ownership": id_of_book_ownership, "days_to_return": 10},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request_john).status, "A")
        # bob gives the book to steve
        response = self.client1.post("/bookexchange-api/bookrequests/" + str(id_of_book_request_steve) + "/give_book/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request_steve).status, "L")
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).quantity, 0)
        # bob tries to give the book to john
        response = self.client1.post("/bookexchange-api/bookrequests/" + str(id_of_book_request_john) + "/give_book/")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookRequest.objects.get(id=id_of_book_request_john).status, "A")
        self.assertEqual(BookOwnership.objects.get(id=id_of_book_ownership).quantity, 0)
