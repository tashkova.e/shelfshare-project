from django.shortcuts import render
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import BookOwnership, BookRequest, ReadBook, ToRead
from .permissions import (
    IsOwnerOfBookOwnership,
    IsReceiverOfBookRequest,
    IsSenderOfBookRequest,
    isUserOfBook,
)
from .serializers import (
    BookOwnershipSerializer,
    BookRequestSerializer,
    ReadBookSerializer,
    ToReadSerializer,
)


# Create your views here.
class ToReadViewSet(viewsets.ModelViewSet):
    queryset = ToRead.objects.all()
    serializer_class = ToReadSerializer

    def get_permissions(self):
        if self.action in ["update", "partial_update", "destroy"]:
            permission_classes = [permissions.IsAuthenticated, isUserOfBook | permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]


class ReadBookViewSet(viewsets.ModelViewSet):
    queryset = ReadBook.objects.all()
    serializer_class = ReadBookSerializer

    def get_permissions(self):
        if self.action in ["update", "partial_update", "destroy"]:
            permission_classes = [permissions.IsAuthenticated, isUserOfBook | permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]


class BookOwnershipViewSet(viewsets.ModelViewSet):
    queryset = BookOwnership.objects.all()
    serializer_class = BookOwnershipSerializer

    def get_permissions(self):
        if self.action in ["update", "partial_update", "destroy"]:
            permission_classes = [permissions.IsAuthenticated, IsOwnerOfBookOwnership | permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]


class BookRequestViewSet(viewsets.ModelViewSet):
    queryset = BookRequest.objects.all()
    serializer_class = BookRequestSerializer

    def get_permissions(self):
        if self.action in ["update", "partial_update", "destroy"]:
            permission_classes = [permissions.IsAuthenticated, IsSenderOfBookRequest | permissions.IsAdminUser]
        elif self.action in ["cancel_request", "return_book"]:
            permission_classes = [permissions.IsAuthenticated, IsSenderOfBookRequest | permissions.IsAdminUser]
        elif self.action in ["cancel_lending", "accept", "reject", "give_book"]:
            permission_classes = [permissions.IsAuthenticated, IsReceiverOfBookRequest | permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    # only allow GET and POST requests
    def update(self, request, *args, **kwargs):
        return Response({"error": "Method not allowed"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def partial_update(self, request, *args, **kwargs):
        return Response({"error": "Method not allowed"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @action(detail=True, methods=["get", "post"])
    def cancel_request(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        success = book_request.cancel_request()
        if not success:
            return Response({"error": "Cannot cancel book request"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def cancel_lending(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        success = book_request.cancel_lending()
        if not success:
            return Response({"error": "Cannot cancel book lending"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def accept(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        serializer = self.get_serializer(book_request, data=request.data)
        # Validate the incoming data using the serializer
        if serializer.is_valid():
            # Perform the accept action if the data is valid
            days_to_return = serializer.validated_data.get("days_to_return")
            if book_request.accept(days_to_return=days_to_return):  # Assuming accept method handles days_to_return
                return Response(status=status.HTTP_200_OK)
            else:
                # Handle cases where the accept method fails (e.g., due to model constraints)
                return Response({"error": "Cannot accept book request"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            # If the data didn't validate, return the errors
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["get", "post"])
    def reject(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        success = book_request.reject()
        if not success:
            return Response({"error": "Cannot reject book request"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def give_book(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        success = book_request.give_book()
        if not success:
            return Response({"error": "Cannot give book"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def return_book(self, request, pk=None):
        book_request = self.get_object()
        # If the request method is GET, return the book request instance
        if request.method == "GET":
            serializer = self.get_serializer(book_request)
            return Response(serializer.data)
        # If the request method is POST
        success = book_request.return_book()
        if not success:
            return Response({"error": "Cannot return book"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_200_OK)
