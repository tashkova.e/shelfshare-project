from rest_framework import routers

from .views import (
    BookOwnershipViewSet,
    BookRequestViewSet,
    ReadBookViewSet,
    ToReadViewSet,
)

router = routers.DefaultRouter()
router.register("bookownerships", BookOwnershipViewSet)
router.register("bookrequests", BookRequestViewSet)
router.register("readbooks", ReadBookViewSet)
router.register("toread", ToReadViewSet)
urlpatterns = router.urls
