from datetime import datetime

import numpy as np
from django.test import TestCase

from ..models import Author, Book, Genre

embedding_vector = np.random.rand(384)


class ModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.genre = Genre.objects.create(genre="Fiction")
        cls.author = Author.objects.create(name="John Doe")
        cls.book = Book.objects.create(
            ISBN="1234567890123",
            title="Sample Book",
            description="A sample description.",
            imageURL="https://example.com/image.jpg",
            publisher="Sample Publisher",
            publishDate="2023-01-01",
            goodreadsURL="https://www.goodreads.com/book/123456",
            embedding=embedding_vector,
        )
        cls.book.genres.add(cls.genre)
        cls.book.author.add(cls.author)

    def test_genre_model(self):
        self.assertEqual(str(self.genre), "Fiction")

    def test_author_model(self):
        self.assertEqual(str(self.author), "John Doe")

    def test_book_model(self):
        book = self.book
        self.assertEqual(book.ISBN, "1234567890123")
        self.assertEqual(book.title, "Sample Book")
        self.assertEqual(book.description, "A sample description.")
        self.assertEqual(book.imageURL, "https://example.com/image.jpg")
        self.assertEqual(book.publisher, "Sample Publisher")
        self.assertEqual(book.publishDate, str(datetime(2023, 1, 1).date()))
        self.assertEqual(book.goodreadsURL, "https://www.goodreads.com/book/123456")
        self.assertEqual(str(book), "Sample Book")

    def test_book_genre_relationship(self):
        book = self.book
        genres = book.genres.all()
        self.assertEqual(genres.count(), 1)
        self.assertEqual(genres[0].genre, "Fiction")

    def test_book_author_relationship(self):
        book = self.book
        authors = book.author.all()
        self.assertEqual(authors.count(), 1)
        self.assertEqual(authors[0].name, "John Doe")
