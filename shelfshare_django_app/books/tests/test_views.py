import numpy as np
from django.test import TestCase
from rest_framework.test import APIRequestFactory

from ..models import Author, Book, Genre
from ..views import AuthorViewSet, BookViewSet, GenreViewSet

embedding_vector = np.random.rand(384)


class SimpleTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.genre = Genre.objects.create(genre="Fiction")
        cls.author = Author.objects.create(name="John Doe")
        cls.book = Book.objects.create(
            ISBN="1234567890123",
            title="Sample Book",
            description="A sample description.",
            imageURL="https://example.com/image.jpg",
            publisher="Sample Publisher",
            publishDate="2023-01-01",
            goodreadsURL="https://www.goodreads.com/book/123456",
            embedding=embedding_vector,
        )
        cls.book.genres.add(cls.genre)
        cls.book.author.add(cls.author)
        # Every test needs access to the request factory.
        cls.factory = APIRequestFactory()
        cls.genre_id = cls.genre.id
        cls.author_id = cls.author.id
        cls.book_id = cls.book.id

    def test_genre_list(self):
        # Create an instance of a GET request.
        request = self.factory.get("/api/api-books/genres/")
        # Use this syntax for class-based views.
        response = GenreViewSet.as_view({"get": "list"})(request)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(len(response.data["results"]), 1, "expected 1 object in list view")

    def test_genre_detail(self):
        request = self.factory.get("/api/api-books/genres/")
        response = GenreViewSet.as_view({"get": "retrieve"})(request, pk=self.genre_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["genre"], "Fiction", "expected genre Fiction")

    def test_genre_create(self):
        request = self.factory.post("/api/api-books/genres/", {"genre": "Nonfiction"}, format="json")
        response = GenreViewSet.as_view({"post": "create"})(request)
        self.assertEqual(
            response.status_code, 201, "expected response code 201, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["genre"], "Nonfiction", "expected genre Nonfiction")

    def test_genre_update(self):
        request = self.factory.put("/api/api-books/genres/", {"genre": "Non-fiction"}, format="json")
        response = GenreViewSet.as_view({"put": "update"})(request, pk=self.genre_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["genre"], "Non-fiction", "expected genre Non-fiction")

    def test_genre_delete(self):
        request = self.factory.delete("/api/api-books/genres/")
        response = GenreViewSet.as_view({"delete": "destroy"})(request, pk=self.genre_id)
        self.assertEqual(
            response.status_code, 204, "expected response code 204, received {0} instead.".format(response.data)
        )

    def test_author_list(self):
        request = self.factory.get("/api/api-books/authors/")
        response = AuthorViewSet.as_view({"get": "list"})(request)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(len(response.data["results"]), 1, "expected 1 object in list view")

    def test_author_detail(self):
        request = self.factory.get("/api/api-books/authors/")
        response = AuthorViewSet.as_view({"get": "retrieve"})(request, pk=self.author_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["name"], "John Doe")

    def test_author_create(self):
        request = self.factory.post("/api/api-books/authors/", {"name": "Jane Doe"}, format="json")
        response = AuthorViewSet.as_view({"post": "create"})(request)
        self.assertEqual(
            response.status_code, 201, "expected response code 201, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["name"], "Jane Doe")

    def test_author_update(self):
        request = self.factory.put("/api/api-books/authors/", {"name": "Jane Doe"}, format="json")
        response = AuthorViewSet.as_view({"put": "update"})(request, pk=self.author_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["name"], "Jane Doe")

    def test_author_delete(self):
        request = self.factory.delete("/api/api-books/authors/")
        response = AuthorViewSet.as_view({"delete": "destroy"})(request, pk=self.author_id)
        self.assertEqual(
            response.status_code, 204, "expected response code 204, received {0} instead.".format(response.data)
        )

    def test_book_list(self):
        request = self.factory.get("/api/api-books/books/")
        response = BookViewSet.as_view({"get": "list"})(request)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(len(response.data["results"]), 1)

    def test_book_detail(self):
        request = self.factory.get("/api/api-books/books/")
        response = BookViewSet.as_view({"get": "retrieve"})(request, pk=self.book_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["title"], "Sample Book")

    def test_book_create(self):
        self.assertTrue(Genre.objects.filter(id=self.genre_id).exists())
        self.assertTrue(Author.objects.filter(id=self.author_id).exists())
        embedding_lst = embedding_vector.tolist()
        request = self.factory.post(
            "/api/api-books/books/",
            {
                "ISBN": "1234567890124",
                "title": "Another Sample Book",
                "description": "Another sample description.",
                "imageURL": "https://example.com/image2.jpg",
                "publisher": "Another Sample Publisher",
                "publishDate": "2023-01-01",
                "goodreadsURL": "https://www.goodreads.com/book/123457",
                "author": [self.author_id],
                "genres": [self.genre_id],
                "embedding": embedding_lst,
            },
            format="json",
        )
        response = BookViewSet.as_view({"post": "create"})(request)
        self.assertEqual(
            response.status_code, 201, "expected response code 201, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["title"], "Another Sample Book")

    def test_book_update(self):
        self.assertTrue(Genre.objects.filter(id=self.genre_id).exists())
        self.assertTrue(Author.objects.filter(id=self.author_id).exists())
        request = self.factory.put(
            "/api/api-books/books/",
            {
                "ISBN": "1234567890123",
                "title": "Sample Book renamed",
                "author": [self.author_id],
                "genres": [self.genre_id],
            },
            format="json",
        )
        response = BookViewSet.as_view({"put": "update"})(request, pk=self.book_id)
        self.assertEqual(
            response.status_code, 200, "expected response code 200, received {0} instead.".format(response.data)
        )
        self.assertEqual(response.data["title"], "Sample Book renamed")

    def test_book_delete(self):
        request = self.factory.delete("/api/api-books/books/")
        response = BookViewSet.as_view({"delete": "destroy"})(request, pk=self.book_id)
        self.assertEqual(
            response.status_code, 204, "expected response code 204, received {0} instead.".format(response.data)
        )
