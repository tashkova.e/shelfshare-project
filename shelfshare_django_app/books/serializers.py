from rest_framework import serializers

from .models import Author, Book, Genre


class GenreSerializer(serializers.ModelSerializer):
    books = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Genre
        fields = ["id", "genre", "books"]


class AuthorSerializer(serializers.ModelSerializer):
    books = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Author
        fields = ["id", "name", "books"]


class BookSerializer(serializers.ModelSerializer):
    genres = serializers.PrimaryKeyRelatedField(many=True, queryset=Genre.objects.all())
    author = serializers.PrimaryKeyRelatedField(many=True, queryset=Author.objects.all())

    class Meta:
        model = Book
        fields = [
            "id",
            "ISBN",
            "title",
            "description",
            "imageURL",
            "publisher",
            "author",
            "publishDate",
            "goodreadsURL",
            "created",
            "genres",
            "embedding",
        ]
