from rest_framework import routers

from .views import AuthorViewSet, BookViewSet, GenreViewSet

router = routers.DefaultRouter()
router.register(r"books", BookViewSet)
router.register(r"genres", GenreViewSet)
router.register(r"authors", AuthorViewSet)

urlpatterns = router.urls
