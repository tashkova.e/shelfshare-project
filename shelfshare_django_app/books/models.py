from django.db import models
from pgvector.django import VectorField


# Create your models here.
class Genre(models.Model):
    genre = models.TextField(unique=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.genre


class Author(models.Model):
    name = models.TextField(unique=True)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.name


class Book(models.Model):
    ISBN = models.CharField(max_length=13, unique=True)
    title = models.TextField()
    description = models.TextField(
        blank=True
    )  # NOTE text fields in django are never set to null, so null=True is unnecessary.
    imageURL = models.TextField(blank=True)
    publisher = models.TextField(blank=True)
    author = models.ManyToManyField(Author)
    publishDate = models.DateField(null=True, blank=True)  # NOTE null for database, blank for form
    goodreadsURL = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    genres = models.ManyToManyField(Genre)
    embedding = VectorField(
        dimensions=384, blank=True, null=True
    )  # NOTE this can be empty because the description is optional

    class Meta:
        ordering = ["ISBN"]

    def __str__(self):
        return self.title
