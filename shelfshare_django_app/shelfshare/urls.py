"""
URL configuration for shelfshare project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path

from shelfshare import views

urlpatterns = [
    path("", views.home, name="home"),
    path("books-api/", include("books.urls")),
    path("users-api/", include("users.urls")),
    path("bookexchange-api/", include("bookexchange.urls")),
    path("analytics/", include("analytics.urls")),
    path("admin/", admin.site.urls),
    path("recommender-api/", include("recommend_app.urls")),
    path("auth-api/", include("rest_framework.urls")),
]

"""
In Django, the order of the paths in the urlpatterns list does matter.
 Django processes the URLs in the order they appear in the list.
 When a request is made, Django starts at the first path in the urlpatterns list and makes its way down the list until it finds a path that matches the requested URL.
"""
