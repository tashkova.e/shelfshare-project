from recommender_engine.data_storage.database_storage import DatabaseStorage
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from utils.recommenders import recs_dict

from shelfshare.settings import SAVE_MODEL_DIR

storage = DatabaseStorage("default")
import pickle


@api_view(["GET"])
def recommend_items(request):
    """
    Returns the top num books for a user
    """
    recommender_type = request.query_params.get("recommender")
    if recommender_type is None:
        return Response(
            {
                "error": "Recommender is required in the query parameters.Choose one of the following: popularity, collab, content, hybrid. Structure of url: recommend/?recommender={rec}&user_id={user_id}"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    if recommender_type not in recs_dict:
        return Response({"error": "Invalid recommender type"}, status=status.HTTP_400_BAD_REQUEST)
    recommender = recs_dict[recommender_type](storage)
    if recommender_type == "hybrid":
        fwls_model_path = SAVE_MODEL_DIR / "fwls_model.pkl"
        with open(fwls_model_path, "rb") as f:
            fwls_model = pickle.load(f)
        recommender.storage.fwls_model = fwls_model

    user_id = request.query_params.get("user_id")
    if user_id is None:
        return Response(
            {
                "error": "user_id is required in the query parameters. Structure of url: recommend/?recommender={rec}&user_id={user_id}"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    num = int(request.query_params.get("num", 6))
    recs = recommender.recommend_items(int(user_id), num)  # list of tuples (book_id, pred), convert it to list of dicts
    recs = [{"book_id": book_id, "prediction": pred} for book_id, pred in recs]
    return Response(recs)


@api_view(["GET"])
def predict_score(request):
    """
    Returns the predicted score for a user and book
    """
    recommender_type = request.query_params.get("recommender")
    if recommender_type is None:
        return Response(
            {
                "error": "Recommender is required in the query parameters.Structure of url: recommend/?recommender={rec}&user_id={user_id}&book_id={book_id}"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    if recommender_type not in recs_dict:
        return Response({"error": "Invalid recommender type"}, status=status.HTTP_400_BAD_REQUEST)
    recommender = recs_dict[recommender_type](storage)
    if recommender_type == "hybrid":
        fwls_model_path = SAVE_MODEL_DIR / "fwls_model.pkl"
        with open(fwls_model_path, "rb") as f:
            fwls_model = pickle.load(f)
        recommender.storage.fwls_model = fwls_model

    user_id = request.query_params.get("user_id")
    if user_id is None:
        return Response(
            {
                "error": "user_id is required in the query parameters. Structure of url: recommend/?recommender={rec}&user_id={user_id}&book_id={book_id}"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    book_id = request.query_params.get("book_id")
    if book_id is None:
        return Response(
            {
                "error": "book_id is required in the query parameters. Structure of url: recommend/?recommender={rec}&user_id={user_id}&book_id={book_id}"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    score = recommender.predict_score(int(user_id), int(book_id))
    return Response({"prediction": score})


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "recommend_items": reverse("recommend_items", request=request, format=format),
            "predict_score": reverse("predict_score", request=request, format=format),
        }
    )
