import logging
import pickle
from pathlib import Path
from unittest.mock import patch

from books.models import Book
from django.test import TestCase
from recommender_engine.data_storage.database_storage import DatabaseStorage
from recommender_engine.recommenders.building_utils import (
    save_similarities_to_db,
)
from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
    build_collab_item_similarity_matrix,
)
from recommender_engine.recommenders.hybrid import (
    FWLSModelBuilderParams,
    build_fwls_model,
)
from rest_framework import status
from rest_framework.test import APIClient
from users.models import User
from utils.create_test_database import (
    create_database_with_fake_books,
    create_movie_book_database,
    create_reduced_database,
)

logger = logging.getLogger("recommender.tests.test_integration")
dummy_models_dir = Path("dummy_models_dir")
import shutil


class BaseTestCase:

    def test_popularity_based_recommender_predict_score(self):
        """Make sure every user gets a score for every book"""
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=popularity&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                # check if the score is a float
                score = response.data["prediction"]
                self.assertTrue(isinstance(score, float))

    def test_collab_recommender_recommend_items(self):
        """Make sure every user gets a valid response"""
        # build the item similarity matrix
        model_params = CollabItemSimilarityParams(min_overlap=1, min_sim=0, normalize=True)
        ratings = self.storage.load_ratings(min_no_ratings=1)
        sim_matrix_df = build_collab_item_similarity_matrix(ratings, model_params=model_params)
        save_similarities_to_db(sim_matrix_df, self.storage.vector_db_name)
        num_of_users_with_recs = 0
        for user_id in self.user_ids:
            response = self.client.get(f"/recommender-api/recommend/?recommender=collab&user_id={user_id}")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            if len(response.data) > 0:
                num_of_users_with_recs += 1
        logger.debug("Number of users with recommendations: %d", num_of_users_with_recs)

    def test_collab_recommender_predict_score(self):
        """Make sure every user gets a valid response"""
        # build the item similarity matrix
        model_params = CollabItemSimilarityParams(min_overlap=1, min_sim=0, normalize=True)
        ratings = self.storage.load_ratings(min_no_ratings=1)
        sim_matrix_df = build_collab_item_similarity_matrix(ratings, model_params=model_params)
        save_similarities_to_db(sim_matrix_df, self.storage.vector_db_name)
        num_of_users_with_scores = 0
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=collab&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                score = response.data["prediction"]
                # check if the score is a float
                self.assertTrue(isinstance(score, float))
                if score != 0:
                    num_of_users_with_scores += 1
        logger.debug("Number of users with non 0 scores: %d", num_of_users_with_scores)

    def test_content_based_recommender_recommend_items(self):
        """Make sure every user gets a valid response"""
        num_of_users_with_recs = 0
        for user_id in self.user_ids:
            response = self.client.get(f"/recommender-api/recommend/?recommender=content&user_id={user_id}")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            if len(response.data) > 0:
                num_of_users_with_recs += 1
        logger.debug("Number of users with recommendations: %d", num_of_users_with_recs)

    def test_content_based_recommender_predict_score(self):
        """Make sure every user gets a valid response"""
        num_of_users_with_scores = 0
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=content&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                score = response.data["prediction"]
                # check if the score is a float
                self.assertTrue(isinstance(score, float))
                if score != 0:
                    num_of_users_with_scores += 1
        logger.debug("Number of users with non 0 scores: %d", num_of_users_with_scores)

    @patch("recommend_app.views.SAVE_MODEL_DIR", dummy_models_dir)
    def test_hybrid_recommender_recommend_items(self):
        """Make sure every user gets a valid response"""
        dummy_models_dir.mkdir(exist_ok=True)
        # build the model
        collab_params_builder = CollabItemSimilarityParams(min_overlap=1, min_sim=0, normalize=True)
        collab_params_recommender = {"neighborhood_size": 10, "max_candidates": 100}
        content_params = {"neighborhood_size": 5, "max_candidates": 50}
        fwls_params = FWLSModelBuilderParams(
            collab_rec={"builder_params": collab_params_builder, "recommender_params": collab_params_recommender},
            content_rec=content_params,
        )
        read_books = self.storage.load_read_books(0, -1)
        model, collab_rec, content_rec = build_fwls_model(read_books, fwls_params)
        collab_sim_matrix = collab_rec.storage.collab_sim_matrix
        save_similarities_to_db(collab_sim_matrix, self.storage.vector_db_name)
        # save the model
        model_path = dummy_models_dir / "fwls_model.pkl"
        with open(model_path, "wb") as f:
            pickle.dump(model, f)

        num_of_users_with_recs = 0
        for user_id in self.user_ids:
            response = self.client.get(f"/recommender-api/recommend/?recommender=hybrid&user_id={user_id}")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            if len(response.data) > 0:
                num_of_users_with_recs += 1
        logger.debug("Number of users with recommendations: %d", num_of_users_with_recs)
        # delete dummy models dir
        shutil.rmtree(dummy_models_dir)

    @patch("recommend_app.views.SAVE_MODEL_DIR", dummy_models_dir)
    def test_hybrid_recommender_predict_score(self):
        """Make sure every user gets a valid response"""
        dummy_models_dir.mkdir(exist_ok=True)
        dummy_models_dir.mkdir(exist_ok=True)
        # build the model
        collab_params_builder = CollabItemSimilarityParams(min_overlap=1, min_sim=0, normalize=True)
        collab_params_recommender = {"neighborhood_size": 10, "max_candidates": 100}
        content_params = {"neighborhood_size": 5, "max_candidates": 50}
        fwls_params = FWLSModelBuilderParams(
            collab_rec={"builder_params": collab_params_builder, "recommender_params": collab_params_recommender},
            content_rec=content_params,
        )
        read_books = self.storage.load_read_books(0, -1)
        model, collab_rec, content_rec = build_fwls_model(read_books, fwls_params)
        collab_sim_matrix = collab_rec.storage.collab_sim_matrix
        save_similarities_to_db(collab_sim_matrix, self.storage.vector_db_name)
        # save the model
        model_path = dummy_models_dir / "fwls_model.pkl"
        with open(model_path, "wb") as f:
            pickle.dump(model, f)

        num_of_users_with_scores = 0
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=hybrid&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                score = response.data["prediction"]
                # check if the score is a float
                self.assertTrue(isinstance(score, float))
                if score != 0:
                    num_of_users_with_scores += 1
        logger.debug("Number of users with non 0 scores: %d", num_of_users_with_scores)
        # delete dummy models dir
        shutil.rmtree(dummy_models_dir)


class TestWithReducedDatabase(BaseTestCase, TestCase):
    @classmethod
    def setUpTestData(cls):
        create_reduced_database("default")
        cls.storage = DatabaseStorage("default")
        cls.client = APIClient()
        # get the ids of the created users
        cls.user_ids = [user.id for user in User.objects.all()]
        # get the ids of the created books
        cls.book_ids = [book.id for book in Book.objects.all()]


class TestWithFakeBooks(BaseTestCase, TestCase):
    @classmethod
    def setUpTestData(cls):
        create_database_with_fake_books("default")
        cls.storage = DatabaseStorage("default")
        cls.client = APIClient()
        # get the ids of the created users
        cls.user_ids = [user.id for user in User.objects.all()]
        # get the ids of the created books
        cls.book_ids = [book.id for book in Book.objects.all()]

    def test_content_based_recommender_predict_score(self):
        """Make sure every user gets a valid response and every score is 0"""
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=content&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                score = response.data["prediction"]
                self.assertEqual(score, 0)

    def test_content_based_recommender_recommend_items(self):
        """Make sure every user gets a valid response and an empty list of recommendations"""
        # build the item similarity matrix
        for user_id in self.user_ids:
            response = self.client.get(f"/recommender-api/recommend/?recommender=content&user_id={user_id}")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data), 0)


class TestWithMovieBooks(BaseTestCase, TestCase):
    @classmethod
    def setUpTestData(cls):
        create_movie_book_database("default")
        cls.storage = DatabaseStorage("default")
        cls.client = APIClient()
        # get the ids of the created users
        cls.user_ids = [user.id for user in User.objects.all()]
        # get the ids of the created books
        cls.book_ids = [book.id for book in Book.objects.all()]

    def test_content_based_recommender_predict_score(self):
        """Make sure every user gets a valid response and every score is 0"""
        for user_id in self.user_ids:
            for book_id in self.book_ids:
                response = self.client.get(
                    f"/recommender-api/predict/?recommender=content&user_id={user_id}&book_id={book_id}"
                )
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                score = response.data["prediction"]
                self.assertEqual(score, 0)

    def test_content_based_recommender_recommend_items(self):
        """Make sure every user gets a valid response and an empty list of recommendations"""
        for user_id in self.user_ids:
            response = self.client.get(f"/recommender-api/recommend/?recommender=content&user_id={user_id}")
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(len(response.data), 0)
