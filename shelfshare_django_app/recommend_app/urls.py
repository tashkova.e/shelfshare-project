from django.urls import path

from .views import api_root, predict_score, recommend_items

urlpatterns = [
    path("", api_root, name="api-root"),
    path("recommend/", recommend_items, name="recommend_items"),
    path("predict/", predict_score, name="predict_score"),
]
