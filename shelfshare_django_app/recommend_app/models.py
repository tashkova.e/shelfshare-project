from books.models import Book
from django.db import models


# Create your models here.
class CollabSimilarity(models.Model):
    """Similarity between two books based on collaborative filtering"""

    created = models.DateTimeField(auto_now_add=True)
    source = models.ForeignKey(Book, related_name="source", on_delete=models.CASCADE)
    target = models.ForeignKey(Book, related_name="target", on_delete=models.CASCADE)
    similarity = models.FloatField()

    class Meta:
        db_table = "similarity"
        unique_together = (("source", "target"),)

    def __str__(self):
        return f"[({self.source} => {self.target}) sim = {self.similarity}]"
