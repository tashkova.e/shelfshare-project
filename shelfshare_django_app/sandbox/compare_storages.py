import os

from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
)
from recommender_engine.recommenders.hybrid import FWLSModelBuilderParams

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import timeit
from datetime import datetime

import pandas as pd
from books.models import Book
from recommender_engine.data_storage_old import DatabaseStorage, FileStorage
from recommender_engine.evaluation_old.scenarios import (
    train_test_multiple_parameters,
)
from recommender_engine.model_building_params import D2VSimilarityParams
from recommender_engine.recommenders_old import (
    CollaborativeFilteringRecommender,
    ContentBasedRecommender,
    FWLSRecommender,
    build_collab_item_similarity_matrix,
    build_d2v_similarity_matrix,
)
from users.models import User
from utils.create_test_database import create_reduced_database

from shelfshare.settings import (
    OUTSIDE_DATA_DIR,
    PROFILERS_DIR,
    TEST_DATASET_DIR,
    logging,
)


def main():
    """Test the times of the most important methods of the storage."""
    logger = logging.getLogger("compare_storages")
    logger.info("Starting the comparison of the storage methods...")
    num_of_iterations = 20
    # populate the database
    create_reduced_database()

    user_item_path = TEST_DATASET_DIR / "original_db_reduced" / "ratings_sample.csv"
    descriptions_path = TEST_DATASET_DIR / "original_db_reduced" / "books_sample.csv"
    user_item_df = pd.read_csv(user_item_path)
    descriptions_df = pd.read_csv(descriptions_path)

    file_storage = FileStorage(user_item_df, descriptions_df)
    database_storage = DatabaseStorage()

    methods_to_test = [
        # building the similarity matrix
        "load_read_books",
        "build_collab_item_similarity_matrix",
        "build_d2v_similarity_matrix",
        # recommendations
        "rated_books_by_user",
        "unrated_books_by_user",
        "candidate_books_recommend_items",
        "candidate_books_predict_score",
    ]
    res_df = pd.DataFrame(columns=["method", "time_file_storage", "time_database_storage"])
    res_df["method"] = methods_to_test

    # time the load_read_books method
    logger.info("Timing the load_read_books method...")
    time_file_storage = timeit.timeit(lambda: file_storage.load_read_books(), number=num_of_iterations)
    time_database_storage = timeit.timeit(lambda: database_storage.load_read_books(), number=num_of_iterations)
    res_df.loc[res_df.method == "load_read_books", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "load_read_books", "time_database_storage"] = time_database_storage
    # log the times
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time the build_collab_item_similarity_matrix method
    logger.info("Timing the build_collab_item_similarity_matrix method...")
    ratings_file = file_storage.load_ratings()
    ratings_database = database_storage.load_ratings()
    # define the parameters
    collab_params = CollabItemSimilarityParams(0, 0.5, True)
    time_file_storage = timeit.timeit(
        lambda: build_collab_item_similarity_matrix(ratings_file, collab_params, False), number=num_of_iterations
    )
    time_database_storage = timeit.timeit(
        lambda: build_collab_item_similarity_matrix(ratings_database, collab_params, True), number=num_of_iterations
    )
    res_df.loc[res_df.method == "build_collab_item_similarity_matrix", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "build_collab_item_similarity_matrix", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time the build_d2v_similarity_matrix method
    logger.info("Timing the build_d2v_similarity_matrix method...")
    descriptions_file = file_storage.load_descriptions()
    descriptions_database = database_storage.load_descriptions()
    # define the parameters
    d2v_params = D2VSimilarityParams(0.9)
    time_file_storage = timeit.timeit(
        lambda: build_d2v_similarity_matrix(descriptions_file, d2v_params, False), number=num_of_iterations
    )
    time_database_storage = timeit.timeit(
        lambda: build_d2v_similarity_matrix(descriptions_database, d2v_params, True), number=num_of_iterations
    )
    res_df.loc[res_df.method == "build_d2v_similarity_matrix", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "build_d2v_similarity_matrix", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time the rated_books_by_user method
    logger.info("Timing the rated_books_by_user method...")
    user_id_file = 21017
    user_id_database = User.objects.get(username="mellowgold15").id
    time_file_storage = timeit.timeit(lambda: file_storage.rated_books_by_user(user_id_file), number=num_of_iterations)
    time_database_storage = timeit.timeit(
        lambda: database_storage.rated_books_by_user(user_id_database), number=num_of_iterations
    )
    res_df.loc[res_df.method == "rated_books_by_user", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "rated_books_by_user", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time unrated_books_by_user
    logger.info("Timing the unrated_books_by_user method...")
    time_file_storage = timeit.timeit(
        lambda: file_storage.unrated_books_by_user(user_id_file), number=num_of_iterations
    )
    time_database_storage = timeit.timeit(
        lambda: database_storage.unrated_books_by_user(user_id_database), number=num_of_iterations
    )
    res_df.loc[res_df.method == "unrated_books_by_user", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "unrated_books_by_user", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time candidate_books_recommend_items
    logger.info("Timing the candidate_books_recommend_items method...")
    rated_books_file = file_storage.rated_books_by_user(user_id_file)
    rated_books_database = database_storage.rated_books_by_user(user_id_database)
    collab_sim_matrix = build_collab_item_similarity_matrix(ratings_file, collab_params, False)
    d2v_sim_matrix = build_d2v_similarity_matrix(descriptions_file, d2v_params, False)
    file_storage.collab_sim_matrix = collab_sim_matrix
    file_storage.d2v_sim_matrix = d2v_sim_matrix

    unrated_books_file = file_storage.unrated_books_by_user(user_id_file)
    unrated_books_database = database_storage.unrated_books_by_user(user_id_database)
    time_file_storage = timeit.timeit(
        lambda: file_storage.candidate_books_recommend_items("collab", rated_books_file, unrated_books_file),
        number=num_of_iterations,
    )
    time_database_storage = timeit.timeit(
        lambda: database_storage.candidate_books_recommend_items(
            "collab", rated_books_database, unrated_books_database
        ),
        number=num_of_iterations,
    )
    res_df.loc[res_df.method == "candidate_books_recommend_items", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "candidate_books_recommend_items", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # time candidate_books_predict_score
    logger.info("Timing the candidate_books_predict_score method...")
    book_id_file = 524
    book_isbn = "1844137872"
    book_id_database = Book.objects.get(ISBN=book_isbn).id
    time_file_storage = timeit.timeit(
        lambda: file_storage.candidate_books_predict_score("collab", book_id_file, rated_books_file),
        number=num_of_iterations,
    )
    time_database_storage = timeit.timeit(
        lambda: database_storage.candidate_books_predict_score("collab", book_id_database, rated_books_database),
        number=num_of_iterations,
    )
    res_df.loc[res_df.method == "candidate_books_predict_score", "time_file_storage"] = time_file_storage
    res_df.loc[res_df.method == "candidate_books_predict_score", "time_database_storage"] = time_database_storage
    logger.info("time_file_storage: %s", time_file_storage)
    logger.info("time_database_storage: %s", time_database_storage)

    # the name of the csv file should be compare_storages_timestamp.csv
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    res_df.to_csv(OUTSIDE_DATA_DIR / f"compare_storages_{timestamp}.csv", index=False)


if __name__ == "__main__":
    main()
