import os

from recommender_engine.recommenders.collaborative import (
    CollabItemSimilarityParams,
)
from recommender_engine.recommenders.hybrid import FWLSModelBuilderParams

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shelfshare.settings")
import django

django.setup()
import cProfile
import pstats
from datetime import datetime

import typer
from recommender_engine.data_storage_old import FileStorage
from recommender_engine.evaluation_old.scenarios import (
    train_test_multiple_parameters,
)
from recommender_engine.model_building_params import D2VSimilarityParams
from recommender_engine.recommenders_old import (
    CollaborativeFilteringRecommender,
    ContentBasedRecommender,
    FWLSRecommender,
)

from shelfshare.settings import (
    OUTSIDE_DATA_DIR,
    PROFILERS_DIR,
    TEST_DATASET_DIR,
)

app = typer.Typer()
import pandas as pd

user_item_path = OUTSIDE_DATA_DIR / "transformed" / "ratings_filtered_some_set_to_0.csv"
descriptions_path = OUTSIDE_DATA_DIR / "transformed" / "books_final.csv"
user_item_df = pd.read_csv(user_item_path)
descriptions_df = pd.read_csv(descriptions_path)
storage = FileStorage(user_item_df, descriptions_df)


@app.command()
def collab():
    k_list = [1, 3, 5, 10]
    test_size = 0.2
    min_no_ratings = 0
    users_with_most_books = -1
    collab_params_list = [
        CollabItemSimilarityParams(0, 0.2, True),
        CollabItemSimilarityParams(0, 0.4, True),
        CollabItemSimilarityParams(1, 0.2, True),
    ]
    recommender_params_list = [{"neighborhood_size": 6} for _ in range(3)]
    with cProfile.Profile() as pr:
        train_test_multiple_parameters(
            CollaborativeFilteringRecommender,
            storage,
            k_list,
            collab_params_list,
            recommender_params_list,
            test_size=test_size,
            min_no_ratings=min_no_ratings,
            users_with_most_books=users_with_most_books,
        )
    stats = pstats.Stats(pr)
    stats.sort_stats(pstats.SortKey.TIME)
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    file_dir = PROFILERS_DIR / "multiple_params"
    file_dir.mkdir(exist_ok=True)
    file_name = f"{file_dir}/collaborative_filtering_{timestamp}.prof"
    stats.dump_stats(filename=file_name)


if __name__ == "__main__":
    app()
