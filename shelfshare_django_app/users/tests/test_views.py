from django.core.exceptions import ValidationError
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory

from ..models import Friend, FriendshipRequest, User
from ..views import FriendshipRequestViewSet, FriendViewSet, UserViewSet


class UserViewTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.uri = "/api/api-users/users"
        self.user = User.objects.create_user("user1", "user1@mail.com", "password")
        self.admin = User.objects.create_superuser("admin", "admin@gmail.com", "password")

    def test_list(self):
        request = self.factory.get(self.uri)
        request.user = self.user
        view = UserViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        self.assertEqual(len(response.data["results"]), 2, "Expected 2 object in List View")

    def test_list_anonymous(self):
        request = self.factory.get(self.uri)
        view = UserViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 401, received {0} instead.".format(response.data),
        )

    def test_create(self):
        params = {
            "username": "testuser",
            "password": "testpass",
            "email": "testuser@example.com",
            "sex": "M",
            "first_name": "Test",
            "last_name": "User",
        }
        request = self.factory.post(self.uri, params)
        view = UserViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            "Expected Response Code 201, received {0} instead.".format(response.data),
        )

    def test_retrieve(self):
        request = self.factory.get(self.uri)
        request.user = self.user
        view = UserViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_retrieve_anonymous(self):
        request = self.factory.get(self.uri)
        view = UserViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 401, received {0} instead.".format(response.data),
        )

    def test_update(self):
        params = {
            "username": "testuser",
            "password": "testpass",
            "sex": "M",
            "email": "newmail@gmail.com",
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user
        view = UserViewSet.as_view({"put": "update"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_update_anonymous(self):
        params = {
            "username": "testuser",
            "password": "testpass",
            "sex": "M",
            "email": "newmail@gmail.com",
        }
        request = self.factory.put(self.uri, params)
        view = UserViewSet.as_view({"put": "update"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update_admin(self):
        params = {
            "username": "testuser",
            "password": "testpass",
            "sex": "M",
            "email": "newmail@gmail.com",
        }
        request = self.factory.put(self.uri, params)
        request.user = self.admin
        view = UserViewSet.as_view({"put": "update"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_delete(self):
        request = self.factory.delete(self.uri)
        request.user = self.user
        view = UserViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT,
            "Expected Response Code 204, received {0} instead.".format(response.data),
        )

    def test_delete_anonymous(self):
        request = self.factory.delete(self.uri)
        view = UserViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_delete_admin(self):
        request = self.factory.delete(self.uri)
        request.user = self.admin
        view = UserViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT,
            "Expected Response Code 204, received {0} instead.".format(response.data),
        )

    def test_get_friends(self):
        url = self.uri + f"/friends"
        request = self.factory.get(url)
        request.user = self.user
        view = UserViewSet.as_view({"get": "friends"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        self.assertEqual(len(response.data["results"]), 0, "Expected 0 object in List View")

    def test_get_friends_anonymous(self):
        url = self.uri + f"/friends"
        request = self.factory.get(url)
        view = UserViewSet.as_view({"get": "friends"})
        response = view(request, pk=self.user.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )


class FriendshipRequestTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user("user1", "user1@mail.com", "password")
        cls.user2 = User.objects.create_user("user2", "user2@mail.com", "password")
        cls.admin = User.objects.create_superuser("admin", "admin@mail.com", "password", is_staff=True)
        cls.uri = "api/api-users/friendshiprequests"
        cls.factory = APIRequestFactory()

    def test_list(self):
        request = self.factory.get(self.uri)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_list_anonymous(self):
        request = self.factory.get(self.uri)
        view = FriendshipRequestViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_create(self):
        params = {
            "to_user": self.user2.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            "Expected Response Code 201, received {0} instead.".format(response.data),
        )
        created_request = FriendshipRequest.objects.get(pk=response.data["id"])
        self.assertEqual(
            created_request.from_user,
            self.user1,
            "Expected from_user to be {0}, received {1} instead.".format(self.user1, created_request.from_user),
        )
        self.assertEqual(
            created_request.to_user,
            self.user2,
            "Expected to_user to be {0}, received {1} instead.".format(self.user2, created_request.to_user),
        )
        self.assertEqual(
            created_request.message,
            "test message",
            'Expected message to be "test message", received {0} instead.'.format(created_request.message),
        )
        self.assertIsNone(
            created_request.rejected,
            "Expected rejected to be null, received {0} instead.".format(created_request.rejected),
        )

    def test_create_to_self(self):
        params = {
            "to_user": self.user1.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "Expected Response Code 400, received {0} instead.".format(response.data),
        )

    def test_create_to_nonexistent_user(self):
        params = {
            "to_user": 3,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "Expected Response Code 400, received {0} instead.".format(response.data),
        )

    def test_double_create(self):
        params = {
            "to_user": self.user2.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            "Expected Response Code 201, received {0} instead.".format(response.data),
        )
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "Expected Response Code 400, received {0} instead.".format(response.data),
        )

    def test_create_already_requested(self):
        params = {
            "to_user": self.user2.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED,
            "Expected Response Code 201, received {0} instead.".format(response.data),
        )
        params = {
            "to_user": self.user1.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST,
            "Expected Response Code 400, received {0} instead.".format(response.data),
        )

    def test_create_anonymous(self):
        params = {
            "to_user": self.user2.pk,
            "message": "test message",
        }
        request = self.factory.post(self.uri, params)
        view = FriendshipRequestViewSet.as_view({"post": "create"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_retrieve(self):
        # create a friendship request
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        request = self.factory.get(self.uri)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_retrieve_anonymous(self):
        request = self.factory.get(self.uri)
        view = FriendshipRequestViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=1)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update_message_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertEqual(
            updated_request.message,
            "new message",
            'Expected message to be "new message", received {0} instead.'.format(updated_request.message),
        )

    def test_update_message_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
        }
        request = self.factory.put(self.uri, params)
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update_message_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
        }
        request = self.factory.put(self.uri, params)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertEqual(
            updated_request.message,
            "new message",
            'Expected message to be "new message", received {0} instead.'.format(updated_request.message),
        )

    def test_update_rejected_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
            "rejected": True,
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertIsNone(
            updated_request.rejected,
            "Expected rejected to be null, received {0} instead.".format(updated_request.rejected),
        )
        self.assertEqual(
            updated_request.message,
            "new message",
            'Expected message to be "new message", received {0} instead.'.format(updated_request.message),
        )

    def test_update_rejected_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
            "rejected": True,
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update_rejected_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        params = {
            "to_user": self.user2.pk,
            "message": "new message",
            "rejected": True,
        }
        request = self.factory.put(self.uri, params)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"put": "update"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertIsNone(
            updated_request.rejected,
            "Expected rejected to be null, received {0} instead.".format(updated_request.rejected),
        )
        self.assertEqual(
            updated_request.message,
            "new message",
            'Expected message to be "new message", received {0} instead.'.format(updated_request.message),
        )

    def test_delete_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        request = self.factory.delete(self.uri)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT,
            "Expected Response Code 204, received {0} instead.".format(response.data),
        )
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def test_delete_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        request = self.factory.delete(self.uri)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_delete_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        request = self.factory.delete(self.uri)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"delete": "destroy"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_204_NO_CONTENT,
            "Expected Response Code 204, received {0} instead.".format(response.data),
        )
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def test_accept_get_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.get(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"get": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_accept_get_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.get(url)
        view = FriendshipRequestViewSet.as_view({"get": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_accept_post_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.post(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_accept_post_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.post(url)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"post": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        # we expect the friendship request to be deleted
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def test_accept_post_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.post(url)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"post": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        # we expect the friendship request to be deleted
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def accept_post_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/accept"
        request = self.factory.post(url)
        view = FriendshipRequestViewSet.as_view({"post": "accept"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_reject_get_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/reject"
        request = self.factory.get(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"get": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_reject_get_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/reject"
        request = self.factory.get(url)
        view = FriendshipRequestViewSet.as_view({"get": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_reject_post_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/reject"
        request = self.factory.post(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_reject_post_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        # check the value of rejected before the request
        self.assertIsNone(
            created_request.rejected,
            "Expected rejected to be null, received {0} instead.".format(created_request.rejected),
        )
        url = self.uri + f"/reject"
        request = self.factory.post(url)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"post": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        # check the value of rejected after the request
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertIsNotNone(
            updated_request.rejected,
            "Expected rejected to be not null, received {0} instead.".format(updated_request.rejected),
        )

    def test_reject_post_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        # check the value of rejected before the request
        self.assertIsNone(
            created_request.rejected,
            "Expected rejected to be null, received {0} instead.".format(created_request.rejected),
        )
        url = self.uri + f"/reject"
        request = self.factory.post(url)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"post": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        # check the value of rejected after the request
        updated_request = FriendshipRequest.objects.get(pk=created_request.pk)
        self.assertIsNotNone(
            updated_request.rejected,
            "Expected rejected to be not null, received {0} instead.".format(updated_request.rejected),
        )

    def test_reject_post_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/reject"
        request = self.factory.post(url)
        view = FriendshipRequestViewSet.as_view({"post": "reject"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_cancel_get_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.get(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"get": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )

    def test_cancel_get_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.get(url)
        view = FriendshipRequestViewSet.as_view({"get": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_cancel_post_creator(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.post(url)
        request.user = self.user1
        view = FriendshipRequestViewSet.as_view({"post": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def test_cancel_post_admin(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.post(url)
        request.user = self.admin
        view = FriendshipRequestViewSet.as_view({"post": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        self.assertFalse(
            FriendshipRequest.objects.filter(pk=created_request.pk).exists(), "Expected FriendshipRequest to be deleted"
        )

    def test_cancel_post_receiver(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.post(url)
        request.user = self.user2
        view = FriendshipRequestViewSet.as_view({"post": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_cancel_post_anonymous(self):
        created_request = FriendshipRequest.objects.create(
            from_user=self.user1, to_user=self.user2, message="test message"
        )
        url = self.uri + f"/cancel"
        request = self.factory.post(url)
        view = FriendshipRequestViewSet.as_view({"post": "cancel"})
        response = view(request, pk=created_request.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )


class FriendViewSetTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user("user1", "user1@mail.com", "password")
        cls.user2 = User.objects.create_user("user2", "user2@mail.com", "password")
        cls.friendship1 = Friend.objects.create(friend1=cls.user1, friend2=cls.user2)
        cls.friendship2 = Friend.objects.create(friend1=cls.user2, friend2=cls.user1)
        cls.uri = "api/api-users/friends"
        cls.factory = APIRequestFactory()

    def test_list(self):
        request = self.factory.get(self.uri)
        request.user = self.user1
        view = FriendViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        self.assertEqual(len(response.data["results"]), 2, "Expected 2 object in List View")

    def test_list_anonymous(self):
        request = self.factory.get(self.uri)
        view = FriendViewSet.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_retrieve(self):
        request = self.factory.get(self.uri)
        request.user = self.user1
        view = FriendViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=self.friendship1.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            "Expected Response Code 200, received {0} instead.".format(response.data),
        )
        retrieved_friendship = Friend.objects.get(pk=response.data["id"])
        self.assertEqual(
            retrieved_friendship.friend1,
            self.user1,
            "Expected friend1 to be {0}, received {1} instead.".format(self.user1, retrieved_friendship.friend1),
        )
        self.assertEqual(
            retrieved_friendship.friend2,
            self.user2,
            "Expected friend2 to be {0}, received {1} instead.".format(self.user2, retrieved_friendship.friend2),
        )

    def test_retrieve_anonymous(self):
        request = self.factory.get(self.uri)
        view = FriendViewSet.as_view({"get": "retrieve"})
        response = view(request, pk=self.friendship1.pk)
        self.assertEqual(
            response.status_code,
            status.HTTP_403_FORBIDDEN,
            "Expected Response Code 403, received {0} instead.".format(response.data),
        )

    def test_update(self):
        params = {
            "friend1": self.user1.pk,
            "friend2": self.user2.pk,
        }
        request = self.factory.put(self.uri, params)
        request.user = self.user1
        view = FriendViewSet.as_view({"put": "update"})
        with self.assertRaises(AttributeError):
            response = view(request, pk=self.friendship1.pk)

    def test_delete(self):
        request = self.factory.delete(self.uri)
        request.user = self.user1
        view = FriendViewSet.as_view({"delete": "destroy"})
        with self.assertRaises(AttributeError):
            response = view(request, pk=self.friendship1.pk)

    def test_create(self):
        params = {
            "friend1": self.user1.pk,
            "friend2": self.user2.pk,
        }
        request = self.factory.post(self.uri, params)
        request.user = self.user1
        view = FriendViewSet.as_view({"post": "create"})
        with self.assertRaises(AttributeError):
            response = view(request, pk=self.friendship1.pk)
