import os

from django.core.exceptions import ValidationError
from django.test import TestCase

from ..models import Friend, FriendshipRequest, User


class BaseTestCase(TestCase):
    def setUp(self):
        """
        Setup some initial users

        """
        self.user_pw = "test"
        self.user_bob = User.objects.create_user("bob", "bob@bob.com", self.user_pw)
        self.user_steve = User.objects.create_user("steve", "steve@steve.com", self.user_pw)
        self.user_susan = User.objects.create_user("susan", "susan@susan.com", self.user_pw)
        self.user_amy = User.objects.create_user("amy", "amy@amy.amy.com", self.user_pw)


class FriendshipModelTests(BaseTestCase):
    def test_friendship_request_accepting(self):
        # Bob wants to be friends with Steve
        req1 = FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve)

        # Ensure neither have friends already
        self.assertEqual(Friend.objects.friends(self.user_bob), [])
        self.assertEqual(Friend.objects.friends(self.user_steve), [])

        # Ensure FriendshipRequest is created
        self.assertEqual(FriendshipRequest.objects.filter(from_user=self.user_bob).count(), 1)
        self.assertEqual(FriendshipRequest.objects.filter(to_user=self.user_steve).count(), 1)

        # Ensure the proper sides have requests or not
        self.assertEqual(len(FriendshipRequest.objects.received_requests(self.user_bob)), 0)
        self.assertEqual(len(FriendshipRequest.objects.received_requests(self.user_steve)), 1)
        self.assertEqual(len(FriendshipRequest.objects.sent_requests(self.user_bob)), 1)
        self.assertEqual(len(FriendshipRequest.objects.sent_requests(self.user_steve)), 0)

        self.assertEqual(len(FriendshipRequest.objects.rejected_requests(self.user_steve)), 0)

        # Ensure they aren't friends at this point
        self.assertFalse(Friend.objects.are_friends(self.user_bob, self.user_steve))

        # Ensure Bob can't request another friendship request from Steve.
        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve)

        # Ensure Steve can't request a friendship request from Bob.
        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_steve, self.user_bob)

        # Accept the request
        req1.accept()
        self.assertEqual(len(Friend.objects.all()), 2)
        # Ensure neither have pending requests
        self.assertEqual(FriendshipRequest.objects.filter(from_user=self.user_bob).count(), 0)
        self.assertEqual(FriendshipRequest.objects.filter(to_user=self.user_steve).count(), 0)

        # Ensure both are in each other's friend lists
        self.assertEqual(Friend.objects.friends(self.user_steve), [self.user_bob])
        self.assertEqual(Friend.objects.friends(self.user_bob), [self.user_steve])
        self.assertTrue(Friend.objects.are_friends(self.user_bob, self.user_steve))

        # Make sure we can remove friendship
        self.assertTrue(Friend.objects.remove_friend(self.user_bob, self.user_steve))
        self.assertFalse(Friend.objects.are_friends(self.user_bob, self.user_steve))
        self.assertEqual(len(Friend.objects.all()), 0)
        self.assertFalse(Friend.objects.remove_friend(self.user_bob, self.user_steve))

    def test_friendship_request_canceling(self):
        # Susan wants to be friends with Amy, but cancels it
        req2 = FriendshipRequest.objects.add_friend(self.user_susan, self.user_amy)
        self.assertEqual(Friend.objects.friends(self.user_susan), [])
        self.assertEqual(Friend.objects.friends(self.user_amy), [])
        req2.cancel()
        self.assertEqual(FriendshipRequest.objects.received_requests(self.user_susan), [])
        self.assertEqual(FriendshipRequest.objects.received_requests(self.user_amy), [])

    def test_friendship_request_rejecting(self):
        # Susan wants to be friends with Amy, but Amy rejects it
        req3 = FriendshipRequest.objects.add_friend(self.user_susan, self.user_amy)
        self.assertEqual(Friend.objects.friends(self.user_susan), [])
        self.assertEqual(Friend.objects.friends(self.user_amy), [])
        self.assertEqual(len(FriendshipRequest.objects.sent_requests(self.user_susan)), 1)
        self.assertIsNone(FriendshipRequest.objects.sent_requests(self.user_susan)[0].rejected)
        req3.reject()
        self.assertEqual(len(FriendshipRequest.objects.sent_requests(self.user_susan)), 1)
        self.assertIsNotNone(FriendshipRequest.objects.sent_requests(self.user_susan)[0].rejected)
        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_susan, self.user_amy)

        self.assertFalse(Friend.objects.are_friends(self.user_susan, self.user_amy))
        self.assertEqual(len(FriendshipRequest.objects.rejected_requests(self.user_amy)), 1)

    def test_friendship_request_with_ourselves(self):
        # Ensure we can't be friends with ourselves
        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_bob, self.user_bob)

        # Ensure we can't do it manually either
        with self.assertRaises(ValidationError):
            Friend.objects.create(friend1=self.user_bob, friend2=self.user_bob)

    def test_already_friends_with_request(self):
        # Make Bob and Steve friends
        req = FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve)
        req.accept()

        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve)

        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_steve, self.user_bob)

    def test_multiple_friendship_requests(self):
        # Bob wants to be friends with Steve
        req1 = FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve)

        # Ensure neither have friends already
        self.assertEqual(Friend.objects.friends(self.user_bob), [])
        self.assertEqual(Friend.objects.friends(self.user_steve), [])

        # Ensure FriendshipRequest is created
        self.assertEqual(FriendshipRequest.objects.filter(from_user=self.user_bob).count(), 1)
        self.assertEqual(FriendshipRequest.objects.filter(to_user=self.user_steve).count(), 1)

        # Steve also wants to be friends with Bob before Bob replies
        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_steve, self.user_bob)

        # Ensure they aren't friends at this point
        self.assertFalse(Friend.objects.are_friends(self.user_bob, self.user_steve))

        # Accept the request
        req1.accept()

        # Ensure neither have pending requests
        self.assertEqual(FriendshipRequest.objects.filter(from_user=self.user_bob).count(), 0)
        self.assertEqual(FriendshipRequest.objects.filter(to_user=self.user_steve).count(), 0)
        self.assertEqual(FriendshipRequest.objects.filter(from_user=self.user_steve).count(), 0)
        self.assertEqual(FriendshipRequest.objects.filter(to_user=self.user_bob).count(), 0)

    def test_multiple_calls_add_friend(self):
        FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve, message="Testing")

        with self.assertRaises(ValidationError):
            FriendshipRequest.objects.add_friend(self.user_bob, self.user_steve, message="Foo Bar")
