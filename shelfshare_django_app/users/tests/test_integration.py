from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from ..models import Friend, FriendshipRequest, User
from ..views import FriendshipRequestViewSet, FriendViewSet, UserViewSet


class LoginTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_user_and_login(self):
        # User creation parameters
        params = {
            "username": "testuser",
            "password": "testpass",
            "email": "user@mail.com",
            "sex": "M",
            "first_name": "Test",
            "last_name": "User",
        }

        # Test user creation
        response = self.client.post("/users-api/users/", params, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Check if user exists in the database
        user_exists = User.objects.filter(username="testuser").exists()
        self.assertTrue(user_exists, "User creation failed")

        # Retrieve the created user
        created_user = User.objects.get(username="testuser")

        # Check if the user is active
        self.assertTrue(created_user.is_active, "User is not active")

        # Check if password is hashed
        self.assertNotEqual(created_user.password, "testpass", "Password is not hashed")

        # Attempt to login
        login_successful = self.client.login(username="testuser", password="testpass")
        self.assertTrue(login_successful, "Login failed")

        # Additional check: Is the user authenticated in the session?
        self.assertIn("_auth_user_id", self.client.session, "User ID not found in session")
        self.assertEqual(int(self.client.session["_auth_user_id"]), created_user.pk)


class InteractionsBetweenUsersTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_adding_and_accepting_friend(self):
        # Create users with api
        params1 = {
            "username": "bob",
            "password": "password",
            "email": "bob@mail.com",
            "sex": "M",
        }
        params2 = {
            "username": "steve",
            "password": "password",
            "email": "steve@mail.com",
            "sex": "M",
        }
        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        reponse2 = self.client.post("/users-api/users/", params2, format="json")
        bob_user = reponse1.data
        steve_user = reponse2.data
        self.assertEqual(reponse1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(reponse2.status_code, status.HTTP_201_CREATED)
        # login as bob
        login_successful = self.client.login(username="bob", password="password")
        self.assertTrue(login_successful, "Login failed")
        # bob adds steve as a friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": steve_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # assure neither bob nor steve can see each other as friends
        response = self.client.get("/users-api/users/" + str(bob_user["id"]) + "/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 0)
        response = self.client.get("/users-api/users/" + str(steve_user["id"]) + "/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 0)
        # ensure friendship request is created
        response = self.client.get("/users-api/friendshiprequests/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["from_user"], bob_user["id"])
        self.assertEqual(response.data["results"][0]["to_user"], steve_user["id"])
        # save id of friendship request
        friendship_request_id = response.data["results"][0]["id"]
        # ensure bob can't request another friendship request from steve
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": steve_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # ensure steve can't request a friendship request from bob
        self.client.logout()
        # Check if the user is authenticated
        is_authenticated = self.client.session.get("_auth_user_id") is not None
        self.assertFalse(is_authenticated, "Logout failed")
        login_successful = self.client.login(username="steve", password="password")
        self.assertTrue(login_successful, "Login failed")
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": bob_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # accept the request
        response = self.client.post(
            "/users-api/friendshiprequests/" + str(friendship_request_id) + "/accept/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # ensure bob and steve are friends
        response = self.client.get("/users-api/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 2)
        # ensure bob and steve can see each other as friends
        response = self.client.get("/users-api/users/" + str(bob_user["id"]) + "/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["id"], steve_user["id"])
        response = self.client.get("/users-api/users/" + str(steve_user["id"]) + "/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["id"], bob_user["id"])

    def test_adding_and_canceling(self):
        # Create users with api
        params1 = {
            "username": "susan",
            "password": "password",
            "email": "susan@mail.com",
            "sex": "F",
        }
        params2 = {
            "username": "amy",
            "password": "password",
            "email": "amy@mail.com",
            "sex": "F",
        }
        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        reponse2 = self.client.post("/users-api/users/", params2, format="json")
        susan_user = reponse1.data
        amy_user = reponse2.data
        self.assertEqual(
            reponse1.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(reponse1.data)
        )
        self.assertEqual(
            reponse2.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(reponse2.data)
        )
        # login as susan
        login_successful = self.client.login(username="susan", password="password")
        self.assertTrue(login_successful, "Login failed")
        # susan adds amy as a friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": amy_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_friendship_request = response.data["id"]
        # susan cancels request
        response = self.client.post(
            "/users-api/friendshiprequests/" + str(id_of_friendship_request) + "/cancel/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # assure there are no firendship requests
        response = self.client.get(
            "/users-api/friendshiprequests/" + str(id_of_friendship_request) + "/", format="json"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_404_NOT_FOUND,
            "expected 404 NOT FOUND, received " + str(response.status_code),
        )

    def test_adding_and_rejecting(self):
        # Create users with api
        params1 = {
            "username": "susan",
            "password": "password",
            "email": "susan@mail.com",
            "sex": "F",
        }
        params2 = {
            "username": "amy",
            "password": "password",
            "email": "amy@mail.com",
            "sex": "F",
        }
        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        reponse2 = self.client.post("/users-api/users/", params2, format="json")
        susan_user = reponse1.data
        amy_user = reponse2.data
        self.assertEqual(
            reponse1.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(reponse1.data)
        )
        self.assertEqual(
            reponse2.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(reponse2.data)
        )
        # login as susan
        login_successful = self.client.login(username="susan", password="password")
        self.assertTrue(login_successful, "Login failed")
        # susan adds amy as a friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": amy_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        id_of_friendship_request = response.data["id"]
        # login as amy
        self.client.logout()
        login_successful = self.client.login(username="amy", password="password")
        self.assertTrue(login_successful, "Login failed")
        # amy rejects request
        response = self.client.post(
            "/users-api/friendshiprequests/" + str(id_of_friendship_request) + "/reject/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # assure the rejected field of the friendship request is not null
        response = self.client.get(
            "/users-api/friendshiprequests/" + str(id_of_friendship_request) + "/", format="json"
        )
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertIsNotNone(response.data["rejected"])
        # assure susan and amy are not friends
        response = self.client.get("/users-api/friends/")
        self.assertEqual(
            response.status_code, status.HTTP_200_OK, "expected 200 OK, received " + str(response.status_code)
        )
        self.assertEqual(len(response.data["results"]), 0)
        # assure susan can't add amy as a friend
        self.client.logout()
        login_successful = self.client.login(username="susan", password="password")
        self.assertTrue(login_successful, "Login failed")
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": amy_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_friendship_request_with_ourselves(self):
        # Create users with api
        params1 = {
            "username": "bob",
            "password": "password",
            "email": "bob@mail.com",
            "sex": "M",
        }
        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        bob_user = reponse1.data
        self.assertEqual(
            reponse1.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(reponse1.data)
        )
        # login as bob
        login_successful = self.client.login(username="bob", password="password")
        self.assertTrue(login_successful, "Login failed")
        # bob adds himself as a friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": bob_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_already_friends_with_request(self):
        # Create users with api
        params1 = {
            "username": "bob",
            "password": "password",
            "email": "bob@mail.com",
            "sex": "M",
        }
        params2 = {
            "username": "steve",
            "password": "password",
            "email": "steve@mail.com",
            "sex": "M",
        }
        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        reponse2 = self.client.post("/users-api/users/", params2, format="json")
        bob_user = reponse1.data
        steve_user = reponse2.data
        self.assertEqual(reponse1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(reponse2.status_code, status.HTTP_201_CREATED)
        # login as bob
        login_successful = self.client.login(username="bob", password="password")
        self.assertTrue(login_successful, "Login failed")
        # add steve as friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": steve_user["id"]}, format="json")
        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED, "expected 201 CREATED, received " + str(response.data)
        )
        # login as steve
        self.client.logout()
        login_successful = self.client.login(username="steve", password="password")
        self.assertTrue(login_successful, "Login failed")
        # steve accepts bob's request
        response = self.client.post(
            "/users-api/friendshiprequests/" + str(response.data["id"]) + "/accept/", format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # assure steve can't add bob as a friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": bob_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        # assure bob can't add steve as a friend
        self.client.logout()
        login_successful = self.client.login(username="bob", password="password")
        self.assertTrue(login_successful, "Login failed")
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": steve_user["id"]}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_friendship_request_with_nonexistent_user(self):
        # Create users with api
        params1 = {
            "username": "bob",
            "password": "password",
            "email": "bob@mail.com",
            "sex": "M",
        }

        reponse1 = self.client.post("/users-api/users/", params1, format="json")
        bob_user = reponse1.data
        self.assertEqual(reponse1.status_code, status.HTTP_201_CREATED)
        # login as bob
        login_successful = self.client.login(username="bob", password="password")
        self.assertTrue(login_successful, "Login failed")
        # add nonexistent user as friend
        response = self.client.post("/users-api/friendshiprequests/", {"to_user": 100}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
