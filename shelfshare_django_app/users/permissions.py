from rest_framework import permissions


class IsSenderOfFriendshipRequest(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the user who sent the friend request.
        return obj.from_user == request.user


class IsReceiverOfFriendshipRequest(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the user who received the friend request.
        is_receiver = obj.to_user == request.user

        return is_receiver


class IsOwnerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Allow access if the request user is the object's user or an admin
        return obj == request.user or request.user.is_staff
