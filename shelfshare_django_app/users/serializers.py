from rest_framework import serializers

from .models import Friend, FriendshipRequest, User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "password",
            "email",
            "sex",
            "first_name",
            "last_name",
            "date_joined",
            "last_login",
        ]
        read_only_fields = ("date_joined", "last_login")
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)  # calls set_password internally
        return user


class FriendSerializer(serializers.ModelSerializer):
    class Meta:
        model = Friend
        fields = ["id", "friend1", "friend2", "created"]
        read_only_fields = ("created",)


class FriendshipRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendshipRequest
        fields = [
            "id",
            "from_user",
            "to_user",
            "message",
            "created",
            "rejected",
        ]
        read_only_fields = (
            "from_user",
            "created",
            "rejected",
        )

    def create(self, validated_data):
        validated_data["from_user"] = self.context["request"].user
        try:
            return FriendshipRequest.objects.add_friend(**validated_data)
        except Exception as e:
            raise serializers.ValidationError(str(e))
