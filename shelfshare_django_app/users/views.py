from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Friend, FriendshipRequest, User
from .permissions import (
    IsOwnerOrAdmin,
    IsReceiverOfFriendshipRequest,
    IsSenderOfFriendshipRequest,
)
from .serializers import (
    FriendSerializer,
    FriendshipRequestSerializer,
    UserSerializer,
)


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action == "create":
            permission_classes = [permissions.AllowAny]
        elif self.action in ["friends", "list", "retrieve"]:
            permission_classes = [permissions.IsAuthenticated]
        else:
            permission_classes = [IsOwnerOrAdmin]
        return [permission() for permission in permission_classes]

    @action(detail=True, methods=["get"])
    def friends(self, request, pk=None):
        user = self.get_object()
        friends = Friend.objects.friends(user)
        page = self.paginate_queryset(friends)
        if page is not None:
            serializer = UserSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        # page is None if there is no pagination class specified in the settings or no pagination parameters
        serializer = UserSerializer(friends, many=True)
        return Response(serializer.data)


class FriendViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = Friend.objects.all()
    serializer_class = FriendSerializer


class FriendshipRequestViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = FriendshipRequest.objects.all()
    serializer_class = FriendshipRequestSerializer

    def get_permissions(self):
        if self.action in ["update", "partial_update", "destroy"]:
            permission_classes = [permissions.IsAuthenticated, IsSenderOfFriendshipRequest | permissions.IsAdminUser]
        elif self.action in ["accept", "reject"]:
            permission_classes = [permissions.IsAuthenticated, IsReceiverOfFriendshipRequest | permissions.IsAdminUser]
        elif self.action == "cancel":
            permission_classes = [permissions.IsAuthenticated, IsSenderOfFriendshipRequest | permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    @action(detail=True, methods=["get", "post"])
    def accept(self, request, pk=None):
        friendship_request = self.get_object()
        # If the request method is GET, return the friendship request instance
        if request.method == "GET":
            serializer = self.get_serializer(friendship_request)
            return Response(serializer.data)
        # If the request method is POST
        friendship_request.accept()
        return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def reject(self, request, pk=None):
        friendship_request = self.get_object()
        # If the request method is GET, return the friendship request instance
        if request.method == "GET":
            serializer = self.get_serializer(friendship_request)
            return Response(serializer.data)
        # If the request method is POST
        friendship_request.reject()
        return Response(status=status.HTTP_200_OK)

    @action(detail=True, methods=["get", "post"])
    def cancel(self, request, pk=None):
        friendship_request = self.get_object()
        # If the request method is GET, return the friendship request instance
        if request.method == "GET":
            serializer = self.get_serializer(friendship_request)
            return Response(serializer.data)
        # If the request method is POST
        friendship_request.cancel()
        return Response(status=status.HTTP_200_OK)
