from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    SEX_CHOICES = [
        ("M", "Male"),
        ("F", "Female"),
        ("O", "Other"),
    ]

    sex = models.CharField(max_length=1, choices=SEX_CHOICES)

    class Meta:
        ordering = ["id"]


class FriendshipRequestManager(models.Manager):
    def received_requests(self, user):
        """Return a list of friendship requests"""

        qs = FriendshipRequest.objects.filter(to_user=user)
        qs = self._friendship_request_select_related(qs, "from_user", "to_user")
        requests = list(qs)

        return requests

    def sent_requests(self, user):
        """Return a list of friendship requests from user"""

        qs = FriendshipRequest.objects.filter(from_user=user)
        qs = self._friendship_request_select_related(qs, "from_user", "to_user")
        requests = list(qs)
        return requests

    def rejected_requests(self, user):
        """Return a list of rejected friendship requests"""

        qs = FriendshipRequest.objects.filter(to_user=user, rejected__isnull=False)
        qs = self._friendship_request_select_related(qs, "from_user", "to_user")
        rejected_requests = list(qs)

        return rejected_requests

    def active_requests(self, user):
        """All requests that haven't been rejected"""

        qs = FriendshipRequest.objects.filter(to_user=user, rejected__isnull=True)
        qs = self._friendship_request_select_related(qs, "from_user", "to_user")
        unrejected_requests = list(qs)

        return unrejected_requests

    def create(self, from_user, to_user, message=""):
        """Create a friendship request"""
        if from_user == to_user:
            raise ValidationError("Users cannot be friends with themselves")

        if Friend.objects.are_friends(from_user, to_user):
            raise ValidationError("Users are already friends")

        if FriendshipRequest.objects.filter(from_user=from_user, to_user=to_user).exists():
            raise ValidationError("You already requested friendship from this user.")

        if FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user).exists():
            raise ValidationError("This user already requested friendship from you.")
        request = super().create(from_user=from_user, to_user=to_user, message=message)

        return request

    def add_friend(self, from_user, to_user, message=""):
        """Create a friendship request"""
        request = FriendshipRequest.objects.create(from_user=from_user, to_user=to_user, message=message)

        return request

    def _friendship_request_select_related(self, qs, *fields):
        "used for optimizing the queries in the friendship request methods"
        qs = qs.select_related(*fields)
        return qs


class FriendshipManager(models.Manager):
    def friends(self, user):
        """Return a list of friends"""
        qs = self.select_related("friend2").filter(friend1=user)
        friends = [u.friend2 for u in qs]
        return friends

    def remove_friend(self, from_user, to_user):
        """Remove a friendship"""
        try:
            qs = self.filter(friend1__in=[to_user, from_user], friend2__in=[from_user, to_user])
            if qs:
                qs.delete()
                return True
            return False
        except Friend.DoesNotExist:
            return False

    def are_friends(self, user1, user2):
        """Return True if user1 and user2 are friends"""
        try:
            self.get(friend1=user1, friend2=user2)
            return True
        except Friend.DoesNotExist:
            return False


class FriendshipRequest(models.Model):
    """Model to represent friendship requests"""

    from_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="friendship_requests_sent",
    )
    to_user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="friendship_requests_received",
    )

    message = models.TextField("Message", blank=True)

    created = models.DateTimeField(auto_now_add=True)
    rejected = models.DateTimeField(blank=True, null=True)
    objects = FriendshipRequestManager()

    class Meta:
        unique_together = ("from_user", "to_user")
        ordering = ["id"]

    def __str__(self):
        return f"User #{self.from_user_id} friendship requested #{self.to_user_id}"

    def accept(self):
        Friend.objects.create(friend1=self.from_user, friend2=self.to_user)
        Friend.objects.create(friend1=self.to_user, friend2=self.from_user)
        self.delete()
        FriendshipRequest.objects.filter(from_user=self.to_user, to_user=self.from_user).delete()
        return True

    def reject(self):
        self.rejected = timezone.now()
        self.save()
        return True

    def cancel(self):
        self.delete()
        return True


class Friend(models.Model):
    friend1 = models.ForeignKey(User, models.CASCADE, related_name="friend1")
    friend2 = models.ForeignKey(User, models.CASCADE, related_name="friend2")
    created = models.DateTimeField(auto_now_add=True)

    objects = FriendshipManager()

    class Meta:
        unique_together = ("friend1", "friend2")
        ordering = ["id"]

    def __str__(self):
        return f"User #{self.friend1_id} is friends with #{self.friend2_id}"

    def save(self, *args, **kwargs):
        if self.friend1 == self.friend2:
            raise ValidationError("Users cannot be friends with themselves.")
        super().save(*args, **kwargs)
