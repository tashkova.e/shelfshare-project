from django.contrib import admin

# Register your models here.
from .models import Friend, FriendshipRequest, User

admin.site.register(User)
admin.site.register(FriendshipRequest)
admin.site.register(Friend)
