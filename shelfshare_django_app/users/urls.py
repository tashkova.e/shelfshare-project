from rest_framework import routers

from .views import FriendshipRequestViewSet, FriendViewSet, UserViewSet

router = routers.DefaultRouter()

router.register("users", UserViewSet)
router.register("friends", FriendViewSet)
router.register("friendshiprequests", FriendshipRequestViewSet)
urlpatterns = router.urls
