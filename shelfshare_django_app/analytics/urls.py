from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="analytics-dashboard"),
    path("user/<int:user_id>/", views.user_profile, name="user-profile"),
    path("genre/<int:genre_id>/", views.genre_profile, name="genre-profile"),
    path("author/<int:author_id>/", views.author_profile, name="author-profile"),
]
