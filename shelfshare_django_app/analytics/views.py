from itertools import accumulate

import pandas as pd
import plotly.express as px
from bookexchange.models import ReadBook
from books.models import Author, Book, Genre
from django.db.models import Avg, Count, IntegerField
from django.db.models.functions import Cast
from django.shortcuts import get_object_or_404, render
from plotly.offline import plot
from users.models import User


# Create your views here.
def index(request):
    no_users = User.objects.count()
    no_books = Book.objects.count()
    newest_users = User.objects.all().order_by("-date_joined")[:20]
    # Get top 10 read books
    top_books = (ReadBook.objects.values("book", "book__title").annotate(count=Count("book")).order_by("-count"))[:10]
    # Convert the queryset to a format suitable for Plotly
    df = pd.DataFrame(list(top_books))
    # Create the plot for the top 10 books
    if df.empty:
        plot_top_read_books = "No data to display."
    else:
        fig = px.bar(
            df,
            x="book__title",
            y="count",
            labels={"book__title": "Book Title", "count": "Count"},
            title="Top 10 Read Books",
        )
        # Convert the plot to an HTML div
        plot_top_read_books = plot(fig, output_type="div", include_plotlyjs=False)
    # Get the top 10 books based on average rating
    top_books = (
        ReadBook.objects.exclude(rating="0")  # Exclude books with no rating
        .values("book", "book__title")
        .annotate(average_rating=Avg(Cast("rating", output_field=IntegerField())))  # Calculate average rating
        .order_by("-average_rating")[:10]
    )  # Order by rating and get top 10

    df = pd.DataFrame(list(top_books))
    if df.empty:
        plot_div = "No data to display."
    else:
        fig = px.bar(
            df,
            x="book__title",
            y="average_rating",
            labels={"book__title": "Book Title", "average_rating": "Average Rating"},
            title="Top 10 Rated Books",
        )

        # Convert the plot to HTML
        plot_div = plot(fig, output_type="div", include_plotlyjs=False)
    # Query to get the distribution of ratings
    ratings_distribution = ReadBook.objects.values("rating").annotate(count=Count("rating")).order_by("rating")

    # Convert the query result to a format suitable for Plotly
    df = pd.DataFrame(list(ratings_distribution))
    if df.empty:
        plot_ratings_histogram = "No data to display."
    else:
        # Create the histogram plot
        fig_histogram = px.bar(
            df, x="rating", y="count", labels={"rating": "Rating", "count": "Count"}, title="Distribution of Ratings"
        )

        # Convert the plot to HTML
        plot_ratings_histogram = plot(fig_histogram, output_type="div", include_plotlyjs=False)

    # plot the distribution of number of ratings per user
    no_ratings_per_user = (
        ReadBook.objects.exclude(rating="0")
        .values("user")
        .annotate(no_ratings=Count("rating"))
        .order_by("no_ratings")
        .values("user", "no_ratings")
    )
    # make a dataframe
    df = pd.DataFrame(list(no_ratings_per_user))  # columns=["user", "no_ratings"])
    if df.empty:
        plot_no_users_histogram = "No data to display."
    else:
        # group by the number of ratings per user
        no_users_per_no_ratings = df.groupby("no_ratings").count()
        no_users_count = no_users_per_no_ratings["user"].values.tolist()
        no_ratings = no_users_per_no_ratings.index.tolist()
        no_ratings = [0] + no_ratings
        no_users_count = [no_users] + no_users_count
        no_users_cumulative = list(accumulate(no_users_count, lambda x, y: x - y))
        fig = px.bar(
            x=no_ratings,
            y=no_users_cumulative,
            labels={"x": "Number of Ratings", "y": "Number of Users"},
            title="Number of users with ratings greater than or equal to x",
        )
        plot_no_users_histogram = plot(fig, output_type="div", include_plotlyjs=False)
    context = {
        "total_users": no_users,
        "total_books": no_books,
        "plot_top_10_books": plot_div,
        "plot_ratings_histogram": plot_ratings_histogram,
        "newest_users": newest_users,
        "plot_most_read_books": plot_top_read_books,
        "plot_no_users_histogram": plot_no_users_histogram,
    }

    return render(request, "analytics/index.html", context)


def user_profile(request, user_id):
    # Assuming `user_id` is passed to the view to identify the user

    # Fetch the user object by ID
    user = get_object_or_404(User, pk=user_id)

    # Calculate the average rating given by the user
    average_rating = (
        ReadBook.objects.filter(user=user)
        .exclude(rating="0")
        .aggregate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
    )

    # Get the top 10 books rated by the user
    user_top_books = (
        ReadBook.objects.filter(user=user)
        .exclude(rating="0")
        .values("book__title")
        .annotate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
        .order_by("-average_rating")[:10]
    )

    # Convert the queryset to a format suitable for Plotly
    if user_top_books:
        df = pd.DataFrame(list(user_top_books))
        # Create the plot for the user's top-rated books
        fig = px.bar(
            df,
            x="book__title",
            y="average_rating",
            labels={"book__title": "Book Title", "average_rating": "Rating"},
            title=f"Top 10 Rated Books by {user.username}",
        )
        # Convert the plot to an HTML div
        plot_div = plot(fig, output_type="div", include_plotlyjs=False)
    else:
        plot_div = "No ratings to display."

    # Pass the data to the template
    context = {
        "user": user,
        "average_rating": average_rating["average_rating"],  # Access the average rating
        "plot_user_top_books": plot_div,
    }

    return render(request, "analytics/user.html", context)


def genre_profile(request, genre_id):
    # Fetch the genre by ID
    genre = get_object_or_404(Genre, pk=genre_id)

    # Calculate the average rating for books in this genre
    books_in_genre = Book.objects.filter(genres=genre)
    average_rating = (
        ReadBook.objects.filter(book__in=books_in_genre)
        .exclude(rating="0")
        .aggregate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
    )

    # Get the most popular books in this genre (e.g., top 10 by rating)
    popular_books = (
        ReadBook.objects.filter(book__in=books_in_genre)
        .exclude(rating="0")
        .values("book", "book__title")
        .annotate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
        .order_by("-average_rating")[:10]
    )
    # Convert the queryset to a format suitable for Plotly
    if popular_books:
        df = pd.DataFrame(list(popular_books))
        # Create the plot for the user's top-rated books
        fig = px.bar(
            df,
            x="book__title",
            y="average_rating",
            labels={"book__title": "Book Title", "average_rating": "Rating"},
            title=f"Top 10 Rated Books by {genre.genre}",
        )
        # Convert the plot to an HTML div
        plot_div = plot(fig, output_type="div", include_plotlyjs=False)
    else:
        plot_div = "No ratings to display."

    context = {"genre": genre, "average_rating": average_rating["average_rating"], "plot_genre_top_books": plot_div}

    return render(request, "analytics/genre.html", context)


def author_profile(request, author_id):
    # Fetch the author by ID
    author = get_object_or_404(Author, pk=author_id)

    # Calculate the average rating for books by this author
    books_by_author = Book.objects.filter(author=author)
    average_rating = (
        ReadBook.objects.filter(book__in=books_by_author)
        .exclude(rating="0")
        .aggregate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
    )

    # Get the most popular books by this author (e.g., top 10 by rating)
    popular_books = (
        ReadBook.objects.filter(book__in=books_by_author)
        .exclude(rating="0")
        .values("book", "book__title")
        .annotate(average_rating=Avg(Cast("rating", output_field=IntegerField())))
        .order_by("-average_rating")[:10]
    )
    # Convert the queryset to a format suitable for Plotly
    if popular_books:
        df = pd.DataFrame(list(popular_books))
        # Create the plot for the user's top-rated books
        fig = px.bar(
            df,
            x="book__title",
            y="average_rating",
            labels={"book__title": "Book Title", "average_rating": "Rating"},
            title=f"Top 10 Rated Books by {author.name}",
        )
        # Convert the plot to an HTML div
        plot_div = plot(fig, output_type="div", include_plotlyjs=False)
    else:
        plot_div = "No ratings to display."

    context = {"author": author, "average_rating": average_rating["average_rating"], "plot_author_top_books": plot_div}

    return render(request, "analytics/author.html", context)
