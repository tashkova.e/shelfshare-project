FROM python:3.11.9-bookworm
ENV PYTHONUNBUFFERED=1
RUN mkdir /django-app
RUN mkdir /data
COPY ./data /data
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./shelfshare_django_app /django-app
WORKDIR /django-app
