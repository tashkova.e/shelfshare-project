# ShelfShare

## Description
This project contains the code and data for the <b>ShelfShare</b> app. The app allows users to interact with books: rate them, mark them as read, exchange them with friends. It also contains a recommender system which recommends books to users based on their review history.

## Project structure

* ### data
    Data from kaggle and goodreads.com used to generate users, books and ratings into the app
* ### notebooks
    Notebooks used for EDA and modifying the data
* ### shelfshare_django_app
    Django app containing all the APIs, recommenders and scripts to generate models into the database from the data
## Running the app
This app can be ran with a local database or as a docker service. In both cases you will need to populate your database with the populate scripts in shelfshare_django_app.

## Recommender system
The recommender system consists of 4 types of recommender, which can be accessed through the recommender endpoint of the api:
- popularity recommender
- collaborative filtering recommender
- content based recommender
- hybrid recommender
