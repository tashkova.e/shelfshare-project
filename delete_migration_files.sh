#!/bin/bash

# Find and delete all .py files in migrations directories, except __init__.py
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete

# Find and delete all .pyc files in migrations directories
find . -path "*/migrations/*.pyc" -delete

echo "All migration files have been deleted."